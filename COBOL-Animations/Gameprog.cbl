*> This code is dedicated to the public domain
*> This is GNUCOBOL 2.0
identification division.
program-id. fifteen.
environment division.
configuration section.
repository. function all intrinsic.
data division.
working-storage section.
   01 Emptyrow PICTURE X(120).
   01 Trackpart1 PICTURE X(120).
   01 Trackpart2 PICTURE X(120).
   01 Trackpart3 PICTURE X(120).
   01 Trackpart4 PICTURE X(120).
   01 Trackpart5 PICTURE X(120).
   01 Trackpart6 PICTURE X(120).
   01 Trackpart7 PICTURE X(120).
   01 Trackpart8 PICTURE X(120).
   01 Trackpart9 PICTURE X(120).
   01 Trackpart10 PICTURE X(120).
   01 Trackpart11 PICTURE X(120).
   01 Trackpart12 PICTURE X(120).
   01 Trackpart13 PICTURE X(120).
   01 Trackpart14 PICTURE X(120).
   01 Trackpart15 PICTURE X(120).
   01 Trackpart16 PICTURE X(120).
   01 Trackpart17 PICTURE X(120).
   01 Trackpart18 PICTURE X(120).
   01 Trackpart19 PICTURE X(120).
   01 Trackpart20 PICTURE X(120).
   01 Trackpart21 PICTURE X(120).
   01 Trackpart22 PICTURE X(120).
   01 Trackpart23 PICTURE X(120).
   01 Trackpart24 PICTURE X(120).
   01 Inputoptionval1 PICTURE X(2).
   01 Inputoptionval2 PICTURE X(2).
01 Inputoptionval3 PICTURE X(2).
01 Inputoptionval4 PICTURE X(2).
01 Inputoptionval5 PICTURE X(2).
01 Inputoptionval6 PICTURE X(2).
01 Inputoptionval7 PICTURE X(2).
01 Inputoptionval8 PICTURE X(2).
01 Inputoptionval9 PICTURE X(2).
01 Inputoptionval10 PICTURE X(2).
01 Inputoptionval11 PICTURE X(2).
01 Inputoptionval12 PICTURE X(2).
01 Inputoptionval13 PICTURE X(2).
01 Inputoptionval14 PICTURE X(2).
01 Inputoptionval15 PICTURE X(2).
01 Inputoptionval16 PICTURE X(2).
01 Optval PICTURE X(1).
01 Optval2 PICTURE X(1).
    01 Optval3 PICTURE X(1).
    01 Returnval PICTURE X(1).
    01 Gamesavevar PICTURE X(1).
    01 Gamename PICTURE X(20).
    01 Moveval PICTURE X(2).
    01 Switchval1 PICTURE 9(1).
    01 Switchval2 PICTURE 9(1).
    01 Switchval3 PICTURE 9(1).
    01 Switchval4 PICTURE 9(1).
    01 Switchval5 PICTURE 9(1).
    01 Switchval6 PICTURE 9(1).
    01 Switchval7 PICTURE 9(1).
    01 Switchval8 PICTURE 9(1).
    01 Switchval9 PICTURE 9(1).
    01 Switchval10 PICTURE 9(1).
    01 Switchval11 PICTURE 9(1).
    01 Switchval12 PICTURE 9(1).
    01 Switchval13 PICTURE 9(1).
    01 Switchval14 PICTURE 9(1).
    01 Switchval15 PICTURE 9(1).
    01 Switchval16 PICTURE 9(1).
    01 Switchval17 PICTURE 9(1).
    01 Switchval18 PICTURE 9(1).
    01 Switchval19 PICTURE 9(1).
    01 Switchval20 PICTURE 9(1).
    01 Switchval21 PICTURE 9(1).
    01 Switchval22 PICTURE 9(1).
    01 Switchval23 PICTURE 9(1).
    01 Switchval24 PICTURE 9(1).
    01 Switchval25 PICTURE 9(1).
    01 Switchval26 PICTURE 9(1).
    01 Switchval27 PICTURE 9(1).
    01 Switchval28 PICTURE 9(1).
    01 Switchval29 PICTURE 9(1).
    01 Switchval30 PICTURE 9(1).
    01 Switchval31 PICTURE 9(1).
01 Posnum PICTURE 9(4).
01 Posnumold PICTURE 9(4).
01 Posnumnew PICTURE 9(4).
01 Rownum PICTURE 9(4).
01 Colnum PICTURE 9(4).
01 Partlen PICTURE 9(4).
01 Stopnumber PICTURE 9(1).
01 Inspectres PICTURE X(2).
01  r pic 9.
01  r-empty pic 9.
01  r-to pic 9.
01  r-from pic 9.

01  c pic 9.
01  c-empty pic 9.
01  c-to pic 9.
01  c-from pic 9.

01  display-table.
    03  display-row occurs 4.
        05  display-cell occurs 4 pic 99.

01  tile-number pic 99.
01  tile-flags pic x(16).

01  display-move value spaces.
    03  tile-id pic 99.

01  row-separator pic x(21) value all '.'.
01  column-separator pic x(3) value ' . '.

01  inversions pic 99.
01  current-tile pic 99.

01  winning-display pic x(32) value
        '01020304'
    &   '05060708'
    &   '09101112'
    &   '13141500'.
    SCREEN SECTION.
    01 Celladressoutput BACKGROUND-COLOR 3 FOREGROUND-COLOUR 0.
    05  VALUE "15 Puzzle game" BLANK SCREEN LINE  1 COL 15.
    05  VALUE "....................." LINE 3 COL 1.
    05  VALUE "." LINE 4 COL 1.
    05  Inputoption1 PIC X(2) LINE 4 COLUMN 3 FROM Inputoptionval1.
    05  VALUE "." LINE 4 COL 6.
    05  Inputoption2 PIC X(2) LINE  4 COLUMN 8 FROM Inputoptionval2.
    05  VALUE "." LINE 4 COL 11.
    05  Inputoption3 PIC X(2) LINE  4 COLUMN 13 FROM Inputoptionval3.
    05  VALUE "." LINE 4 COL 16.
    05  Inputoption4 LINE  4 COLUMN 18 PIC X(2) FROM Inputoptionval4.
    05  VALUE "." LINE 4 COL 21.
    05  VALUE "....................." LINE 5 COL 1.
    05  VALUE "." LINE 6 COL 1.
    05  Inputoption5 LINE  6 COLUMN 3 PIC X(2) FROM Inputoptionval5.
    05  VALUE "." LINE 6 COL 6.
    05  Inputoption6 LINE  6 COLUMN 8 PIC X(2) FROM Inputoptionval6.
    05  VALUE "." LINE 6 COL 11.
    05  Inputoption7 LINE  6 COLUMN 13 PIC X(2) FROM Inputoptionval7.
    05  VALUE "." LINE 6 COL 16.
    05  Inputoption8 LINE  6 COLUMN 18 PIC X(2) FROM Inputoptionval8.
    05  VALUE "." LINE 6 COL 21.
    05  VALUE "....................." LINE 7 COL 1.
    05  VALUE "." LINE 8 COL 1.
    05  Inputoption9 LINE  8 COLUMN 3 PIC X(2) FROM Inputoptionval9.
    05  VALUE "." LINE 8 COL 6.
    05  Inputoption10 LINE  8 COLUMN 8 PIC X(2) FROM Inputoptionval10.
    05  VALUE "." LINE 8 COL 11.
    05  Inputoption11 LINE  8 COLUMN 13 PIC X(2) FROM Inputoptionval11.
    05  VALUE "." LINE 8 COL 16.
    05  Inputoption12 LINE  8 COLUMN 18 PIC X(2) FROM Inputoptionval12.
    05  VALUE "." LINE 8 COL 21.
    05  VALUE "....................." LINE 9 COL 1.
    05  VALUE "." LINE 10 COL 1.
    05  Inputoption13 LINE  10 COLUMN 3 PIC X(2) FROM Inputoptionval13.
    05  VALUE "." LINE 10 COL 6.
    05  Inputoption14 LINE  10 COLUMN 8 PIC X(2) FROM Inputoptionval14.
    05  VALUE "." LINE 10 COL 11.
    05  Inputoption15 LINE  10 COLUMN 13 PIC X(2) FROM Inputoptionval15.
    05  VALUE "." LINE 10 COL 16.
    05  Inputoption16 LINE  10 COLUMN 18 PIC X(2) FROM Inputoptionval16.
    05  VALUE "." LINE 10 COL 21.
    05  VALUE "....................." LINE 11 COL 1.
    05  Posnuminfo LINE  14 COLUMN 18 PIC X(2) FROM Posnum.
    05  Movevalinput LINE  12 COLUMN 18 PIC X(2) TO Moveval.
    05  VALUE "Fastinput: " LINE 16 COL 30.
    05  Optvalinput LINE  16 COLUMN 40 PIC X(1) TO Optval.
    05  VALUE "Back to Start: " LINE 20 COLUMN 20.
    05  Returnvalinput LINE  20 COLUMN 40 PIC X(1) TO Returnval.
    01 Gamesavedetailscreen3 BACKGROUND-COLOR 3 FOREGROUND-COLOUR 0.
    05  VALUE "Spielname: " BLANK SCREEN LINE 3 COL 3.
    05  Gamenameinput LINE  3 COLUMN 26 PIC X(20) TO Gamename.
    01 Startscreen BACKGROUND-COLOR 3 FOREGROUND-COLOUR 0.
    05  VALUE "Gamecollection: " BLANK SCREEN LINE 1 COL 3.
    05  VALUE "Ein Spiel durch Setzen von x auswaehlen " LINE 2 COL 3.
    05  VALUE "15puzzlegame: " LINE 3 COL 3.
    05  Optvalinput_Start LINE  3 COLUMN 20 PIC X(1) TO Optval.
    05  VALUE "Trackgame: " Line 4 COL 3.
    05  Optvalinput2 LINE  4 COLUMN 20 PIC X(1) TO Optval2.
    05  VALUE "Elevatrogame: " Line 5 COL 3.
    05  Optvalinput3 LINE  5 COLUMN 20 PIC X(1) TO Optval3.
    01 Gamesavescreen BACKGROUND-COLOR 3 FOREGROUND-COLOUR 0.
    05  VALUE "Spiel speichern(j/n): " BLANK SCREEN LINE 3 COL 3.
    05  Gamesavevarinput LINE  3 COLUMN 26 PIC X(1) TO Gamesavevar.
    01 Trackscreen BACKGROUND-COLOR 3 FOREGROUND-COLOUR 0.
    05  VALUE "Railwaynetworkscreen: " BLANK SCREEN LINE 1 COL 3.
    05  VALUE "Switchprog: " LINE 27 COLUMN 20.
    05  Optvalinput LINE  27 COLUMN 40 PIC X(1) TO Optval.
    05 Trackpartinput1 LINE 2 COLUMN 1 PIC X(120) FROM Trackpart1.
    05 Trackpartinput2 LINE 3 COLUMN 1 PIC X(120) FROM Trackpart2.
    05 Trackpartinput3 LINE 4 COLUMN 1 PIC X(120) FROM Trackpart3.
    05 Trackpartinput4 LINE 5 COLUMN 1 PIC X(120) FROM Trackpart4.
    05 Trackpartinput5 LINE 6 COLUMN 1 PIC X(120) FROM Trackpart5.
    05 Trackpartinput6 LINE 7 COLUMN 1 PIC X(120) FROM Trackpart6.
    05 Trackpartinput7 LINE 8 COLUMN 1 PIC X(120) FROM Trackpart7.
    05 Trackpartinput8 LINE 9 COLUMN 1 PIC X(120) FROM Trackpart8.
    05 Trackpartinput9 LINE 10 COLUMN 1 PIC X(120) FROM Trackpart9.
    05 Trackpartinput10 LINE 11 COLUMN 1 PIC X(120) FROM Trackpart10.
    05 Trackpartinput11 LINE 12 COLUMN 1 PIC X(120) FROM Trackpart11.
    05 Trackpartinput12 LINE 13 COLUMN 1 PIC X(120) FROM Trackpart12.
    05 Trackpartinput13 LINE 14 COLUMN 1 PIC X(120) FROM Trackpart13.
    05 Trackpartinput14 LINE 15 COLUMN 1 PIC X(120) FROM Trackpart14.
    05 Trackpartinput15 LINE 16 COLUMN 1 PIC X(120) FROM Trackpart15.
    05 Trackpartinput16 LINE 17 COLUMN 1 PIC X(120) FROM Trackpart16.
    05 Trackpartinput17 LINE 18 COLUMN 1 PIC X(120) FROM Trackpart17.
    05 Trackpartinput18 LINE 19 COLUMN 1 PIC X(120) FROM Trackpart18.
    05 Trackpartinput19 LINE 20 COLUMN 1 PIC X(120) FROM Trackpart19.
    05 Trackpartinput20 LINE 21 COLUMN 1 PIC X(120) FROM Trackpart20.
    05 Trackpartinput21 LINE 22 COLUMN 1 PIC X(120) FROM Trackpart21.
    05 Trackpartinput22 LINE 23 COLUMN 1 PIC X(120) FROM Trackpart22.
    05 Trackpartinput23 LINE 24 COLUMN 1 PIC X(120) FROM Trackpart23.
    05 Trackpartinput24 LINE 25 COLUMN 1 PIC X(120) FROM Trackpart24.
    01 Switchscreen BACKGROUND-COLOR 3 FOREGROUND-COLOUR 0.
    05  VALUE "Switchscreen: " BLANK SCREEN LINE 1 COL 3.
    05  VALUE "Switch1: " LINE 3 COLUMN 3.
    05  Switchinput1 LINE 3 COLUMN 12 PIC 9(1) TO Switchval1.
    05  VALUE "Switch2: " LINE 4 COLUMN 3.
    05  Switchinput2 LINE 4 COLUMN 12 PIC 9(1) TO Switchval2.
    05  VALUE "Switch3: " LINE 5 COLUMN 3.
    05  Switchinput3 LINE 5 COLUMN 12 PIC 9(1) TO Switchval3.
    05  VALUE "Switch4: " LINE 6 COLUMN 3.
    05  Switchinput4 LINE 6 COLUMN 12 PIC 9(1) TO Switchval4.
    05  VALUE "Switch5: " LINE 7 COLUMN 3.
    05  Switchinput5 LINE 7 COLUMN 12 PIC 9(1) TO Switchval5.
    05  VALUE "Switch6: " LINE 8 COLUMN 3.
    05  Switchinput6 LINE 8 COLUMN 12 PIC 9(1) TO Switchval6.
    05  VALUE "Switch7: " LINE 9 COLUMN 3.
    05  Switchinput7 LINE 9 COLUMN 12 PIC 9(1) TO Switchval7.
    05  VALUE "Switch8: " LINE 10 COLUMN 3.
    05  Switchinput8 LINE 10 COLUMN 12 PIC 9(1) TO Switchval8.
    05  VALUE "Switch9: " LINE 11 COLUMN 3.
    05  Switchinput9 LINE 11 COLUMN 12 PIC 9(1) TO Switchval9.
    05  VALUE "Switch10: " LINE 12 COLUMN 3.
    05  Switchinput10 LINE 12 COLUMN 12 PIC 9(1) TO Switchval10.
    05  VALUE "Switch11: " LINE 13 COLUMN 3.
    05  Switchinput11 LINE 13 COLUMN 12 PIC 9(1) TO Switchval11.
    05  VALUE "Switch12: " LINE 14 COLUMN 3.
    05  Switchinput12 LINE 14 COLUMN 12 PIC 9(1) TO Switchval12.
    05  VALUE "Switch13: " LINE 15 COLUMN 3.
    05  Switchinput13 LINE 15 COLUMN 12 PIC 9(1) TO Switchval13.
    05  VALUE "Switch14: " LINE 16 COLUMN 3.
    05  Switchinput14 LINE 16 COLUMN 12 PIC 9(1) TO Switchval14.
    05  VALUE "Switch15: " LINE 17 COLUMN 3.
    05  Switchinput15 LINE 17 COLUMN 12 PIC 9(1) TO Switchval15.
    05  VALUE "Switch16: " LINE 18 COLUMN 3.
    05  Switchinput16 LINE 18 COLUMN 12 PIC 9(1) TO Switchval16.
    05  VALUE "Switch17: " LINE 19 COLUMN 3.
    05  Switchinput17 LINE 19 COLUMN 12 PIC 9(1) TO Switchval17.
    05  VALUE "Switch18: " LINE 20 COLUMN 3.
    05  Switchinput18 LINE 20 COLUMN 12 PIC 9(1) TO Switchval18.
    05  VALUE "Switch19: " LINE 21 COLUMN 3.
    05  Switchinput19 LINE 21 COLUMN 12 PIC 9(1) TO Switchval19.
    05  VALUE "Switch20: " LINE 22 COLUMN 3.
    05  Switchinput20 LINE 22 COLUMN 12 PIC 9(1) TO Switchval20.
    05  VALUE "Switch21: " LINE 23 COLUMN 3.
    05  Switchinput21 LINE 23 COLUMN 12 PIC 9(1) TO Switchval21.
    05  VALUE "Switch22: " LINE 24 COLUMN 3.
    05  Switchinput22 LINE 24 COLUMN 12 PIC 9(1) TO Switchval22.
    05  VALUE "Back to Trackscreen: " LINE 27 COLUMN 3.
    05  Optvalinput_1 LINE  27 COLUMN 40 PIC X(1) TO Optval.
    01 Elevatroscreen BACKGROUND-COLOR 3 FOREGROUND-COLOUR 0.
    05  VALUE "Elevatorscreen: " BLANK SCREEN LINE 1 COL 3.
    05  VALUE "Back to Trackscreen: " LINE 28 COLUMN 3.
    05  Optvalinput_2 LINE  28 COLUMN 40 PIC X(1) TO Optval.
    05  VALUE "Stopnumber: " LINE 26 COLUMN 3.
    05  Stopnumberinput LINE 26 COLUMN 30 PIC 9(1) TO Stopnumber.
    05 Trackpartinput1 LINE 2 COLUMN 1 PIC X(120) FROM Trackpart1.
    05 Trackpartinput2 LINE 3 COLUMN 1 PIC X(120) FROM Trackpart2.
    05 Trackpartinput3 LINE 4 COLUMN 1 PIC X(120) FROM Trackpart3.
    05 Trackpartinput4 LINE 5 COLUMN 1 PIC X(120) FROM Trackpart4.
    05 Trackpartinput5 LINE 6 COLUMN 1 PIC X(120) FROM Trackpart5.
    05 Trackpartinput6 LINE 7 COLUMN 1 PIC X(120) FROM Trackpart6.
    05 Trackpartinput7 LINE 8 COLUMN 1 PIC X(120) FROM Trackpart7.
    05 Trackpartinput8 LINE 9 COLUMN 1 PIC X(120) FROM Trackpart8.
    05 Trackpartinput9 LINE 10 COLUMN 1 PIC X(120) FROM Trackpart9.
    05 Trackpartinput10 LINE 11 COLUMN 1 PIC X(120) FROM Trackpart10.
    05 Trackpartinput11 LINE 12 COLUMN 1 PIC X(120) FROM Trackpart11.
    05 Trackpartinput12 LINE 13 COLUMN 1 PIC X(120) FROM Trackpart12.
    05 Trackpartinput13 LINE 14 COLUMN 1 PIC X(120) FROM Trackpart13.
    05 Trackpartinput14 LINE 15 COLUMN 1 PIC X(120) FROM Trackpart14.
    05 Trackpartinput15 LINE 16 COLUMN 1 PIC X(120) FROM Trackpart15.
    05 Trackpartinput16 LINE 17 COLUMN 1 PIC X(120) FROM Trackpart16.
    05 Trackpartinput17 LINE 18 COLUMN 1 PIC X(120) FROM Trackpart17.
    05 Trackpartinput18 LINE 19 COLUMN 1 PIC X(120) FROM Trackpart18.
    05 Trackpartinput19 LINE 20 COLUMN 1 PIC X(120) FROM Trackpart19.
    05 Trackpartinput20 LINE 21 COLUMN 1 PIC X(120) FROM Trackpart20.
    05 Trackpartinput21 LINE 22 COLUMN 1 PIC X(120) FROM Trackpart21.
    05 Trackpartinput22 LINE 23 COLUMN 1 PIC X(120) FROM Trackpart22.
    05 Trackpartinput23 LINE 24 COLUMN 1 PIC X(120) FROM Trackpart23.
    05 Trackpartinput24 LINE 25 COLUMN 1 PIC X(120) FROM Trackpart24.
    procedure division.
    PERFORM Gamestartprog.
    stop run.
    Gamestartprog.
    DISPLAY Startscreen.
    ACCEPT Startscreen.
    IF Optval = "x" THEN PERFORM start-fifteen.
    MOVE " " TO Optval.
    IF Optval2 = "x" THEN PERFORM Railnetworkprog.
    MOVE " " TO Optval2.
    IF Optval3 = "x" THEN PERFORM Elevatroprog.
    MOVE " " TO Optval3.
    Elevatroprog.
    PERFORM Elevatroinit.
    Elevatroinit.
    MOVE" " TO Emptyrow.
    MOVE"_____________________________________________________" TO Trackpart1.
    MOVE"|      |                                            |" TO Trackpart2.
    MOVE"|      |                                            |" TO Trackpart3.
    MOVE"|      |                                            |" TO Trackpart4.
    MOVE"|      |                                            |" TO Trackpart5.
    MOVE"|      |                                            |" TO Trackpart6.
    MOVE"|      |                                            |" TO Trackpart7.
    MOVE"|      |                                            |" TO Trackpart8.
    MOVE"|      |     _____________________________     _____|" TO Trackpart9.
    MOVE"|      |     |                           |     |     " TO Trackpart10.
    MOVE"|      |     |                           |     |     " TO Trackpart11.
    MOVE"|      |     |                           |     |     " TO Trackpart12.
    MOVE"|      |     |                           |     |     " TO Trackpart13.
    MOVE"|      |     |                           |     |     " TO Trackpart14.
    MOVE"|      |     |             STOP1         |     |     " TO Trackpart15.
    MOVE"|      |     |___________________        |     |_____" TO Trackpart16.
    MOVE"|STOP2 |                   _____|        |          |" TO Trackpart17.
    MOVE"|      |                   |xxx||        |          |" TO Trackpart18.
    MOVE"|      |                   |x x||        |          |" TO Trackpart19.
    MOVE"|      |                   |xxx||        |          |" TO Trackpart20.
    MOVE"|      |                   |  x||        |          |" TO Trackpart21.
    MOVE"|      |                   |xxx||        |          |" TO Trackpart22.
    MOVE"|      |                   |___||        |          |" TO Trackpart23.
    MOVE"|______|________________________|        |__________|" TO Trackpart24.
    DISPLAY Elevatroscreen.
    ACCEPT Elevatroscreen.
    MOVE 28 TO Posnum.
    PERFORM Elevatroupdate.
    DISPLAY Elevatroscreen.
    ACCEPT Elevatroscreen.
    Elevatroupdate.
    *>IF Stopnumber = 2 THEN PERFORM Leftsidemoveprog 19 TIMES.
    IF Stopnumber = 2 THEN PERFORM Movestart.
    Leftsidemoveprog.
    MOVE Trackpart17(Posnum:5) TO Trackpart17(Posnum - 1:5).
    MOVE Trackpart18(Posnum:5) TO Trackpart18(Posnum - 1:5).
    MOVE Trackpart19(Posnum:5) TO Trackpart19(Posnum - 1:5).
    MOVE Trackpart20(Posnum:5) TO Trackpart20(Posnum - 1:5).
    MOVE Trackpart21(Posnum:5) TO Trackpart21(Posnum - 1:5).
    MOVE Trackpart22(Posnum:5) TO Trackpart22(Posnum - 1:5).
    MOVE Trackpart23(Posnum:5) TO Trackpart23(Posnum - 1:5).
    MOVE " " TO Trackpart17(Posnum + 4:1).
    MOVE " " TO Trackpart18(Posnum + 4:1).
    MOVE " " TO Trackpart19(Posnum + 4:1).
    MOVE " " TO Trackpart20(Posnum + 4:1).
    MOVE " " TO Trackpart21(Posnum + 4:1).
    MOVE " " TO Trackpart22(Posnum + 4:1).
    MOVE " " TO Trackpart23(Posnum + 4:1).
    SUBTRACT 1 FROM Posnum.
    DISPLAY Elevatroscreen.
    ACCEPT OMITTED.
    Movestart.
    MOVE 28 TO Posnumold.
    MOVE 27 TO Posnumnew.
    MOVE 5 TO Partlen.
    PERFORM Posupdateprog 19 TIMES.
    Posupdateprog.
    MOVE 17 TO Rownum.
    PERFORM Horizontalmoveprog 7 TIMES.
    MOVE 17 TO Rownum.
    PERFORM Horizontalresetprog 7 TIMES.
    DISPLAY Elevatroscreen.
    ACCEPT OMITTED.
    SUBTRACT 1 FROM Posnumold.
    SUBTRACT 1 FROM Posnumnew.
    Horizontalmoveprog.
    EVALUATE Rownum
    WHEN 1 MOVE Trackpart1(Posnumold:Partlen) TO Trackpart1(Posnumnew:Partlen)
    WHEN 2 MOVE Trackpart2(Posnumold:Partlen) TO Trackpart2(Posnumnew:Partlen)
    WHEN 3 MOVE Trackpart3(Posnumold:Partlen) TO Trackpart3(Posnumnew:Partlen)
    WHEN 4 MOVE Trackpart4(Posnumold:Partlen) TO Trackpart4(Posnumnew:Partlen)
    WHEN 5 MOVE Trackpart5(Posnumold:Partlen) TO Trackpart5(Posnumnew:Partlen)
    WHEN 6 MOVE Trackpart6(Posnumold:Partlen) TO Trackpart6(Posnumnew:Partlen)
    WHEN 7 MOVE Trackpart7(Posnumold:Partlen) TO Trackpart7(Posnumnew:Partlen)
    WHEN 8 MOVE Trackpart8(Posnumold:Partlen) TO Trackpart8(Posnumnew:Partlen)
    WHEN 9 MOVE Trackpart9(Posnumold:Partlen) TO Trackpart9(Posnumnew:Partlen)
    WHEN 10 MOVE Trackpart10(Posnumold:Partlen) TO Trackpart10(Posnumnew:Partlen)
    WHEN 11 MOVE Trackpart11(Posnumold:Partlen) TO Trackpart11(Posnumnew:Partlen)
    WHEN 12 MOVE Trackpart12(Posnumold:Partlen) TO Trackpart12(Posnumnew:Partlen)
    WHEN 13 MOVE Trackpart13(Posnumold:Partlen) TO Trackpart13(Posnumnew:Partlen)
    WHEN 14 MOVE Trackpart14(Posnumold:Partlen) TO Trackpart14(Posnumnew:Partlen)
    WHEN 15 MOVE Trackpart15(Posnumold:Partlen) TO Trackpart15(Posnumnew:Partlen)
    WHEN 16 MOVE Trackpart16(Posnumold:Partlen) TO Trackpart16(Posnumnew:Partlen)
    WHEN 17 MOVE Trackpart17(Posnumold:Partlen) TO Trackpart17(Posnumnew:Partlen)
    WHEN 18 MOVE Trackpart18(Posnumold:Partlen) TO Trackpart18(Posnumnew:Partlen)
    WHEN 19 MOVE Trackpart19(Posnumold:Partlen) TO Trackpart19(Posnumnew:Partlen)
    WHEN 20 MOVE Trackpart20(Posnumold:Partlen) TO Trackpart20(Posnumnew:Partlen)
    WHEN 21 MOVE Trackpart21(Posnumold:Partlen) TO Trackpart21(Posnumnew:Partlen)
    WHEN 22 MOVE Trackpart22(Posnumold:Partlen) TO Trackpart22(Posnumnew:Partlen)
    WHEN 23 MOVE Trackpart23(Posnumold:Partlen) TO Trackpart23(Posnumnew:Partlen)
    WHEN 24 MOVE Trackpart24(Posnumold:Partlen) TO Trackpart24(Posnumnew:Partlen)
    END-EVALUATE.
    ADD 1 TO Rownum.
    Horizontalresetprog.
    EVALUATE Rownum
    WHEN 1 MOVE Emptyrow(Posnumold + Partlen - 1:1) TO Trackpart1(Posnumold + Partlen - 1:1)
    WHEN 2 MOVE Emptyrow(Posnumold + Partlen - 1:1) TO Trackpart2(Posnumold + Partlen - 1:1)
    WHEN 3 MOVE Emptyrow(Posnumold + Partlen - 1:1) TO Trackpart3(Posnumold + Partlen - 1:1)
    WHEN 4 MOVE Emptyrow(Posnumold + Partlen - 1:1) TO Trackpart4(Posnumold + Partlen - 1:1)
    WHEN 5 MOVE Emptyrow(Posnumold + Partlen - 1:1) TO Trackpart5(Posnumold + Partlen - 1:1)
    WHEN 6 MOVE Emptyrow(Posnumold + Partlen - 1:1) TO Trackpart6(Posnumold + Partlen - 1:1)
    WHEN 7 MOVE Emptyrow(Posnumold + Partlen - 1:1) TO Trackpart7(Posnumold + Partlen - 1:1)
    WHEN 8 MOVE Emptyrow(Posnumold + Partlen - 1:1) TO Trackpart8(Posnumold + Partlen - 1:1)
    WHEN 9 MOVE Emptyrow(Posnumold + Partlen - 1:1) TO Trackpart9(Posnumold + Partlen - 1:1)
    WHEN 10 MOVE Emptyrow(Posnumold + Partlen - 1:1) TO Trackpart10(Posnumold + Partlen - 1:1)
    WHEN 11 MOVE Emptyrow(Posnumold + Partlen - 1:1) TO Trackpart11(Posnumold + Partlen - 1:1)
    WHEN 12 MOVE Emptyrow(Posnumold + Partlen - 1:1) TO Trackpart12(Posnumold + Partlen - 1:1)
    WHEN 13 MOVE Emptyrow(Posnumold + Partlen - 1:1) TO Trackpart13(Posnumold + Partlen - 1:1)
    WHEN 14 MOVE Emptyrow(Posnumold + Partlen - 1:1) TO Trackpart14(Posnumold + Partlen - 1:1)
    WHEN 15 MOVE Emptyrow(Posnumold + Partlen - 1:1) TO Trackpart15(Posnumold + Partlen - 1:1)
    WHEN 16 MOVE Emptyrow(Posnumold + Partlen - 1:1) TO Trackpart16(Posnumold + Partlen - 1:1)
    WHEN 17 MOVE Emptyrow(Posnumold + Partlen - 1:1) TO Trackpart17(Posnumold + Partlen - 1:1)
    WHEN 18 MOVE Emptyrow(Posnumold + Partlen - 1:1) TO Trackpart18(Posnumold + Partlen - 1:1)
    WHEN 19 MOVE Emptyrow(Posnumold + Partlen - 1:1) TO Trackpart19(Posnumold + Partlen - 1:1)
    WHEN 20 MOVE Emptyrow(Posnumold + Partlen - 1:1) TO Trackpart20(Posnumold + Partlen - 1:1)
    WHEN 21 MOVE Emptyrow(Posnumold + Partlen - 1:1) TO Trackpart21(Posnumold + Partlen - 1:1)
    WHEN 22 MOVE Emptyrow(Posnumold + Partlen - 1:1) TO Trackpart22(Posnumold + Partlen - 1:1)
    WHEN 23 MOVE Emptyrow(Posnumold + Partlen - 1:1) TO Trackpart23(Posnumold + Partlen - 1:1)
    WHEN 24 MOVE Emptyrow(Posnumold + Partlen - 1:1) TO Trackpart24(Posnumold + Partlen - 1:1)
    END-EVALUATE.
    ADD 1 TO Rownum.
    Railnetworkprog.
    MOVE " " TO Optval.
    PERFORM Trackinit.
    PERFORM Switchupdateprog.
    DISPLAY Trackscreen.
    ACCEPT Trackscreen.
    IF Optval = "x" THEN PERFORM Switchsubprog.
    Switchsubprog.
    DISPLAY Switchscreen.
    ACCEPT Switchscreen.
    PERFORM Switchupdateprog.
    IF Optval = "x" THEN PERFORM Railnetworkprog ELSE PERFORM Switchsubprog.
    Trackinit.
    MOVE"_______________________________________________________________________________________________________" TO Trackpart1.
    MOVE"          \                                                                                            " TO Trackpart2.
    MOVE"           \____________________________________________________                                       " TO Trackpart3.
    MOVE"               \                                                                                       " TO Trackpart4.
    MOVE"                \_______________________________________________                                       " TO Trackpart5.
    MOVE"                           /                                                                           " TO Trackpart6.
    MOVE"                          /                                                                            " TO Trackpart7.
    MOVE"   _____________________ /                                                                             " TO Trackpart8.
    MOVE"                        /                                                                              " TO Trackpart9.
    MOVE"   ___________________ /                                                                               " TO Trackpart10.
    MOVE"                      /                                                                                " TO Trackpart11.
    MOVE"   _________________ /                                                                                 " TO Trackpart12.
    MOVE"                    /                                                                                  " TO Trackpart13.
    MOVE"   _______________ /                                                                                   " TO Trackpart14.
    MOVE"                  /                                                                                    " TO Trackpart15.
    MOVE"   ______________/                                                                                     " TO Trackpart16.
    MOVE"              /                                                                                        " TO Trackpart17.
    MOVE"   __________/                                                                                         " TO Trackpart18.
    MOVE"        \_______________________________________                                                       " TO Trackpart19.
    Switchupdateprog.
    IF Switchval1 EQUAL TO 1 THEN MOVE "_" TO Trackpart1(11:1) ELSE MOVE " " TO Trackpart1(11:1).
    IF Switchval1 EQUAL TO 0 THEN MOVE "\" TO Trackpart2(11:1) ELSE MOVE " " TO Trackpart2(11:1).
    IF Switchval2 EQUAL TO 1 THEN MOVE "_" TO Trackpart3(16:1) ELSE MOVE " " TO Trackpart3(16:1).
    IF Switchval2 EQUAL TO 0 THEN MOVE "\" TO Trackpart4(16:1) ELSE MOVE " " TO Trackpart4(16:1).
    IF Switchval3 EQUAL TO 1 THEN MOVE "_" TO Trackpart5(28:1) ELSE MOVE " " TO Trackpart5(28:1).
    IF Switchval3 EQUAL TO 0 THEN MOVE "/" TO Trackpart6(28:1) ELSE MOVE " " TO Trackpart6(28:1).
    IF Switchval4 EQUAL TO 1 THEN MOVE "_" TO Trackpart8(25:1) ELSE MOVE " " TO Trackpart8(25:1).
    IF Switchval4 EQUAL TO 0 THEN MOVE "/" TO Trackpart9(25:1) ELSE MOVE " " TO Trackpart9(25:1).
    IF Switchval5 EQUAL TO 1 THEN MOVE "_" TO Trackpart10(23:1) ELSE MOVE " " TO Trackpart10(23:1).
    IF Switchval5 EQUAL TO 0 THEN MOVE "/" TO Trackpart11(23:1) ELSE MOVE " " TO Trackpart11(23:1).
    IF Switchval6 EQUAL TO 1 THEN MOVE "_" TO Trackpart12(21:1) ELSE MOVE " " TO Trackpart12(21:1).
    IF Switchval6 EQUAL TO 0 THEN MOVE "/" TO Trackpart13(21:1) ELSE MOVE " " TO Trackpart13(21:1).
    IF Switchval7 EQUAL TO 1 THEN MOVE "_" TO Trackpart14(19:1) ELSE MOVE " " TO Trackpart14(19:1).
    IF Switchval7 EQUAL TO 0 THEN MOVE "/" TO Trackpart15(19:1) ELSE MOVE " " TO Trackpart15(19:1).
    IF Switchval8 EQUAL TO 1 THEN MOVE "_" TO Trackpart16(15:1) ELSE MOVE " " TO Trackpart16(15:1).
    IF Switchval8 EQUAL TO 0 THEN MOVE "/" TO Trackpart17(15:1) ELSE MOVE " " TO Trackpart17(15:1).
    IF Switchval9 EQUAL TO 1 THEN MOVE "_" TO Trackpart18(9:1) ELSE MOVE " " TO Trackpart18(9:1).
    IF Switchval9 EQUAL TO 0 THEN MOVE "\" TO Trackpart19(9:1) ELSE MOVE " " TO Trackpart19(9:1).
    start-fifteen.
    MOVE " " TO Optval.
    MOVE "_" TO Optvalinput_2.
    *>display 'start fifteen puzzle'.
    *>display '    enter a two-digit tile number and press <enter> to move'.
    *>display '    press <enter> only to exit'.
    *> tables with an odd number of inversions are not solvable
    perform initialize-table with test after until inversions = 0.
    *>PERFORM Tableinitalt.
    *>perform show-table.
    *>accept display-move
    *>perform until display-move = spaces
    *>   perform move-tile
    *>    perform show-table
    *>   move spaces to display-move
    *>   accept display-move
    *>end-perform
    PERFORM Displaysetupprog.
    *>DISPLAY display-cell(2,1).
    DISPLAY Celladressoutput.
    ACCEPT Celladressoutput.
    PERFORM Tablemodprog UNTIL Returnvalinput = "x".
    IF Returnvalinput = "x" THEN PERFORM Gamesaveprog.
    Gamesaveprog.
    DISPLAY Gamesavescreen.
    ACCEPT Gamesavescreen.
    IF Gamesavevar = "n" THEN PERFORM Gamestartsubprog.
    IF Gamesavevar = "j" THEN DISPLAY Gamesavedetailscreen3.
    IF Gamesavevar = "j" THEN ACCEPT Gamesavedetailscreen3.
    Gamestartsubprog.
    MOVE " " TO Gamesavevar.
    MOVE " " TO Optval.
    MOVE "_" TO Optvalinput_Start.
    PERFORM Gamestartprog.
    Displaysetupprog.
    MOVE display-cell(1,1) TO Inputoptionval1.
    MOVE display-cell(1,2) TO Inputoptionval2.
    MOVE display-cell(1,3) TO Inputoptionval3.
    MOVE display-cell(1,4) TO Inputoptionval4.
    MOVE display-cell(2,1) TO Inputoptionval5.
    MOVE display-cell(2,2) TO Inputoptionval6.
    MOVE display-cell(2,3) TO Inputoptionval7.
    MOVE display-cell(2,4) TO Inputoptionval8.
    MOVE display-cell(3,1) TO Inputoptionval9.
    MOVE display-cell(3,2) TO Inputoptionval10.
    MOVE display-cell(3,3) TO Inputoptionval11.
    MOVE display-cell(3,4) TO Inputoptionval12.
    MOVE display-cell(4,1) TO Inputoptionval13.
    MOVE display-cell(4,2) TO Inputoptionval14.
    MOVE display-cell(4,3) TO Inputoptionval15.
    MOVE display-cell(4,4) TO Inputoptionval16.
    IF Inputoptionval1 = "00" THEN MOVE 1 TO Posnum.
    IF Inputoptionval2 = "00" THEN MOVE 2 TO Posnum.
    IF Inputoptionval3 = "00" THEN MOVE 3 TO Posnum.
    IF Inputoptionval4 = "00" THEN MOVE 4 TO Posnum.
    IF Inputoptionval5 = "00" THEN MOVE 5 TO Posnum.
    IF Inputoptionval6 = "00" THEN MOVE 6 TO Posnum.
    IF Inputoptionval7 = "00" THEN MOVE 7 TO Posnum.
    IF Inputoptionval8 = "00" THEN MOVE 8 TO Posnum.
    IF Inputoptionval9 = "00" THEN MOVE 9 TO Posnum.
    IF Inputoptionval10 = "00" THEN MOVE 10 TO Posnum.
    IF Inputoptionval11 = "00" THEN MOVE 11 TO Posnum.
    IF Inputoptionval12 = "00" THEN MOVE 12 TO Posnum.
    IF Inputoptionval13 = "00" THEN MOVE 13 TO Posnum.
    IF Inputoptionval14 = "00" THEN MOVE 14 TO Posnum.
    IF Inputoptionval15 = "00" THEN MOVE 15 TO Posnum.
    IF Inputoptionval16 = "00" THEN MOVE 16 TO Posnum.
    IF Inputoptionval1 = "00" THEN MOVE " " TO Inputoptionval1.
    IF Inputoptionval2 = "00" THEN MOVE " " TO Inputoptionval2.
    IF Inputoptionval3 = "00" THEN MOVE " " TO Inputoptionval3.
    IF Inputoptionval4 = "00" THEN MOVE " " TO Inputoptionval4.
    IF Inputoptionval5 = "00" THEN MOVE " " TO Inputoptionval5.
    IF Inputoptionval6 = "00" THEN MOVE " " TO Inputoptionval6.
    IF Inputoptionval7 = "00" THEN MOVE " " TO Inputoptionval7.
    IF Inputoptionval8 = "00" THEN MOVE " " TO Inputoptionval8.
    IF Inputoptionval9 = "00" THEN MOVE " " TO Inputoptionval9.
    IF Inputoptionval10 = "00" THEN MOVE " " TO Inputoptionval10.
    IF Inputoptionval11 = "00" THEN MOVE " " TO Inputoptionval11.
    IF Inputoptionval12 = "00" THEN MOVE " " TO Inputoptionval12.
    IF Inputoptionval13 = "00" THEN MOVE " " TO Inputoptionval13.
    IF Inputoptionval14 = "00" THEN MOVE " " TO Inputoptionval14.
    IF Inputoptionval15 = "00" THEN MOVE " " TO Inputoptionval15.
    IF Inputoptionval16 = "00" THEN MOVE " " TO Inputoptionval16.
    *>MOVE Posnum TO Movevalinput.
    Tableinitalt.
    MOVE "08" TO Inputoptionval1.
    MOVE "05" TO Inputoptionval2.
    MOVE "13" TO Inputoptionval3.
    MOVE "02" TO Inputoptionval4.
    MOVE "12" TO Inputoptionval5.
    MOVE "07" TO Inputoptionval6.
    MOVE "00" TO Inputoptionval7.
    MOVE "06" TO Inputoptionval8.
    MOVE "01" TO Inputoptionval9.
    MOVE "03" TO Inputoptionval10.
    MOVE "11" TO Inputoptionval11.
    MOVE "14" TO Inputoptionval12.
    MOVE "13" TO Inputoptionval13.
    MOVE "04" TO Inputoptionval14.
    MOVE "10" TO Inputoptionval15.
    MOVE "15" TO Inputoptionval16.
    Tablemodprog.
    Evaluate Posnum
    WHEN 1 PERFORM Valprog1
    WHEN 2 PERFORM Valprog2
    WHEN 3 PERFORM Valprog3
    WHEN 4 PERFORM Valprog4
    WHEN 5 PERFORM Valprog5
    WHEN 6 PERFORM Valprog6
    WHEN 7 PERFORM Valprog7
    WHEN 8 PERFORM Valprog8
    WHEN 9 PERFORM Valprog9
    WHEN 10 PERFORM Valprog10
    WHEN 11 PERFORM Valprog11
    WHEN 12 PERFORM Valprog12
    WHEN 13 PERFORM Valprog13
    WHEN 14 PERFORM Valprog14
    WHEN 15 PERFORM Valprog15
    WHEN 16 PERFORM Valprog16
    END-EVALUATE.
    MOVE Inputoptionval1 TO Inputoption1.
    MOVE Inputoptionval2 TO Inputoption2.
    MOVE Inputoptionval3 TO Inputoption3.
    MOVE Inputoptionval4 TO Inputoption4.
    MOVE Inputoptionval5 TO Inputoption5.
    MOVE Inputoptionval6 TO Inputoption6.
    MOVE Inputoptionval7 TO Inputoption7.
    MOVE Inputoptionval8 TO Inputoption8.
    MOVE Inputoptionval9 TO Inputoption9.
    MOVE Inputoptionval10 TO Inputoption10.
    MOVE Inputoptionval11 TO Inputoption11.
    MOVE Inputoptionval12 TO Inputoption12.
    MOVE Inputoptionval13 TO Inputoption13.
    MOVE Inputoptionval14 TO Inputoption14.
    MOVE Inputoptionval15 TO Inputoption15.
    MOVE Inputoptionval16 TO Inputoption16.
    DISPLAY Celladressoutput.
    ACCEPT Celladressoutput.
    Valprog1_A.
    Evaluate Moveval
    WHEN Inputoptionval2 MOVE Inputoptionval2 TO Inputoptionval1
    WHEN Inputoptionval2 MOVE " " TO Inputoptionval2
    WHEN Inputoptionval5 MOVE Inputoptionval5 TO Inputoptionval1
    WHEN Inputoptionval5 MOVE " " TO Inputoptionval5
    END-EVALUATE.
    Valprog1.
    EVALUATE Moveval
    WHEN Inputoptionval2 PERFORM Inputprog1
    WHEN Inputoptionval5 PERFORM Inputprog2
    END-EVALUATE.
    Valprog2.
    EVALUATE Moveval
    WHEN Inputoptionval1 PERFORM Inputprog3
    WHEN Inputoptionval3 PERFORM Inputprog4
    WHEN Inputoptionval6 PERFORM Inputprog5
    END-EVALUATE.
    Valprog3.
    EVALUATE Moveval
    WHEN Inputoptionval2 PERFORM Inputprog6
    WHEN Inputoptionval4 PERFORM Inputprog7
    WHEN Inputoptionval7 PERFORM Inputprog8
    END-EVALUATE.
    Valprog4.
    EVALUATE Moveval
    WHEN Inputoptionval3 PERFORM Inputprog9
    WHEN Inputoptionval8 PERFORM Inputprog10
    END-EVALUATE.
    Valprog5.
    EVALUATE Moveval
    WHEN Inputoptionval6 PERFORM Inputprog11
    WHEN Inputoptionval1 PERFORM Inputprog12
    WHEN Inputoptionval9 PERFORM Inputprog13
    END-EVALUATE.
    Valprog6.
    EVALUATE Moveval
    WHEN Inputoptionval5 PERFORM Inputprog14
    WHEN Inputoptionval7 PERFORM Inputprog15
    WHEN Inputoptionval2 PERFORM Inputprog16
    WHEN Inputoptionval10 PERFORM Inputprog17
    END-EVALUATE.
    Valprog7.
    EVALUATE Moveval
    WHEN Inputoptionval6 PERFORM Inputprog18
    WHEN Inputoptionval8 PERFORM Inputprog19
    WHEN Inputoptionval3 PERFORM Inputprog20
    WHEN Inputoptionval11 PERFORM Inputprog21
    END-EVALUATE.
    Valprog8.
    EVALUATE Moveval
    WHEN Inputoptionval7 PERFORM Inputprog22
    WHEN Inputoptionval4 PERFORM Inputprog23
    WHEN Inputoptionval12 PERFORM Inputprog24
    END-EVALUATE.
    Valprog9.
    EVALUATE Moveval
    WHEN Inputoptionval10 PERFORM Inputprog25
    WHEN Inputoptionval5 PERFORM Inputprog26
    WHEN Inputoptionval13 PERFORM Inputprog27
    END-EVALUATE.
    Valprog10.
    EVALUATE Moveval
    WHEN Inputoptionval9 PERFORM Inputprog28
    WHEN Inputoptionval11 PERFORM Inputprog29
    WHEN Inputoptionval6 PERFORM Inputprog30
    WHEN Inputoptionval14 PERFORM Inputprog31
    END-EVALUATE.
    Valprog11.
    EVALUATE Moveval
    WHEN Inputoptionval10 PERFORM Inputprog32
    WHEN Inputoptionval12 PERFORM Inputprog33
    WHEN Inputoptionval7 PERFORM Inputprog34
    WHEN Inputoptionval15 PERFORM Inputprog35
    END-EVALUATE.
    Valprog12.
    EVALUATE Moveval
    WHEN Inputoptionval11 PERFORM Inputprog36
    WHEN Inputoptionval8 PERFORM Inputprog37
    WHEN Inputoptionval16 PERFORM Inputprog38
    END-EVALUATE.
    Valprog13.
    EVALUATE Moveval
    WHEN Inputoptionval14 PERFORM Inputprog39
    WHEN Inputoptionval9 PERFORM Inputprog40
    END-EVALUATE.
    Valprog14.
    EVALUATE Moveval
    WHEN Inputoptionval13 PERFORM Inputprog41
    WHEN Inputoptionval15 PERFORM Inputprog42
    WHEN Inputoptionval10 PERFORM Inputprog43
    END-EVALUATE.
    Valprog15.
    EVALUATE Moveval
    WHEN Inputoptionval14 PERFORM Inputprog44
    WHEN Inputoptionval16 PERFORM Inputprog45
    WHEN Inputoptionval11 PERFORM Inputprog46
    END-EVALUATE.
    Valprog16.
    EVALUATE Moveval
    WHEN Inputoptionval15 PERFORM Inputprog47
    WHEN Inputoptionval12 PERFORM Inputprog48
    END-EVALUATE.
    Inputprog1.
    MOVE Inputoptionval2 TO Inputoptionval1.
    MOVE " " TO Inputoptionval2.
    MOVE 2 TO Posnum.
    Inputprog2.
    MOVE Inputoptionval5 TO Inputoptionval1.
    MOVE " " TO Inputoptionval5.
    MOVE 5 TO Posnum.
    Inputprog3.
    MOVE Inputoptionval1 TO Inputoptionval2.
    MOVE " " TO Inputoptionval1.
    MOVE 1 TO Posnum.
    Inputprog4.
    MOVE Inputoptionval3 TO Inputoptionval2.
    MOVE " " TO Inputoptionval3.
    MOVE 3 TO Posnum.
    Inputprog5.
    MOVE Inputoptionval6 TO Inputoptionval2.
    MOVE " " TO Inputoptionval6.
    MOVE 6 TO Posnum.
    Inputprog6.
    MOVE Inputoptionval2 TO Inputoptionval3.
    MOVE " " TO Inputoptionval2.
    MOVE 2 TO Posnum.
    Inputprog7.
    MOVE Inputoptionval4 TO Inputoptionval3.
    MOVE " " TO Inputoptionval4.
    MOVE 4 TO Posnum.
    Inputprog8.
    MOVE Inputoptionval7 TO Inputoptionval3.
    MOVE " " TO Inputoptionval7.
    MOVE 7 TO Posnum.
    Inputprog9.
    MOVE Inputoptionval3 TO Inputoptionval4.
    MOVE " " TO Inputoptionval3.
    MOVE 3 TO Posnum.
    Inputprog10.
    MOVE Inputoptionval8 TO Inputoptionval4.
    MOVE " " TO Inputoptionval8.
    MOVE 8 TO Posnum.
    Inputprog11.
    MOVE Inputoptionval6 TO Inputoptionval5.
    MOVE " " TO Inputoptionval6.
    MOVE 6 TO Posnum.
    Inputprog12.
    MOVE Inputoptionval1 TO Inputoptionval5.
    MOVE " " TO Inputoptionval1.
    MOVE 1 TO Posnum.
    Inputprog13.
    MOVE Inputoptionval9 TO Inputoptionval5.
    MOVE " " TO Inputoptionval9.
    MOVE 9 TO Posnum.
    Inputprog14.
    MOVE Inputoptionval5 TO Inputoptionval6.
    MOVE " " TO Inputoptionval5.
    MOVE 5 TO Posnum.
    Inputprog15.
    MOVE Inputoptionval7 TO Inputoptionval6.
    MOVE " " TO Inputoptionval7.
    MOVE 7 TO Posnum.
    Inputprog16.
    MOVE Inputoptionval2 TO Inputoptionval6.
    MOVE " " TO Inputoptionval2.
    MOVE 2 TO Posnum.
    Inputprog17.
    MOVE Inputoptionval10 TO Inputoptionval6.
    MOVE " " TO Inputoptionval10.
    MOVE 10 TO Posnum.
    Inputprog18.
    MOVE Inputoptionval6 TO Inputoptionval7.
    MOVE " " TO Inputoptionval6.
    MOVE 6 TO Posnum.
    Inputprog19.
    MOVE Inputoptionval8 TO Inputoptionval7.
    MOVE " " TO Inputoptionval8.
    MOVE 8 TO Posnum.
    Inputprog20.
    MOVE Inputoptionval3 TO Inputoptionval7.
    MOVE " " TO Inputoptionval3.
    MOVE 3 TO Posnum.
    Inputprog21.
    MOVE Inputoptionval11 TO Inputoptionval7.
    MOVE " " TO Inputoptionval11.
    MOVE 11 TO Posnum.
    Inputprog22.
    MOVE Inputoptionval7 TO Inputoptionval8.
    MOVE " " TO Inputoptionval7.
    MOVE 7 TO Posnum.
    Inputprog23.
    MOVE Inputoptionval4 TO Inputoptionval8.
    MOVE " " TO Inputoptionval4.
    MOVE 4 TO Posnum.
    Inputprog24.
    MOVE Inputoptionval12 TO Inputoptionval8.
    MOVE " " TO Inputoptionval12.
    MOVE 12 TO Posnum.
    Inputprog25.
    MOVE Inputoptionval10 TO Inputoptionval9.
    MOVE " " TO Inputoptionval10.
    MOVE 10 TO Posnum.
    Inputprog26.
    MOVE Inputoptionval5 TO Inputoptionval9.
    MOVE " " TO Inputoptionval5.
    MOVE 5 TO Posnum.
    Inputprog27.
    MOVE Inputoptionval13 TO Inputoptionval9.
    MOVE " " TO Inputoptionval13.
    MOVE 13 TO Posnum.
    Inputprog28.
    MOVE Inputoptionval9 TO Inputoptionval10.
    MOVE " " TO Inputoptionval9.
    MOVE 9 TO Posnum.
    Inputprog29.
    MOVE Inputoptionval11 TO Inputoptionval10.
    MOVE " " TO Inputoptionval11.
    MOVE 11 TO Posnum.
    Inputprog30.
    MOVE Inputoptionval6 TO Inputoptionval10.
    MOVE " " TO Inputoptionval6.
    MOVE 6 TO Posnum.
    Inputprog31.
    MOVE Inputoptionval14 TO Inputoptionval10.
    MOVE " " TO Inputoptionval14.
    MOVE 14 TO Posnum.
    Inputprog32.
    MOVE Inputoptionval10 TO Inputoptionval11.
    MOVE " " TO Inputoptionval10.
    MOVE 10 TO Posnum.
    Inputprog33.
    MOVE Inputoptionval12 TO Inputoptionval11.
    MOVE " " TO Inputoptionval12.
    MOVE 12 TO Posnum.
    Inputprog34.
    MOVE Inputoptionval7 TO Inputoptionval11.
    MOVE " " TO Inputoptionval7.
    MOVE 7 TO Posnum.
    Inputprog35.
    MOVE Inputoptionval15 TO Inputoptionval11.
    MOVE " " TO Inputoptionval15.
    MOVE 15 TO Posnum.
    Inputprog36.
    MOVE Inputoptionval11 TO Inputoptionval12.
    MOVE " " TO Inputoptionval11.
    MOVE 11 TO Posnum.
    Inputprog37.
    MOVE Inputoptionval8 TO Inputoptionval12.
    MOVE " " TO Inputoptionval8.
    MOVE 8 TO Posnum.
    Inputprog38.
    MOVE Inputoptionval16 TO Inputoptionval12.
    MOVE " " TO Inputoptionval16.
    MOVE 16 TO Posnum.
    Inputprog39.
    MOVE Inputoptionval14 TO Inputoptionval13.
    MOVE " " TO Inputoptionval14.
    MOVE 14 TO Posnum.
    Inputprog40.
    MOVE Inputoptionval9 TO Inputoptionval13.
    MOVE " " TO Inputoptionval9.
    MOVE 9 TO Posnum.
    Inputprog41.
    MOVE Inputoptionval13 TO Inputoptionval14.
    MOVE " " TO Inputoptionval13.
    MOVE 13 TO Posnum.
    Inputprog42.
    MOVE Inputoptionval15 TO Inputoptionval14.
    MOVE " " TO Inputoptionval15.
    MOVE 15 TO Posnum.
    Inputprog43.
    MOVE Inputoptionval10 TO Inputoptionval14.
    MOVE " " TO Inputoptionval10.
    MOVE 10 TO Posnum.
    Inputprog44.
    MOVE Inputoptionval14 TO Inputoptionval15.
    MOVE " " TO Inputoptionval14.
    MOVE 14 TO Posnum.
    Inputprog45.
    MOVE Inputoptionval16 TO Inputoptionval15.
    MOVE " " TO Inputoptionval16.
    MOVE 16 TO Posnum.
    Inputprog46.
    MOVE Inputoptionval11 TO Inputoptionval15.
    MOVE " " TO Inputoptionval11.
    MOVE 11 TO Posnum.
    Inputprog47.
    MOVE Inputoptionval15 TO Inputoptionval16.
    MOVE " " TO Inputoptionval15.
    MOVE 15 TO Posnum.
    Inputprog48.
    MOVE Inputoptionval12 TO Inputoptionval16.
    MOVE " " TO Inputoptionval12.
    MOVE 12 TO Posnum.
    initialize-table.
    compute tile-number = random(seconds-past-midnight) *> seed only
    move spaces to tile-flags
    move 0 to current-tile inversions
    perform varying r from 1 by 1 until r > 4
    after c from 1 by 1 until c > 4
        perform with test after
        until tile-flags(tile-number + 1:1) = space
            compute tile-number = random() * 100
            compute tile-number = mod(tile-number, 16)
        end-perform
        move 'x' to tile-flags(tile-number + 1:1)
        if tile-number > 0 and < current-tile
            add 1 to inversions
        end-if
        move tile-number to display-cell(r,c) current-tile
    end-perform
    compute inversions = mod(inversions,2)
    .
show-table.
    if display-table = winning-display
        display 'winning'
    end-if
    display space row-separator
    perform varying r from 1 by 1 until r > 4
        perform varying c from 1 by 1 until c > 4
            display column-separator with no advancing
            if display-cell(r,c) = 00
                display '  ' with no advancing
                move r to r-empty
                move c to c-empty
            else
                display display-cell(r,c) with no advancing
            end-if
        end-perform
        display column-separator
    end-perform
    display space row-separator
    .
move-tile.
    if not (tile-id numeric and tile-id >= 01 and <= 15)
        display 'invalid tile number'
        exit paragraph
    end-if

    *> find the entered tile-id row and column (r,c)
    perform varying r from 1 by 1 until r > 4
    after c from 1 by 1 until c > 4
        if display-cell(r,c) = tile-id
            exit perform
        end-if
    end-perform

    *> show-table filled (r-empty,c-empty)
    evaluate true
    when r = r-empty
        if c-empty < c
            *> shift left
            perform varying c-to from c-empty by 1 until c-to > c
                compute c-from = c-to + 1
                move display-cell(r-empty,c-from) to display-cell(r-empty,c-to)
            end-perform
        else
           *> shift right
           perform varying c-to from c-empty by -1 until c-to < c
               compute c-from = c-to - 1
               move display-cell(r-empty,c-from) to display-cell(r-empty,c-to)
           end-perform
       end-if
       move 00 to display-cell(r,c)
    when c = c-empty
        if r-empty < r
            *>shift up
            perform varying r-to from r-empty by 1 until r-to > r
                compute r-from = r-to + 1
                move display-cell(r-from,c-empty) to display-cell(r-to,c-empty)
            end-perform
        else
            *> shift down
            perform varying r-to from r-empty by -1 until r-to < r
                compute r-from = r-to - 1
                move display-cell(r-from,c-empty) to display-cell(r-to,c-empty)
            end-perform
        end-if
        move 00 to display-cell(r,c)
    when other
         display 'invalid move'
    end-evaluate
    .
end program fifteen.
