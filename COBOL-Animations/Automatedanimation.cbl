*> This code is dedicated to the public domain
*> This is GNUCOBOL 2.0
identification division.
program-id. Animation.
environment division.
data division.
working-storage section.
   01 Emptyrow PICTURE X(120).
   01 Trackpart1 PICTURE X(120).
   01 Trackpart2 PICTURE X(120).
   01 Trackpart3 PICTURE X(120).
   01 Trackpart4 PICTURE X(120).
   01 Trackpart5 PICTURE X(120).
   01 Trackpart6 PICTURE X(120).
   01 Trackpart7 PICTURE X(120).
   01 Trackpart8 PICTURE X(120).
   01 Trackpart9 PICTURE X(120).
   01 Trackpart10 PICTURE X(120).
   01 Trackpart11 PICTURE X(120).
   01 Trackpart12 PICTURE X(120).
   01 Trackpart13 PICTURE X(120).
   01 Trackpart14 PICTURE X(120).
   01 Trackpart15 PICTURE X(120).
   01 Trackpart16 PICTURE X(120).
   01 Trackpart17 PICTURE X(120).
   01 Trackpart18 PICTURE X(120).
   01 Trackpart19 PICTURE X(120).
   01 Trackpart20 PICTURE X(120).
   01 Trackpart21 PICTURE X(120).
   01 Trackpart22 PICTURE X(120).
   01 Trackpart23 PICTURE X(120).
   01 Trackpart24 PICTURE X(120).
   01 Inputoptionval1 PICTURE X(2).
   01 Inputoptionval2 PICTURE X(2).
01 Inputoptionval3 PICTURE X(2).
01 Inputoptionval4 PICTURE X(2).
01 Inputoptionval5 PICTURE X(2).
01 Inputoptionval6 PICTURE X(2).
01 Inputoptionval7 PICTURE X(2).
01 Inputoptionval8 PICTURE X(2).
01 Inputoptionval9 PICTURE X(2).
01 Inputoptionval10 PICTURE X(2).
01 Inputoptionval11 PICTURE X(2).
01 Inputoptionval12 PICTURE X(2).
01 Inputoptionval13 PICTURE X(2).
01 Inputoptionval14 PICTURE X(2).
01 Inputoptionval15 PICTURE X(2).
01 Inputoptionval16 PICTURE X(2).
01 Optval PICTURE X(1).
01 Optval2 PICTURE X(1).
    01 Optval3 PICTURE X(1).
    01 Returnval PICTURE X(1).
    01 Gamesavevar PICTURE X(1).
    01 Gamename PICTURE X(20).
    01 Moveval PICTURE X(2).
    01 Switchval1 PICTURE 9(1).
    01 Switchval2 PICTURE 9(1).
    01 Switchval3 PICTURE 9(1).
    01 Switchval4 PICTURE 9(1).
    01 Switchval5 PICTURE 9(1).
    01 Switchval6 PICTURE 9(1).
    01 Switchval7 PICTURE 9(1).
    01 Switchval8 PICTURE 9(1).
    01 Switchval9 PICTURE 9(1).
    01 Switchval10 PICTURE 9(1).
    01 Switchval11 PICTURE 9(1).
    01 Switchval12 PICTURE 9(1).
    01 Switchval13 PICTURE 9(1).
    01 Switchval14 PICTURE 9(1).
    01 Switchval15 PICTURE 9(1).
    01 Switchval16 PICTURE 9(1).
    01 Switchval17 PICTURE 9(1).
    01 Switchval18 PICTURE 9(1).
    01 Switchval19 PICTURE 9(1).
    01 Switchval20 PICTURE 9(1).
    01 Switchval21 PICTURE 9(1).
    01 Switchval22 PICTURE 9(1).
    01 Switchval23 PICTURE 9(1).
    01 Switchval24 PICTURE 9(1).
    01 Switchval25 PICTURE 9(1).
    01 Switchval26 PICTURE 9(1).
    01 Switchval27 PICTURE 9(1).
    01 Switchval28 PICTURE 9(1).
    01 Switchval29 PICTURE 9(1).
    01 Switchval30 PICTURE 9(1).
    01 Switchval31 PICTURE 9(1).
01 Posnum PICTURE 9(4).
01 Posnumold PICTURE 9(4).
01 Posnumnew PICTURE 9(4).
01 Rownum PICTURE 9(4).
01 Colnum PICTURE 9(4).
01 Partlen PICTURE 9(4).
01 Stopnumber PICTURE 9(1).
01 Inspectres PICTURE X(2).
01  r pic 9.
01  r-empty pic 9.
01  r-to pic 9.
01  r-from pic 9.

01  c pic 9.
01  c-empty pic 9.
01  c-to pic 9.
01  c-from pic 9.
    SCREEN SECTION.
    01 Celladressoutput BACKGROUND-COLOR 3 FOREGROUND-COLOUR 0.
    05  VALUE "15 Puzzle game" BLANK SCREEN LINE  1 COL 15.
    05  VALUE "....................." LINE 3 COL 1.
    05  VALUE "." LINE 4 COL 1.
    05  Inputoption1 PIC X(2) LINE 4 COLUMN 3 FROM Inputoptionval1.
    05  VALUE "." LINE 4 COL 6.
    05  Inputoption2 PIC X(2) LINE  4 COLUMN 8 FROM Inputoptionval2.
    05  VALUE "." LINE 4 COL 11.
    05  Inputoption3 PIC X(2) LINE  4 COLUMN 13 FROM Inputoptionval3.
    05  VALUE "." LINE 4 COL 16.
    05  Inputoption4 LINE  4 COLUMN 18 PIC X(2) FROM Inputoptionval4.
    05  VALUE "." LINE 4 COL 21.
    05  VALUE "....................." LINE 5 COL 1.
    05  VALUE "." LINE 6 COL 1.
    05  Inputoption5 LINE  6 COLUMN 3 PIC X(2) FROM Inputoptionval5.
    05  VALUE "." LINE 6 COL 6.
    05  Inputoption6 LINE  6 COLUMN 8 PIC X(2) FROM Inputoptionval6.
    05  VALUE "." LINE 6 COL 11.
    05  Inputoption7 LINE  6 COLUMN 13 PIC X(2) FROM Inputoptionval7.
    05  VALUE "." LINE 6 COL 16.
    05  Inputoption8 LINE  6 COLUMN 18 PIC X(2) FROM Inputoptionval8.
    05  VALUE "." LINE 6 COL 21.
    05  VALUE "....................." LINE 7 COL 1.
    05  VALUE "." LINE 8 COL 1.
    05  Inputoption9 LINE  8 COLUMN 3 PIC X(2) FROM Inputoptionval9.
    05  VALUE "." LINE 8 COL 6.
    05  Inputoption10 LINE  8 COLUMN 8 PIC X(2) FROM Inputoptionval10.
    05  VALUE "." LINE 8 COL 11.
    05  Inputoption11 LINE  8 COLUMN 13 PIC X(2) FROM Inputoptionval11.
    05  VALUE "." LINE 8 COL 16.
    05  Inputoption12 LINE  8 COLUMN 18 PIC X(2) FROM Inputoptionval12.
    05  VALUE "." LINE 8 COL 21.
    05  VALUE "....................." LINE 9 COL 1.
    05  VALUE "." LINE 10 COL 1.
    05  Inputoption13 LINE  10 COLUMN 3 PIC X(2) FROM Inputoptionval13.
    05  VALUE "." LINE 10 COL 6.
    05  Inputoption14 LINE  10 COLUMN 8 PIC X(2) FROM Inputoptionval14.
    05  VALUE "." LINE 10 COL 11.
    05  Inputoption15 LINE  10 COLUMN 13 PIC X(2) FROM Inputoptionval15.
    05  VALUE "." LINE 10 COL 16.
    05  Inputoption16 LINE  10 COLUMN 18 PIC X(2) FROM Inputoptionval16.
    05  VALUE "." LINE 10 COL 21.
    05  VALUE "....................." LINE 11 COL 1.
    05  Posnuminfo LINE  14 COLUMN 18 PIC X(2) FROM Posnum.
    05  Movevalinput LINE  12 COLUMN 18 PIC X(2) TO Moveval.
    05  VALUE "Fastinput: " LINE 16 COL 30.
    05  Optvalinput LINE  16 COLUMN 40 PIC X(1) TO Optval.
    05  VALUE "Back to Start: " LINE 20 COLUMN 20.
    05  Returnvalinput LINE  20 COLUMN 40 PIC X(1) TO Returnval.
    01 Gamesavedetailscreen3 BACKGROUND-COLOR 3 FOREGROUND-COLOUR 0.
    05  VALUE "Spielname: " BLANK SCREEN LINE 3 COL 3.
    05  Gamenameinput LINE  3 COLUMN 26 PIC X(20) TO Gamename.
    01 Startscreen BACKGROUND-COLOR 3 FOREGROUND-COLOUR 0.
    05  VALUE "Gamecollection: " BLANK SCREEN LINE 1 COL 3.
    05  VALUE "Ein Spiel durch Setzen von x auswaehlen " LINE 2 COL 3.
    05  VALUE "15puzzlegame: " LINE 3 COL 3.
    05  Optvalinput_Start LINE  3 COLUMN 20 PIC X(1) TO Optval.
    05  VALUE "Trackgame: " Line 4 COL 3.
    05  Optvalinput2 LINE  4 COLUMN 20 PIC X(1) TO Optval2.
    05  VALUE "Elevatrogame: " Line 5 COL 3.
    05  Optvalinput3 LINE  5 COLUMN 20 PIC X(1) TO Optval3.
    01 Gamesavescreen BACKGROUND-COLOR 3 FOREGROUND-COLOUR 0.
    05  VALUE "Spiel speichern(j/n): " BLANK SCREEN LINE 3 COL 3.
    05  Gamesavevarinput LINE  3 COLUMN 26 PIC X(1) TO Gamesavevar.
    01 Trackscreen BACKGROUND-COLOR 3 FOREGROUND-COLOUR 0.
    05  VALUE "Railwaynetworkscreen: " BLANK SCREEN LINE 1 COL 3.
    05  VALUE "Switchprog: " LINE 27 COLUMN 20.
    05  Optvalinput LINE  27 COLUMN 40 PIC X(1) TO Optval.
    05 Trackpartinput1 LINE 2 COLUMN 1 PIC X(120) FROM Trackpart1.
    05 Trackpartinput2 LINE 3 COLUMN 1 PIC X(120) FROM Trackpart2.
    05 Trackpartinput3 LINE 4 COLUMN 1 PIC X(120) FROM Trackpart3.
    05 Trackpartinput4 LINE 5 COLUMN 1 PIC X(120) FROM Trackpart4.
    05 Trackpartinput5 LINE 6 COLUMN 1 PIC X(120) FROM Trackpart5.
    05 Trackpartinput6 LINE 7 COLUMN 1 PIC X(120) FROM Trackpart6.
    05 Trackpartinput7 LINE 8 COLUMN 1 PIC X(120) FROM Trackpart7.
    05 Trackpartinput8 LINE 9 COLUMN 1 PIC X(120) FROM Trackpart8.
    05 Trackpartinput9 LINE 10 COLUMN 1 PIC X(120) FROM Trackpart9.
    05 Trackpartinput10 LINE 11 COLUMN 1 PIC X(120) FROM Trackpart10.
    05 Trackpartinput11 LINE 12 COLUMN 1 PIC X(120) FROM Trackpart11.
    05 Trackpartinput12 LINE 13 COLUMN 1 PIC X(120) FROM Trackpart12.
    05 Trackpartinput13 LINE 14 COLUMN 1 PIC X(120) FROM Trackpart13.
    05 Trackpartinput14 LINE 15 COLUMN 1 PIC X(120) FROM Trackpart14.
    05 Trackpartinput15 LINE 16 COLUMN 1 PIC X(120) FROM Trackpart15.
    05 Trackpartinput16 LINE 17 COLUMN 1 PIC X(120) FROM Trackpart16.
    05 Trackpartinput17 LINE 18 COLUMN 1 PIC X(120) FROM Trackpart17.
    05 Trackpartinput18 LINE 19 COLUMN 1 PIC X(120) FROM Trackpart18.
    05 Trackpartinput19 LINE 20 COLUMN 1 PIC X(120) FROM Trackpart19.
    05 Trackpartinput20 LINE 21 COLUMN 1 PIC X(120) FROM Trackpart20.
    05 Trackpartinput21 LINE 22 COLUMN 1 PIC X(120) FROM Trackpart21.
    05 Trackpartinput22 LINE 23 COLUMN 1 PIC X(120) FROM Trackpart22.
    05 Trackpartinput23 LINE 24 COLUMN 1 PIC X(120) FROM Trackpart23.
    05 Trackpartinput24 LINE 25 COLUMN 1 PIC X(120) FROM Trackpart24.
    01 Switchscreen BACKGROUND-COLOR 3 FOREGROUND-COLOUR 0.
    05  VALUE "Switchscreen: " BLANK SCREEN LINE 1 COL 3.
    05  VALUE "Switch1: " LINE 3 COLUMN 3.
    05  Switchinput1 LINE 3 COLUMN 12 PIC 9(1) TO Switchval1.
    05  VALUE "Switch2: " LINE 4 COLUMN 3.
    05  Switchinput2 LINE 4 COLUMN 12 PIC 9(1) TO Switchval2.
    05  VALUE "Switch3: " LINE 5 COLUMN 3.
    05  Switchinput3 LINE 5 COLUMN 12 PIC 9(1) TO Switchval3.
    05  VALUE "Switch4: " LINE 6 COLUMN 3.
    05  Switchinput4 LINE 6 COLUMN 12 PIC 9(1) TO Switchval4.
    05  VALUE "Switch5: " LINE 7 COLUMN 3.
    05  Switchinput5 LINE 7 COLUMN 12 PIC 9(1) TO Switchval5.
    05  VALUE "Switch6: " LINE 8 COLUMN 3.
    05  Switchinput6 LINE 8 COLUMN 12 PIC 9(1) TO Switchval6.
    05  VALUE "Switch7: " LINE 9 COLUMN 3.
    05  Switchinput7 LINE 9 COLUMN 12 PIC 9(1) TO Switchval7.
    05  VALUE "Switch8: " LINE 10 COLUMN 3.
    05  Switchinput8 LINE 10 COLUMN 12 PIC 9(1) TO Switchval8.
    05  VALUE "Switch9: " LINE 11 COLUMN 3.
    05  Switchinput9 LINE 11 COLUMN 12 PIC 9(1) TO Switchval9.
    05  VALUE "Switch10: " LINE 12 COLUMN 3.
    05  Switchinput10 LINE 12 COLUMN 12 PIC 9(1) TO Switchval10.
    05  VALUE "Switch11: " LINE 13 COLUMN 3.
    05  Switchinput11 LINE 13 COLUMN 12 PIC 9(1) TO Switchval11.
    05  VALUE "Switch12: " LINE 14 COLUMN 3.
    05  Switchinput12 LINE 14 COLUMN 12 PIC 9(1) TO Switchval12.
    05  VALUE "Switch13: " LINE 15 COLUMN 3.
    05  Switchinput13 LINE 15 COLUMN 12 PIC 9(1) TO Switchval13.
    05  VALUE "Switch14: " LINE 16 COLUMN 3.
    05  Switchinput14 LINE 16 COLUMN 12 PIC 9(1) TO Switchval14.
    05  VALUE "Switch15: " LINE 17 COLUMN 3.
    05  Switchinput15 LINE 17 COLUMN 12 PIC 9(1) TO Switchval15.
    05  VALUE "Switch16: " LINE 18 COLUMN 3.
    05  Switchinput16 LINE 18 COLUMN 12 PIC 9(1) TO Switchval16.
    05  VALUE "Switch17: " LINE 19 COLUMN 3.
    05  Switchinput17 LINE 19 COLUMN 12 PIC 9(1) TO Switchval17.
    05  VALUE "Switch18: " LINE 20 COLUMN 3.
    05  Switchinput18 LINE 20 COLUMN 12 PIC 9(1) TO Switchval18.
    05  VALUE "Switch19: " LINE 21 COLUMN 3.
    05  Switchinput19 LINE 21 COLUMN 12 PIC 9(1) TO Switchval19.
    05  VALUE "Switch20: " LINE 22 COLUMN 3.
    05  Switchinput20 LINE 22 COLUMN 12 PIC 9(1) TO Switchval20.
    05  VALUE "Switch21: " LINE 23 COLUMN 3.
    05  Switchinput21 LINE 23 COLUMN 12 PIC 9(1) TO Switchval21.
    05  VALUE "Switch22: " LINE 24 COLUMN 3.
    05  Switchinput22 LINE 24 COLUMN 12 PIC 9(1) TO Switchval22.
    05  VALUE "Back to Trackscreen: " LINE 27 COLUMN 3.
    05  Optvalinput_1 LINE  27 COLUMN 40 PIC X(1) TO Optval.
    01 Elevatroscreen BACKGROUND-COLOR 3 FOREGROUND-COLOUR 0.
    05  VALUE "Elevatorscreen: " BLANK SCREEN LINE 1 COL 3.
    05  VALUE "Back to Trackscreen: " LINE 28 COLUMN 3.
    05  Optvalinput_2 LINE  28 COLUMN 40 PIC X(1) TO Optval.
    05  VALUE "Stopnumber: " LINE 26 COLUMN 3.
    05  Stopnumberinput LINE 26 COLUMN 30 PIC 9(1) TO Stopnumber.
    05 Trackpartinput1 LINE 2 COLUMN 1 PIC X(120) FROM Trackpart1.
    05 Trackpartinput2 LINE 3 COLUMN 1 PIC X(120) FROM Trackpart2.
    05 Trackpartinput3 LINE 4 COLUMN 1 PIC X(120) FROM Trackpart3.
    05 Trackpartinput4 LINE 5 COLUMN 1 PIC X(120) FROM Trackpart4.
    05 Trackpartinput5 LINE 6 COLUMN 1 PIC X(120) FROM Trackpart5.
    05 Trackpartinput6 LINE 7 COLUMN 1 PIC X(120) FROM Trackpart6.
    05 Trackpartinput7 LINE 8 COLUMN 1 PIC X(120) FROM Trackpart7.
    05 Trackpartinput8 LINE 9 COLUMN 1 PIC X(120) FROM Trackpart8.
    05 Trackpartinput9 LINE 10 COLUMN 1 PIC X(120) FROM Trackpart9.
    05 Trackpartinput10 LINE 11 COLUMN 1 PIC X(120) FROM Trackpart10.
    05 Trackpartinput11 LINE 12 COLUMN 1 PIC X(120) FROM Trackpart11.
    05 Trackpartinput12 LINE 13 COLUMN 1 PIC X(120) FROM Trackpart12.
    05 Trackpartinput13 LINE 14 COLUMN 1 PIC X(120) FROM Trackpart13.
    05 Trackpartinput14 LINE 15 COLUMN 1 PIC X(120) FROM Trackpart14.
    05 Trackpartinput15 LINE 16 COLUMN 1 PIC X(120) FROM Trackpart15.
    05 Trackpartinput16 LINE 17 COLUMN 1 PIC X(120) FROM Trackpart16.
    05 Trackpartinput17 LINE 18 COLUMN 1 PIC X(120) FROM Trackpart17.
    05 Trackpartinput18 LINE 19 COLUMN 1 PIC X(120) FROM Trackpart18.
    05 Trackpartinput19 LINE 20 COLUMN 1 PIC X(120) FROM Trackpart19.
    05 Trackpartinput20 LINE 21 COLUMN 1 PIC X(120) FROM Trackpart20.
    05 Trackpartinput21 LINE 22 COLUMN 1 PIC X(120) FROM Trackpart21.
    05 Trackpartinput22 LINE 23 COLUMN 1 PIC X(120) FROM Trackpart22.
    05 Trackpartinput23 LINE 24 COLUMN 1 PIC X(120) FROM Trackpart23.
    05 Trackpartinput24 LINE 25 COLUMN 1 PIC X(120) FROM Trackpart24.
    procedure division.
    PERFORM Gamestartprog.
    stop run.
    Gamestartprog.
    DISPLAY Startscreen.
    ACCEPT Startscreen.
    MOVE " " TO Optval.
    IF Optval2 = "x" THEN PERFORM Railnetworkprog.
    MOVE " " TO Optval2.
    IF Optval3 = "x" THEN PERFORM Elevatroprog.
    MOVE " " TO Optval3.
    Fastinputprog.
    Elevatroprog.
    PERFORM Elevatroinit.
    Elevatroinit.
    MOVE" " TO Emptyrow.
    MOVE"_____________________________________________________" TO Trackpart1.
    MOVE"|      |                                            |" TO Trackpart2.
    MOVE"|      |                                            |" TO Trackpart3.
    MOVE"|      |                                            |" TO Trackpart4.
    MOVE"|      |                                            |" TO Trackpart5.
    MOVE"|      |                                            |" TO Trackpart6.
    MOVE"|      |                                            |" TO Trackpart7.
    MOVE"|      |                                            |" TO Trackpart8.
    MOVE"|      |     _____________________________     _____|" TO Trackpart9.
    MOVE"|      |     |                           |     |     " TO Trackpart10.
    MOVE"|      |     |                           |     |     " TO Trackpart11.
    MOVE"|      |     |                           |     |     " TO Trackpart12.
    MOVE"|      |     |                           |     |     " TO Trackpart13.
    MOVE"|      |     |                           |     |     " TO Trackpart14.
    MOVE"|      |     |             STOP1         |     |     " TO Trackpart15.
    MOVE"|      |     |___________________        |     |_____" TO Trackpart16.
    MOVE"|STOP2 |                   _____|        |          |" TO Trackpart17.
    MOVE"|      |                   |xxx||        |          |" TO Trackpart18.
    MOVE"|      |                   |x x||        |          |" TO Trackpart19.
    MOVE"|      |                   |xxx||        |          |" TO Trackpart20.
    MOVE"|      |                   |  x||        |          |" TO Trackpart21.
    MOVE"|      |                   |xxx||        |          |" TO Trackpart22.
    MOVE"|      |                   |___||        |          |" TO Trackpart23.
    MOVE"|______|________________________|        |__________|" TO Trackpart24.
    DISPLAY Elevatroscreen.
    ACCEPT Elevatroscreen.
    MOVE 28 TO Posnum.
    PERFORM Elevatroupdate.
    DISPLAY Elevatroscreen.
    ACCEPT Elevatroscreen.
    Elevatroupdate.
    *>IF Stopnumber = 2 THEN PERFORM Leftsidemoveprog 19 TIMES.
    IF Stopnumber = 2 THEN PERFORM Movestart.
    Leftsidemoveprog.
    MOVE Trackpart17(Posnum:5) TO Trackpart17(Posnum - 1:5).
    MOVE Trackpart18(Posnum:5) TO Trackpart18(Posnum - 1:5).
    MOVE Trackpart19(Posnum:5) TO Trackpart19(Posnum - 1:5).
    MOVE Trackpart20(Posnum:5) TO Trackpart20(Posnum - 1:5).
    MOVE Trackpart21(Posnum:5) TO Trackpart21(Posnum - 1:5).
    MOVE Trackpart22(Posnum:5) TO Trackpart22(Posnum - 1:5).
    MOVE Trackpart23(Posnum:5) TO Trackpart23(Posnum - 1:5).
    MOVE " " TO Trackpart17(Posnum + 4:1).
    MOVE " " TO Trackpart18(Posnum + 4:1).
    MOVE " " TO Trackpart19(Posnum + 4:1).
    MOVE " " TO Trackpart20(Posnum + 4:1).
    MOVE " " TO Trackpart21(Posnum + 4:1).
    MOVE " " TO Trackpart22(Posnum + 4:1).
    MOVE " " TO Trackpart23(Posnum + 4:1).
    SUBTRACT 1 FROM Posnum.
    DISPLAY Elevatroscreen.
    ACCEPT OMITTED WITH TIMEOUT 1.
    Movestart.
    MOVE 28 TO Posnumold.
    MOVE 27 TO Posnumnew.
    MOVE 5 TO Partlen.
    PERFORM Posupdateprog 19 TIMES.
    Posupdateprog.
    MOVE 17 TO Rownum.
    PERFORM Horizontalmoveprog 7 TIMES.
    MOVE 17 TO Rownum.
    PERFORM Horizontalresetprog 7 TIMES.
    DISPLAY Elevatroscreen.
    ACCEPT OMITTED WITH TIMEOUT 1.
    SUBTRACT 1 FROM Posnumold.
    SUBTRACT 1 FROM Posnumnew.
    Horizontalmoveprog.
    EVALUATE Rownum
    WHEN 1 MOVE Trackpart1(Posnumold:Partlen) TO Trackpart1(Posnumnew:Partlen)
    WHEN 2 MOVE Trackpart2(Posnumold:Partlen) TO Trackpart2(Posnumnew:Partlen)
    WHEN 3 MOVE Trackpart3(Posnumold:Partlen) TO Trackpart3(Posnumnew:Partlen)
    WHEN 4 MOVE Trackpart4(Posnumold:Partlen) TO Trackpart4(Posnumnew:Partlen)
    WHEN 5 MOVE Trackpart5(Posnumold:Partlen) TO Trackpart5(Posnumnew:Partlen)
    WHEN 6 MOVE Trackpart6(Posnumold:Partlen) TO Trackpart6(Posnumnew:Partlen)
    WHEN 7 MOVE Trackpart7(Posnumold:Partlen) TO Trackpart7(Posnumnew:Partlen)
    WHEN 8 MOVE Trackpart8(Posnumold:Partlen) TO Trackpart8(Posnumnew:Partlen)
    WHEN 9 MOVE Trackpart9(Posnumold:Partlen) TO Trackpart9(Posnumnew:Partlen)
    WHEN 10 MOVE Trackpart10(Posnumold:Partlen) TO Trackpart10(Posnumnew:Partlen)
    WHEN 11 MOVE Trackpart11(Posnumold:Partlen) TO Trackpart11(Posnumnew:Partlen)
    WHEN 12 MOVE Trackpart12(Posnumold:Partlen) TO Trackpart12(Posnumnew:Partlen)
    WHEN 13 MOVE Trackpart13(Posnumold:Partlen) TO Trackpart13(Posnumnew:Partlen)
    WHEN 14 MOVE Trackpart14(Posnumold:Partlen) TO Trackpart14(Posnumnew:Partlen)
    WHEN 15 MOVE Trackpart15(Posnumold:Partlen) TO Trackpart15(Posnumnew:Partlen)
    WHEN 16 MOVE Trackpart16(Posnumold:Partlen) TO Trackpart16(Posnumnew:Partlen)
    WHEN 17 MOVE Trackpart17(Posnumold:Partlen) TO Trackpart17(Posnumnew:Partlen)
    WHEN 18 MOVE Trackpart18(Posnumold:Partlen) TO Trackpart18(Posnumnew:Partlen)
    WHEN 19 MOVE Trackpart19(Posnumold:Partlen) TO Trackpart19(Posnumnew:Partlen)
    WHEN 20 MOVE Trackpart20(Posnumold:Partlen) TO Trackpart20(Posnumnew:Partlen)
    WHEN 21 MOVE Trackpart21(Posnumold:Partlen) TO Trackpart21(Posnumnew:Partlen)
    WHEN 22 MOVE Trackpart22(Posnumold:Partlen) TO Trackpart22(Posnumnew:Partlen)
    WHEN 23 MOVE Trackpart23(Posnumold:Partlen) TO Trackpart23(Posnumnew:Partlen)
    WHEN 24 MOVE Trackpart24(Posnumold:Partlen) TO Trackpart24(Posnumnew:Partlen)
    END-EVALUATE.
    ADD 1 TO Rownum.
    Horizontalresetprog.
    EVALUATE Rownum
    WHEN 1 MOVE Emptyrow(Posnumold + Partlen - 1:1) TO Trackpart1(Posnumold + Partlen - 1:1)
    WHEN 2 MOVE Emptyrow(Posnumold + Partlen - 1:1) TO Trackpart2(Posnumold + Partlen - 1:1)
    WHEN 3 MOVE Emptyrow(Posnumold + Partlen - 1:1) TO Trackpart3(Posnumold + Partlen - 1:1)
    WHEN 4 MOVE Emptyrow(Posnumold + Partlen - 1:1) TO Trackpart4(Posnumold + Partlen - 1:1)
    WHEN 5 MOVE Emptyrow(Posnumold + Partlen - 1:1) TO Trackpart5(Posnumold + Partlen - 1:1)
    WHEN 6 MOVE Emptyrow(Posnumold + Partlen - 1:1) TO Trackpart6(Posnumold + Partlen - 1:1)
    WHEN 7 MOVE Emptyrow(Posnumold + Partlen - 1:1) TO Trackpart7(Posnumold + Partlen - 1:1)
    WHEN 8 MOVE Emptyrow(Posnumold + Partlen - 1:1) TO Trackpart8(Posnumold + Partlen - 1:1)
    WHEN 9 MOVE Emptyrow(Posnumold + Partlen - 1:1) TO Trackpart9(Posnumold + Partlen - 1:1)
    WHEN 10 MOVE Emptyrow(Posnumold + Partlen - 1:1) TO Trackpart10(Posnumold + Partlen - 1:1)
    WHEN 11 MOVE Emptyrow(Posnumold + Partlen - 1:1) TO Trackpart11(Posnumold + Partlen - 1:1)
    WHEN 12 MOVE Emptyrow(Posnumold + Partlen - 1:1) TO Trackpart12(Posnumold + Partlen - 1:1)
    WHEN 13 MOVE Emptyrow(Posnumold + Partlen - 1:1) TO Trackpart13(Posnumold + Partlen - 1:1)
    WHEN 14 MOVE Emptyrow(Posnumold + Partlen - 1:1) TO Trackpart14(Posnumold + Partlen - 1:1)
    WHEN 15 MOVE Emptyrow(Posnumold + Partlen - 1:1) TO Trackpart15(Posnumold + Partlen - 1:1)
    WHEN 16 MOVE Emptyrow(Posnumold + Partlen - 1:1) TO Trackpart16(Posnumold + Partlen - 1:1)
    WHEN 17 MOVE Emptyrow(Posnumold + Partlen - 1:1) TO Trackpart17(Posnumold + Partlen - 1:1)
    WHEN 18 MOVE Emptyrow(Posnumold + Partlen - 1:1) TO Trackpart18(Posnumold + Partlen - 1:1)
    WHEN 19 MOVE Emptyrow(Posnumold + Partlen - 1:1) TO Trackpart19(Posnumold + Partlen - 1:1)
    WHEN 20 MOVE Emptyrow(Posnumold + Partlen - 1:1) TO Trackpart20(Posnumold + Partlen - 1:1)
    WHEN 21 MOVE Emptyrow(Posnumold + Partlen - 1:1) TO Trackpart21(Posnumold + Partlen - 1:1)
    WHEN 22 MOVE Emptyrow(Posnumold + Partlen - 1:1) TO Trackpart22(Posnumold + Partlen - 1:1)
    WHEN 23 MOVE Emptyrow(Posnumold + Partlen - 1:1) TO Trackpart23(Posnumold + Partlen - 1:1)
    WHEN 24 MOVE Emptyrow(Posnumold + Partlen - 1:1) TO Trackpart24(Posnumold + Partlen - 1:1)
    END-EVALUATE.
    ADD 1 TO Rownum.
    Railnetworkprog.
    MOVE " " TO Optval.
    DISPLAY Trackscreen.
    ACCEPT Trackscreen.
    IF Optval = "x" THEN PERFORM Switchsubprog.
    Switchsubprog.
    DISPLAY Switchscreen.
    ACCEPT Switchscreen.
    end program Animation.
