*>****************************************************************
*> Author:
*> Date:
*> Purpose: A programming language based on Animations, moving around barges
*> Tectonics: cobc
*>****************************************************************
IDENTIFICATION DIVISION.
PROGRAM-ID. TUGBOAT.
DATA DIVISION.
FILE SECTION.
WORKING-STORAGE SECTION.
01 Str PICTURE X(20).
01 Str2 PIC X(1).
01 Calcnum PIC 9(1).
01 Directionval PIC 9(1).
01 Num PIC 9(4).
01 Num2 PIC 9(4).
01 Performval PIC 9(4).
01 Framenum PIC 9(1).
01 Framenum2 PIC 9(1).
01 Baseline1 PICTURE X(135).
01 Baseline2 PICTURE X(135).
01 Baseline3 PICTURE X(135).
01 Baseline4 PICTURE X(135).
01 Baseline5 PICTURE X(135).
01 Baseline6 PICTURE X(135).
01 Baseline7 PICTURE X(135).
01 Baseline8 PICTURE X(135).
01 Baseline9 PICTURE X(135).
01 Baseline10 PICTURE X(135).
01 Baseline11 PICTURE X(135).
01 Baseline12 PICTURE X(135).
01 Baseline13 PICTURE X(135).
01 Baseline14 PICTURE X(135).
01 Baseline15 PICTURE X(135).
01 Baseline16 PICTURE X(135).
01 Baseline17 PICTURE X(135).
01 Baseline18 PICTURE X(135).
01 Baseline19 PICTURE X(135).
01 Baseline20 PICTURE X(135).
01 Baseline21 PICTURE X(135).
01 Baseline22 PICTURE X(135).
01 Baseline23 PICTURE X(135).
01 Baseline24 PICTURE X(135).
01 Baseline25 PICTURE X(135).
01 Baseline26 PICTURE X(135).
01 Baseline27 PICTURE X(135).
01 Baseline28 PICTURE X(135).
01 Baseline29 PICTURE X(135).
01 Baseline30 PICTURE X(135).
01 Baseline31 PICTURE X(135).
01 Baseline32 PICTURE X(135).
01 Baseline33 PICTURE X(135).
01 Baseline34 PICTURE X(135).
01 Baseline35 PICTURE X(135).
SCREEN SECTION.
01  DATA-ENTRY-SCREEN BACKGROUND-COLOR 3 FOREGROUND-COLOUR 0.
    05  VALUE "ASCII-ART ANIMATION" BLANK SCREEN LINE 1 COL 30.
*>05  VALUE "Hello" LINE 12 COL 1.
*>05  Rolltext1 LINE 1 COL 1 PIC X(135) FROM Baseline1.
05  Rolltext2 LINE 2 COL 1 PIC X(135) FROM Baseline2.
05  Rolltext3 LINE 3 COL 1 PIC X(135) FROM Baseline3.
05  Rolltext4 LINE 4 COL 1 PIC X(135) FROM Baseline4.
05  Rolltext5 LINE 5 COL 1 PIC X(135) FROM Baseline5.
05  Rolltext6 LINE 6 COL 1 PIC X(135) FROM Baseline6.
05  Rolltext7 LINE 7 COL 1 PIC X(135) FROM Baseline7.
05  Rolltext8 LINE 8 COL 1 PIC X(135) FROM Baseline8.
05  Rolltext9 LINE 9 COL 1 PIC X(135) FROM Baseline9.
05  Rolltext10 LINE 10 COL 1 PIC X(135) FROM Baseline10.
05  Rolltext11 LINE 11 COL 1 PIC X(135) FROM Baseline11.
05  Rolltext12 LINE 12 COL 1 PIC X(135) FROM Baseline12.
05  Rolltext13 LINE 13 COL 1 PIC X(135) FROM Baseline13.
05  Rolltext14 LINE 14 COL 1 PIC X(135) FROM Baseline14.
05  Rolltext15 LINE 15 COL 1 PIC X(135) FROM Baseline15.
05  Rolltext16 LINE 16 COL 1 PIC X(135) FROM Baseline16.
05  Rolltext17 LINE 17 COL 1 PIC X(135) FROM Baseline17.
05  Rolltext18 LINE 18 COL 1 PIC X(135) FROM Baseline18.
05  Rolltext19 LINE 19 COL 1 PIC X(135) FROM Baseline19.
05  Rolltext20 LINE 20 COL 1 PIC X(135) FROM Baseline20.
05  Rolltext21 LINE 21 COL 1 PIC X(135) FROM Baseline21.
05  Rolltext22 LINE 22 COL 1 PIC X(135) FROM Baseline22.
05  Rolltext23 LINE 23 COL 1 PIC X(135) FROM Baseline23.
05  Rolltext24 LINE 24 COL 1 PIC X(135) FROM Baseline24.
05  Rolltext25 LINE 25 COL 1 PIC X(135) FROM Baseline25.
05  Rolltext26 LINE 26 COL 1 PIC X(135) FROM Baseline26.
05  Rolltext27 LINE 27 COL 1 PIC X(135) FROM Baseline27.
05  Rolltext28 LINE 28 COL 1 PIC X(135) FROM Baseline28.
05  Rolltext29 LINE 29 COL 1 PIC X(135) FROM Baseline29.
05  Rolltext30 LINE 30 COL 1 PIC X(135) FROM Baseline30.
05  Rolltext31 LINE 31 COL 1 PIC X(135) FROM Baseline31.
05  Rolltext32 LINE 32 COL 1 PIC X(135) FROM Baseline32.
05  Rolltext33 LINE 33 COL 1 PIC X(135) FROM Baseline33.
05  Rolltext34 LINE 34 COL 1 PIC X(135) FROM Baseline34.
05  Rolltext35 LINE 35 COL 1 PIC X(135) FROM Baseline35.
PROCEDURE DIVISION.
MAIN-PROCEDURE.
*>PERFORM Sceneinit.
PERFORM Sceneinitalt.
*>Start of Specification
MOVE 30 TO Performval.
MOVE 0 TO Calcnum.
MOVE 1 TO Directionval.
MOVE 22 TO Num2.
MOVE 10 TO Num.
PERFORM Alternatemove.
*>-_________________
MOVE 10 TO Performval.
MOVE 1 TO Calcnum.
MOVE 0 TO Directionval.
MOVE 24 TO Num2.
MOVE 40 TO Num.
PERFORM Alternatemove.
*>-_________________
MOVE 30 TO Performval.
MOVE 1 TO Calcnum.
MOVE 1 TO Directionval.
MOVE 13 TO Num2.
MOVE 40 TO Num.
PERFORM Alternatemove.
*>-_________________
MOVE 10 TO Performval.
MOVE 0 TO Calcnum.
MOVE 0 TO Directionval.
MOVE 15 TO Num2.
MOVE 10 TO Num.
PERFORM Alternatemove.
*>-_________________
PERFORM Displayprog 10 TIMES.
        STOP RUN.
            Alternatemove.
    *>Beginn neue Navigation   
    MOVE 1 TO Framenum.
    *>PERFORM Moveinit.
    PERFORM Reversescreensalt Performval TIMES.
    *>PERFORM Displayprog 10 TIMES.
    *>PERFORM Reversescreensalt.
    Moveinit.
        *>0 gleich rechts, 1 gleich links
    *>0 gleich runter, 1 gleich hoch.
    MOVE 0 TO Calcnum.
    *>1 gleich horizontal, 0 gleich vertikal
    MOVE 1 TO Directionval.
    *>IF Directionval = 0 THEN MOVE 22 TO Num2.
    *>Num2 ist die Zeilenangabe
    MOVE 22 TO Num2.
    *>Num ist die Spaltenangabe
    MOVE 10 TO Num.
Sceneinit.
MOVE "_________" TO Baseline2(1:10).
MOVE "____" TO Baseline3(3:4).
MOVE "|  |" TO Baseline4(3:4).
MOVE "|__|" TO Baseline5(3:4).
*>Umrandung außen
MOVE "|" TO Baseline3(1:1).
MOVE "|" TO Baseline4(1:1).
MOVE "|" TO Baseline5(1:1).
MOVE "|" TO Baseline6(1:1).
MOVE "|" TO Baseline7(1:1).
MOVE "|" TO Baseline8(1:1).
MOVE "|" TO Baseline9(1:1).
MOVE "|" TO Baseline10(1:1).
MOVE "|" TO Baseline11(1:1).
MOVE "|" TO Baseline12(1:1).
MOVE "|" TO Baseline13(1:1).
MOVE "|" TO Baseline14(1:1).
MOVE "|" TO Baseline15(1:1).
MOVE "|" TO Baseline16(1:1).
MOVE "|" TO Baseline17(1:1).
MOVE "|" TO Baseline18(1:1).
MOVE "|" TO Baseline19(1:1).
MOVE "|" TO Baseline20(1:1).
MOVE "|" TO Baseline21(1:1).
MOVE "|" TO Baseline22(1:1).
MOVE "|" TO Baseline23(1:1).
MOVE "|" TO Baseline24(1:1).
MOVE "|" TO Baseline25(1:1).
MOVE "|" TO Baseline26(1:1).
MOVE "|" TO Baseline27(1:1).
MOVE "|" TO Baseline28(1:1).
MOVE "|" TO Baseline29(1:1).
MOVE "|" TO Baseline30(1:1).
MOVE "______________________________________________________________________________________________________________________" TO Baseline27(2:120).
MOVE "xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx" TO Baseline28(2:120).
DISPLAY DATA-ENTRY-SCREEN.
ACCEPT OMITTED.
Sceneinitalt.
MOVE "XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX" TO Baseline2(1:82).
MOVE "XXXXXXX                                                          XXXXXXXXXXXXXXXXX" TO Baseline3(1:82).
MOVE "XXXXXXX                                                          XXXXXXXXXXXXXXXXX" TO Baseline4(1:82).
MOVE "XXXXXXX                                                          XXXXXXXXXXXXXXXXX" TO Baseline5(1:82).
MOVE "XXXXXXX                                                          XXXXXXXXXXXXXXXXX" TO Baseline6(1:82).
MOVE "XXXXXXX                                                          XXXXXXXXXXXXXXXXX" TO Baseline7(1:82).
MOVE "XXXXXXX                                                          XXXXXXXXXXXXXXXXX" TO Baseline8(1:82).
MOVE "XX                 XXXXXXXXXXXXXXXXXX                            XXXXXXXXXXXXXXXXX" TO Baseline9(1:82).
MOVE "XX                 XXXXXXXXXXXXXXXXXX                            XXXXXXXXXXXXXXXXX" TO Baseline10(1:82).
MOVE "XX                 XXXXXXXXXXXXXXXXXX                            XXXXXXXXXXXXXXXXX" TO Baseline11(1:82).
MOVE "XX                 XXXXXXXXXXXXXXXXXX                            XXXXXXXXXXXXXXXXX" TO Baseline12(1:82).
MOVE "XX                 XXXXXXXXXXXXXXXXXX                            XXXXXXXXXXXXXXXXX" TO Baseline13(1:82).
MOVE "XX                 XXXXXXXXXXXXXXXXXX                            XXXXXXXXXXXXXXXXX" TO Baseline14(1:82).
MOVE "XXXXXXX       XXXXXXXXXXXXXXXXXXXXXXX                            XXXXXXXXXXXXXXXXX" TO Baseline15(1:82).
MOVE "XX                 XXXXXXXXXXXXXXXXXX                            XXXXXXXXXXXXXXXXX" TO Baseline16(1:82).
MOVE "XX                 XX                                            XXXXXXXXXXXXXXXXX" TO Baseline17(1:82).
MOVE "XX                 XX                                            XXXXXXXXXXXXXXXXX" TO Baseline18(1:82).
MOVE "XX                 XX                                            XXXXXXXXXXXXXXXXX" TO Baseline19(1:82).
MOVE "XX                 XX                                            XXXXXXXXXXXXXXXXX" TO Baseline20(1:82).
MOVE "XX                                                               XXXXXXXXXXXXXXXXX" TO Baseline21(1:82).
MOVE "XX                                                               XXXXXXXXXXXXXXXXX" TO Baseline22(1:82).
MOVE "XX                                                               XXXXXXXXXXXXXXXXX" TO Baseline23(1:82).
MOVE "XX                                                               XXXXXXXXXXXXXXXXX" TO Baseline24(1:82).
MOVE "XX                                                               XXXXXXXXXXXXXXXXX" TO Baseline25(1:82).
MOVE "XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX" TO Baseline26(1:82).
MOVE "XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX" TO Baseline27(1:82).
MOVE "XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX" TO Baseline28(1:82).
MOVE "XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX" TO Baseline29(1:82).
MOVE "XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX" TO Baseline30(1:82). 
DISPLAY DATA-ENTRY-SCREEN.
ACCEPT OMITTED.
Movespec.
MOVE 4 TO Num.
PERFORM Furtherscreens 55 TIMES.
MOVE " " TO Baseline3(Num - 1:4).
MOVE " " TO Baseline4(Num - 1:4).
MOVE " " TO Baseline5(Num - 1:4).
    MOVE 1 TO Framenum.
    MOVE 4 TO Num2.
PERFORM Verticaldisplay1 20 TIMES.
    MOVE 59 TO Num.
    MOVE 1 TO Calcnum.
    PERFORM Reversescreens 55 TIMES.
    MOVE 4 TO Num.
    MOVE 0 TO Calcnum.
    PERFORM Reversescreens 55 TIMES.
    PERFORM Alternatemove.

    Reversescreensalt.
        MOVE 1 TO Framenum2.
    *>IF Directionval = 1 THEN MOVE 22 TO Num2.
    IF Directionval = 0 THEN SUBTRACT 2 FROM Num2.
    Move Num TO Baseline3(4:4).
    *>MOVE 0 TO Framenum2.
    *>PERFORM Verticalpos.
    *>MOVE 1 TO Framenum.
*>IF Directionval = 0 THEN SUBTRACT 7 FROM Num2.
IF Directionval EQUAL TO 1 THEN PERFORM Horizontaladd 3 TIMES.
IF Directionval = 0 THEN PERFORM Verticaladd 3 TIMES.
IF Directionval = 0 AND Calcnum = 1 THEN MOVE 0 TO Framenum2.
IF Directionval = 0 AND Calcnum = 1 THEN ADD 1 TO Num2.
IF Directionval = 0 AND Calcnum = 1 THEN PERFORM Verticalpos.
IF Directionval = 0 AND Calcnum = 1 THEN SUBTRACT 1 FROM Num2.
IF Directionval = 0 AND Calcnum = 1 THEN MOVE 1 TO Framenum2.
    *>Start of the Correction
IF Directionval = 0 AND Calcnum = 0 THEN MOVE 0 TO Framenum2.
IF Directionval = 0 AND Calcnum = 0 THEN SUBTRACT 3  FROM Num2.
IF Directionval = 0 AND Calcnum = 0 THEN PERFORM Verticalpos.
IF Directionval = 0 AND Calcnum = 0 THEN ADD 3 TO Num2.
IF Directionval = 0 AND Calcnum = 0 THEN MOVE 1 TO Framenum2.
  *>End
IF Directionval = 1 THEN Subtract 3 FROM Num2.
*>Correction if there is a Vertical Move
IF Directionval = 0 AND Calcnum = 1 THEN Subtract 2 FROM Num2.
IF Directionval = 0 AND Calcnum = 0 THEN ADD 0 TO Num2.
*>MOVE "____" TO Baseline23(Num:4).
*>MOVE "|  |" TO Baseline24(Num:4).
*>MOVE "|__|" TO Baseline25(Num:4).
IF Directionval = 1 AND Calcnum = 1 THEN SUBTRACT 1 FROM Num.
IF Directionval = 1 AND Calcnum = 0 THEN ADD 1 TO Num.
DISPLAY DATA-ENTRY-SCREEN.
ACCEPT OMITTED.
Displayprog.
DISPLAY DATA-ENTRY-SCREEN.
ACCEPT OMITTED.
Reversescreens.
MOVE " " TO Baseline23(3:100).
MOVE " " TO Baseline24(3:100).
MOVE " " TO Baseline25(3:100).
MOVE "____" TO Baseline23(Num:4).
MOVE "|  |" TO Baseline24(Num:4).
MOVE "|__|" TO Baseline25(Num:4).
IF Calcnum = 1 THEN SUBTRACT 1 FROM Num.
IF Calcnum = 0 THEN ADD 1 TO Num.
DISPLAY DATA-ENTRY-SCREEN.
ACCEPT OMITTED.

Horizontaladd.
ADD 1 TO Num2.
PERFORM Verticalpos.
IF Framenum2 > 0 THEN ADD 1 TO Framenum2.
Verticaladd.
ADD 1 TO Num2.
PERFORM Verticalpos.
IF Framenum2 > 0 THEN ADD 1 TO Framenum2.
Furtherscreens.
MOVE " " TO Baseline3(3:100).
MOVE " " TO Baseline4(3:100).
MOVE " " TO Baseline5(3:100).
MOVE "____" TO Baseline3(Num:4).
MOVE "|  |" TO Baseline4(Num:4).
MOVE "|__|" TO Baseline5(Num:4).
ADD 1 TO Num.
DISPLAY DATA-ENTRY-SCREEN.
ACCEPT OMITTED.
Verticaldisplay1.
MOVE "<==>" TO Str.
MOVE 1 TO Framenum2.
PERFORM Verticalmove 3 TIMES.
DISPLAY DATA-ENTRY-SCREEN.
ACCEPT OMITTED.
SUBTRACT 3 FROM Num2.
MOVE 0 TO Framenum2.
MOVE " " TO Str.
PERFORM Verticalmove 3 TIMES.
SUBTRACT 2 FROM Num2.
MOVE 1 TO Framenum2.
Verticalmove.
PERFORM Verticalpos.
ADD 1 TO Num2.
IF Framenum2 > 0 THEN ADD 1 TO Framenum2.
Framederivation.
IF Framenum2 = 0 THEN MOVE "     " TO Str.
IF Framenum2 = 1 THEN MOVE " ____" TO Str.
IF Framenum2 = 2 THEN MOVE " |  |" TO Str.
IF Framenum2 = 3 THEN MOVE " |__|" TO Str.
Verticalpos.
PERFORM Framederivation.
IF Num2 EQUAL TO 3 AND Framenum = 1 THEN MOVE Str TO Baseline3(Num:6).
IF Num2 EQUAL TO 4 AND Framenum = 1 THEN MOVE Str TO Baseline4(Num:6).
IF Num2 EQUAL TO 5 AND Framenum = 1 THEN MOVE Str TO Baseline5(Num:6).
IF Num2 EQUAL TO 6 AND Framenum = 1 THEN MOVE Str TO Baseline6(Num:6).
IF Num2 EQUAL TO 7 AND Framenum = 1 THEN MOVE Str TO Baseline7(Num:6).
IF Num2 EQUAL TO 8 AND Framenum = 1 THEN MOVE Str TO Baseline8(Num:6).
IF Num2 EQUAL TO 9 AND Framenum = 1 THEN MOVE Str TO Baseline9(Num:6).
IF Num2 EQUAL TO 10 AND Framenum = 1 THEN MOVE Str TO Baseline10(Num:6).
IF Num2 EQUAL TO 11 AND Framenum = 1 THEN MOVE Str TO Baseline11(Num:6).
IF Num2 EQUAL TO 12 AND Framenum = 1 THEN MOVE Str TO Baseline12(Num:6).
IF Num2 EQUAL TO 13 AND Framenum = 1 THEN MOVE Str TO Baseline13(Num:6).
IF Num2 EQUAL TO 14 AND Framenum = 1 THEN MOVE Str TO Baseline14(Num:6).
IF Num2 EQUAL TO 15 AND Framenum = 1 THEN MOVE Str TO Baseline15(Num:6).
IF Num2 EQUAL TO 16 AND Framenum = 1 THEN MOVE Str TO Baseline16(Num:6).
IF Num2 EQUAL TO 17 AND Framenum = 1 THEN MOVE Str TO Baseline17(Num:6).
IF Num2 EQUAL TO 18 AND Framenum = 1 THEN MOVE Str TO Baseline18(Num:6).
IF Num2 EQUAL TO 19 AND Framenum = 1 THEN MOVE Str TO Baseline19(Num:6).
IF Num2 EQUAL TO 20 AND Framenum = 1 THEN MOVE Str TO Baseline20(Num:6).
IF Num2 EQUAL TO 21 AND Framenum = 1 THEN MOVE Str TO Baseline21(Num:6).
IF Num2 EQUAL TO 22 AND Framenum = 1 THEN MOVE Str TO Baseline22(Num:6).
IF Num2 EQUAL TO 23 AND Framenum = 1 THEN MOVE Str TO Baseline23(Num:6).
IF Num2 EQUAL TO 24 AND Framenum = 1 THEN MOVE Str TO Baseline24(Num:6).
IF Num2 EQUAL TO 25 AND Framenum = 1 THEN MOVE Str TO Baseline25(Num:6).
IF Num2 EQUAL TO 26 AND Framenum = 1 THEN MOVE Str TO Baseline26(Num:6).
IF Num2 EQUAL TO 27 AND Framenum = 1 THEN MOVE Str TO Baseline27(Num:6).
END PROGRAM TUGBOAT.
