IDENTIFICATION DIVISION.
PROGRAM-ID. YOUR-PROGRAM-NAME.
ENVIRONMENT DIVISION.
INPUT-OUTPUT SECTION.
FILE-CONTROL.
    SELECT Inputfile ASSIGN TO "/home/hogue/Dokumente/Cabmove/Housetemplates.txt"
    ORGANIZATION IS LINE SEQUENTIAL.
DATA DIVISION.
FILE SECTION.
    FD  Inputfile.
    01  Record-Name3.
    02 Field1 PIC X(135).

WORKING-STORAGE SECTION.
01 Str1 PIC X(20).
01 Objectstore PIC X(135).
01 Str2 PIC X(1).
01 Saveindicator PIC X(1).
01 Saveindicator2 PIC X(1).
01 Num PIC 9(4).
01 Num2 PIC 9(4).
01 Insertpos PIC 9(4).
01 Scenestartpos PIC 9(4).
01 Scenecountpos PIC 9(4).
01 Scenewidth PIC 9(4).
01 Totalscenewidth PIC 9(4).
01 Scenecorrection PIC 9(4).
01 Scenebaserow PIC 9(4).
01 Sceneoutputrow PIC 9(4).
01 Sceneoutputbaserow PIC 9(4).
01 Indentlevel PIC 9(4).
01 Startcol PIC 9(4).
01 Correctionval PIC 9(4).
01 Objectwidth PIC 9(4).
01 Objectheigth PIC 9(4).
01 Totalheigth PIC 9(4).
01 Totalheigthres PIC 9(4).
01 Houserownumber PIC 9(4).
01 Houserowaddnumber PIC 9(4).
01 Housenumber PIC 9(4).
01 Relrow PIC 9(4).
01 Addbase PIC 9(4).
01 Newbaserow PIC 9(4).
01 Newbaserowpart PIC 9(4).
01 Bottomrow PIC 9(4).
01 Rowposbaseaddition PIC S9(4).
01 Rowposbase PIC 9(4).
01 Rowposbasemin PIC 9(4).
01 Rowposres PIC 9(4).
01 Countnum PIC 9(4).
01 Furthermoveprog PIC 9(4).
01 Countnumbase PIC 9(4).
01 Sceneorigin PIC 9(4).
01 Scenepartstart PIC 9(4).
01 Houserowwidth PIC 9(4).
01 Newrow PIC 9(4).
01 Cabmovecount PIC 9(4).
01 Newrowbase PIC 9(4).
01 Minpos PIC 9(4).
01 Minrow PIC 9(4).
01 Maxrow PIC 9(4).
01 Subcount PIC 9(4).
01 Reacttime PIC 9(4).
01 Windowcount PIC 9(2).
01 Depthval PIC 9(2).
01 Doorpos PIC 9(2).
01 Initnum PIC 9(1).
01 Modenum PIC 9(1).
*>Variablen zur Richtungsangabe
*>1 gleich hoch, 2 gleich runter, 0 neutral
01 Horizontalnum PIC 9(1).
*>1 gleich links, 2 gleich rechts, 0 neutral
01 Verticalnum PIC 9(1).
*>Zeichenketten mit Richtungsangaben
01 Horizontalstring PIC X(100).
01 Verticalstring PIC X(100).
01 Levelcount PIC 9(2).
01 Baseline1 PICTURE X(135).
01 Baseline2 PICTURE X(135).
01 Baseline3 PICTURE X(135).
01 Inputstring PICTURE X(135).
01 Rowwidthtable PIC 9(4) OCCURS 1000 TIMES.
01 Rowposbaseminrows PIC 9(4) OCCURS 1000 TIMES.
01 Animatetablerow PIC X(135) OCCURS 1000 TIMES.
01 Newanimatetablerow PIC X(135) OCCURS 1000 TIMES.

SCREEN SECTION.

01 Houseanimatescreen BACKGROUND-COLOR 3 FOREGROUND-COLOUR 0.
05  VALUE "Houseanimationscene    Housenumber: " BLANK SCREEN LINE 1 COL 15.
05  Saveindicatorinput4 LINE 26 COLUMN 19 PIC X(1) TO Saveindicator.
05 VALUE "Housenumber: " LINE 27 COL 2.
05 Housenumberinput LINE 27 COLUMN 18 PIC 9(4) TO Houserownumber.
05 Animatetext80_3 PIC X(135) FROM Newanimatetablerow(1) LINE 2 COL 1.
05 Animatetext81_3 PIC X(135) FROM Newanimatetablerow(2) LINE 3 COL 1.
05 Animatetext82_3 PIC X(135) FROM Newanimatetablerow(3) LINE 4 COL 1.
05 Animatetext83_3 PIC X(135) FROM Newanimatetablerow(4) LINE 5 COL 1.
05 Animatetext84_3 PIC X(135) FROM Newanimatetablerow(5) LINE 6 COL 1.
05 Animatetext85_3 PIC X(135) FROM Newanimatetablerow(6) LINE 7 COL 1.
05 Animatetext86_3 PIC X(135) FROM Newanimatetablerow(7) LINE 8 COL 1.
05 Animatetext87_3 PIC X(135) FROM Newanimatetablerow(8) LINE 9 COL 1.
05 Animatetextin_3put PIC X(135) FROM Newanimatetablerow(9) LINE 10 COL 1.
05 Animatetext89_3 PIC X(135) FROM Newanimatetablerow(10) LINE 11 COL 1.
05 Animatetext90_3 PIC X(135) FROM Newanimatetablerow(11) LINE 12 COL 1.
05 Animatetext91_3 PIC X(135) FROM Newanimatetablerow(12) LINE 13 COL 1.
05 Animatetext92_3 PIC X(135) FROM Newanimatetablerow(13) LINE 14 COL 1.
05 Animatetext93_3 PIC X(135) FROM Newanimatetablerow(14) LINE 15 COL 1.
05 Animatetext94_3 PIC X(135) FROM Newanimatetablerow(15) LINE 16 COL 1.
05 Animatetext95_3 PIC X(135) FROM Newanimatetablerow(16) LINE 17 COL 1.
05 Animatetext96_3 PIC X(135) FROM Newanimatetablerow(17) LINE 18 COL 1.
05 Animatetext97_3 PIC X(135) FROM Newanimatetablerow(18) LINE 19 COL 1.
05 Animatetext98_3 PIC X(135) FROM Newanimatetablerow(19) LINE 20 COL 1.
05 Animatetext99_3 PIC X(135) FROM Newanimatetablerow(20) LINE 21 COL 1.
05 Animatetext10_30 PIC X(135) FROM Newanimatetablerow(21) LINE 22 COL 1.
05 Animatetext10_31 PIC X(135) FROM Newanimatetablerow(22) LINE 23 COL 1.
05 Animatetext10_32 PIC X(135) FROM Newanimatetablerow(23) LINE 24 COL 1.

PROCEDURE DIVISION.
    MAIN-PROCEDURE.
    *>PERFORM Houserowdesignerprog.
    MOVE 1 TO Countnum.
    MOVE 2 TO Newbaserowpart.
    OPEN INPUT Inputfile.
    PERFORM Inputmove 50 TIMES.
    CLOSE Inputfile.
    MOVE 1 TO Initnum.
    MOVE 1 TO Countnumbase.
    MOVE 23 TO Objectheigth.
    PERFORM Scenemoveprog.
    MOVE 10 TO Scenewidth.
    MOVE 6 TO Objectheigth.
    MOVE 2 TO Scenepartstart.
    MOVE 2 TO Scenestartpos.
    MOVE 1 TO Modenum.
    MOVE 50 TO Reacttime.
    *>MOVE 1 TO Newbaserowpart.
    *>PERFORM Cabinrightmove 3 TIMES.
    *>MOVE 1 TO Countnum.
    *>PERFORM Sidemovespacesubprog5 40 TIMES.
    *>MOVE 3 TO Newrow.
    *>PERFORM Cabinleftmove.
    *>Variablen zur Richtungsangabe
    *>1 gleich hoch, 2 gleich runter, 0 neutral
    *>1 gleich links, 2 gleich rechts, 0 neutral
    MOVE "11100222000000000000000000002222222222220000000000000000000000000000000000000" TO Verticalstring.
    MOVE "22211112022222222222222222220000011111112222222222222222222222222222222222222" TO Horizontalstring.
    MOVE 5 TO Newbaserowpart.
    MOVE Newbaserowpart TO Newrow.
    MOVE 9 TO Scenestartpos.
    MOVE 1 TO Cabmovecount.
    MOVE 0 TO Horizontalnum.
    MOVE 0 TO Verticalnum.
    PERFORM Severalcabmovesprog UNTIL Cabmovecount = 75.
    *>Startzeile und Anzahl Schritte nach oben links, da Countnum gleich 2
    *>MOVE 2 TO Modenum.
    *>MOVE 4 TO Newrowbase.
    *>MOVE 5 TO Cabmovecount.
    *>PERFORM Severalleftmovesprog.

    DISPLAY Houseanimatescreen.
    ACCEPT Houseanimatescreen.
    STOP RUN.

    Severalcabmovesprog.
    *>Needs values for Newrow, Newrowbase and Scenestartpos.
    MOVE 1 TO Countnum.
    PERFORM Sidemovespacesubprog5 40 TIMES.
    *>Bewegung nach links oder rechts durch Anpassung von Scenestartpos.
    MOVE Horizontalstring(Cabmovecount:1) TO Horizontalnum.
    MOVE Verticalstring(Cabmovecount:1) TO Verticalnum.
    MOVE Horizontalnum TO Newanimatetablerow(3)(3:1).
    IF Horizontalnum = 1 THEN SUBTRACT 1 FROM Scenestartpos.
    IF Horizontalnum = 2 THEN ADD 1 TO Scenestartpos.
    PERFORM Cabinmove.
    *>MOVE Newbaserowpart TO Newrow.
    *>Weitere Bewegung durch Angabe von Newrow bestimmen
    IF Verticalnum = 1 THEN SUBTRACT 1 FROM Newrow.
    IF Verticalnum = 2 THEN ADD 1 TO Newrow.
    ADD 1 TO Cabmovecount.

    Cabinmove.
    *>Newrow außerhalb definieren
    *>MOVE Newbaserowpart TO Newrow.
    MOVE 44 TO Sceneorigin.
    PERFORM Cabinsubmove 6 TIMES.
    SUBTRACT 6 FROM Newrow.

    DISPLAY Houseanimatescreen.
    ACCEPT OMITTED WITH TIMEOUT Reacttime.
    MOVE 1 TO Initnum.
    *>IF Scenestartpos LESS THAN Scenewidth THEN ADD 1 TO Scenepartstart.
    *>IF Scenestartpos LESS THAN Scenewidth THEN SUBTRACT 1 FROM Scenewidth.

    Cabinsubmove.
    *>Bewegung aktuell nach links.
    *>MOVE 5 TO Scenestartpos.
    MOVE 10 TO Scenewidth.
    MOVE Animatetablerow(Sceneorigin)(Scenepartstart:Scenewidth) TO Newanimatetablerow(Newrow)(Scenestartpos:Scenewidth).
    *>MOVE SPACE TO Newanimatetablerow(Newrow)(Scenestartpos + Scenewidth:1).
    ADD 1 TO Newrow.
    ADD 1 TO Sceneorigin.

    Nextsteps.
    *>Startzeile und Anzahl Schritte nach unten links
    MOVE 2 TO Newrowbase.
    MOVE 1 TO Cabmovecount.
    PERFORM Severalleftmovesprogold.

    MOVE 3 TO Newbaserowpart.
    ADD 1 TO Scenestartpos.
    PERFORM Cabinrightmove 5 TIMES.

    MOVE 4 TO Newrowbase.
    MOVE 1 TO Cabmovecount.
    PERFORM Severalleftmovesprogold.

    Severalleftmovesprogold.

    *>==========================================
    *>Program for the Cabin to move to left and down.
    *>==========================================
    SUBTRACT 1 FROM Scenestartpos.
    MOVE 1 TO Sceneoutputbaserow.
    MOVE Newrowbase TO Newrow.
    ADD 1 TO Newrowbase.
    PERFORM Diagonalleftmove.
    MOVE Newrowbase TO Newbaserowpart.
    PERFORM Furtherdiagonalleftmove3 Cabmovecount TIMES.


    Furtherdiagonalleftmove3.
    ADD 1 TO Sceneoutputbaserow.
    MOVE Newbaserowpart TO Newrow.
    PERFORM Diagonalleftmove.
    IF Modenum = 2 THEN SUBTRACT 1 FROM Newbaserowpart ELSE ADD 1 TO Newbaserowpart.

    Furtherdiagonalleftmove4.
    ADD 1 TO Sceneoutputbaserow.
    MOVE 4 TO Newrow.
    PERFORM Diagonalleftmove.

    Diagonalleftmove.
    MOVE Sceneoutputbaserow TO Sceneoutputrow.
    ADD 1 TO Objectheigth.
    *>PERFORM Sidemovespacesubprog4 Objectheigth TIMES.
    MOVE 1 TO Countnum.
    PERFORM Sidemovespacesubprog5 40 TIMES.
    SUBTRACT 1 FROM Objectheigth.
    MOVE 0 TO Initnum.
    PERFORM Cabinmove.
    MOVE 1 TO Initnum.



    Cabinrightmove.

    IF Initnum = 1 THEN MOVE Newbaserowpart TO Newrow.
    MOVE 44 TO Sceneorigin.
    PERFORM Cabinrightsubmove 6 TIMES.
    ADD 1 TO Scenestartpos.
    *>IF Scenestartpos LESS THAN Scenewidth THEN ADD 1 TO Scenepartstart.
    *>IF Scenestartpos LESS THAN Scenewidth THEN SUBTRACT 1 FROM Scenewidth.
    DISPLAY Houseanimatescreen.
    ACCEPT OMITTED WITH TIMEOUT 1.

    Cabinrightsubmove.
    *>Bewegung aktuell nach links.
    MOVE Animatetablerow(Sceneorigin)(Scenepartstart:Scenewidth) TO Newanimatetablerow(Newrow)(Scenestartpos:Scenewidth).
    MOVE SPACE TO Newanimatetablerow(Newrow)(Scenestartpos - 1:1).
    ADD 1 TO Newrow.
    ADD 1 TO Sceneorigin.

    *>==========================================
    *>Program for the Cabin to move in a direction.
    *>==========================================



    Sidemovespacesubprog4.
    *>ADD 1 TO Totalscenewidth.
    MOVE SPACES TO Newanimatetablerow(Sceneoutputrow)(Scenestartpos - 1:Scenewidth + 2).
    ADD 1 TO Sceneoutputrow.

    Fieldresetprog.
    *>Emptyprogram
    Sidemovespacesubprog5.
    *>Programm zum positionieren der festen Szene
    MOVE SPACES TO Newanimatetablerow(Countnum).
    SUBTRACT 1 FROM Newrow GIVING Minrow.
    ADD 6 TO Newrow GIVING Maxrow.
    IF Countnum GREATER THAN Minrow AND LESS THAN Maxrow THEN MOVE Animatetablerow(Countnum)(1:Scenestartpos - 1) TO Newanimatetablerow(Countnum)(1:Scenestartpos - 1).
    IF Countnum GREATER THAN Minrow AND LESS THAN Maxrow THEN MOVE Animatetablerow(Countnum)(Scenestartpos + Scenewidth:135 - Scenestartpos - Scenewidth) TO Newanimatetablerow(Countnum)(Scenestartpos + Scenewidth:135 - Scenestartpos - Scenewidth).
    IF Countnum < Newrow THEN MOVE Animatetablerow(Countnum) TO Newanimatetablerow(Countnum).
    IF Countnum > Newrow + 5 THEN MOVE Animatetablerow(Countnum) TO Newanimatetablerow(Countnum).
    ADD 1 TO Countnum.
    *>ADD 1 TO Sceneoutputrow.

    Scenemoveprog.
    *>Programm zum Gesamtszene verschieben
    MOVE Countnumbase TO Countnum.
    MOVE 1 TO Rowposbase.
    PERFORM Displayinit Objectheigth TIMES.
    ADD 1 TO Countnumbase.
    DISPLAY Houseanimatescreen.
    ACCEPT OMITTED WITH TIMEOUT 1.

    Inputmove.
    READ Inputfile INTO Animatetablerow(Countnum).
    ADD 1 TO Countnum.

    Displayinit.
    MOVE Animatetablerow(Countnum)(1:135) TO Newanimatetablerow(Rowposbase)(1:135).
    ADD 1 TO Countnum.
    ADD 1 TO Rowposbase.


    Newanimatetableinit.
    MOVE SPACES TO Newanimatetablerow(Relrow).
    ADD 1 TO Relrow.

    Housescenewriteprog.

    WRITE Record-Name3 FROM Newanimatetablerow(Relrow).
    ADD 1 TO Relrow.

    Houserowdisplaystart.
    PERFORM Houserownumberaddprog.
    PERFORM Houserowdisplayprog.
    DISPLAY Houseanimatescreen.
    ACCEPT Houseanimatescreen.

    Houserowdisplayprog.
    MOVE 1 TO Newbaserow.
    PERFORM Houserowdisplaysub Totalheigthres TIMES.

    Houserowdisplaysub.
    MOVE Newanimatetablerow(Relrow) TO Newanimatetablerow(Newbaserow).
    ADD 1 TO Relrow.
    ADD 1 TO Newbaserow.

    Houserownumberaddprog.
    *>Program to calculate Relrow
    MOVE 40 TO Relrow.
    MOVE Rowposbaseminrows(Houserownumber) TO Totalheigthres.
    SUBTRACT 1 FROM Houserownumber.
    MOVE 1 TO Houserowaddnumber.
    IF Houserownumber GREATER THAN ZERO THEN PERFORM Subaddprog Houserownumber TIMES.

    Subaddprog.
    MOVE Rowposbaseminrows(Houserowaddnumber) TO Addbase.
    ADD Addbase TO Relrow.
    ADD 1 TO Houserowaddnumber.

    Cabinmoveprog.
    MOVE 7 TO Scenewidth.
    MOVE 12 TO Scenestartpos.
    MOVE 1 TO Scenepartstart.
    *>PERFORM Cabininitprog.
    MOVE 18 TO Newrow.
    PERFORM Cabinrightmove 12 TIMES.

    END PROGRAM YOUR-PROGRAM-NAME.