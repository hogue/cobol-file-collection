IDENTIFICATION DIVISION.
PROGRAM-ID. YOUR-PROGRAM-NAME.
ENVIRONMENT DIVISION.
INPUT-OUTPUT SECTION.
FILE-CONTROL.
    SELECT Inputfile ASSIGN TO "D:\Cobolprogramme\Simulatorgame\Housetemplates.txt"
    ORGANIZATION IS LINE SEQUENTIAL.
DATA DIVISION.
FILE SECTION.
    FD  Inputfile.
    01  Record-Name3.
    02 Field1                  PIC X(135).

WORKING-STORAGE SECTION.
01 Str1 PIC X(20).
01 Objectstore PIC X(135).
01 Str2 PIC X(1).
01 Saveindicator PIC X(1).
01 Saveindicator2 PIC X(1).
01 Num PIC 9(4).
01 Num2 PIC 9(4).
01 Insertpos PIC 9(4).
01 Scenestartpos PIC 9(4).
01 Scenecountpos PIC 9(4).
01 Scenewidth PIC 9(4).
01 Totalscenewidth PIC 9(4).
01 Scenecorrection PIC 9(4).
01 Scenebaserow PIC 9(4).
01 Sceneoutputrow PIC 9(4).
01 Sceneoutputbaserow PIC 9(4).
01 Indentlevel PIC 9(4).
01 Startcol PIC 9(4).
01 Correctionval PIC 9(4).
01 Objectwidth PIC 9(4).
01 Objectheigth PIC 9(4).
01 Totalheigth PIC 9(4).
01 Totalheigthres PIC 9(4).
01 Houserownumber PIC 9(4).
01 Houserowaddnumber PIC 9(4).
01 Housenumber PIC 9(4).
01 Relrow PIC 9(4).
01 Addbase PIC 9(4).
01 Newbaserow PIC 9(4).
01 Newbaserowpart PIC 9(4).
01 Bottomrow PIC 9(4).
01 Rowposbaseaddition PIC S9(4).
01 Rowposbase PIC 9(4).
01 Rowposbasemin PIC 9(4).
01 Rowposres PIC 9(4).
01 Countnum PIC 9(4).
01 Furthermoveprog PIC 9(4).
01 Countnumbase PIC 9(4).
01 Sceneorigin PIC 9(4).
01 Scenepartstart PIC 9(4).
01 Houserowwidth PIC 9(4).
01 Newrow PIC 9(4).
01 Cabmovecount PIC 9(4).
01 Newrowbase PIC 9(4).
01 Minpos PIC 9(4).
01 Minrow PIC 9(4).
01 Maxrow PIC 9(4).
01 Subcount PIC 9(4).
01 Reacttime PIC 9(4).
01 Windowcount PIC 9(2).
01 Depthval PIC 9(2).
01 Doorpos PIC 9(2).
01 Initnum PIC 9(1).
01 Modenum PIC 9(1).
*>Variablen zur Richtungsangabe
*>1 gleich hoch, 2 gleich runter, 0 neutral
01 Horizontalnum PIC 9(1).
*>1 gleich links, 2 gleich rechts, 0 neutral
01 Verticalnum PIC 9(1).
*>Zeichenketten mit Richtungsangaben
01 Horizontalstring PIC X(100).
01 Verticalstring PIC X(100).
01 Levelcount PIC 9(2).
01 Baseline1 PICTURE X(135).
01 Baseline2 PICTURE X(135).
01 Baseline3 PICTURE X(135).
01 Inputstring PICTURE X(135).
01 Rowwidthtable PIC 9(4) OCCURS 1000 TIMES.
01 Rowposbaseminrows PIC 9(4) OCCURS 1000 TIMES.
01 Animatetablerow PIC X(135) OCCURS 1000 TIMES.
01 Newanimatetablerow PIC X(135) OCCURS 1000 TIMES.

SCREEN SECTION.

01 Housegeneratorscreen BACKGROUND-COLOR 3 FOREGROUND-COLOUR 0.
05  VALUE "Housegenerator" BLANK SCREEN     LINE 1 COL 15.
05 VALUE "Fenster:" LINE 19 COL 1.
05 VALUE "Tiefe: " LINE 20 COL 1.
05 VALUE "Stockwerke: " LINE 21 COL 1.
05 VALUE "Tuerposition: " LINE 22 COL 1.
05 VALUE "Hauserreihe fertig: " LINE 23 COL 1.
05  Windowcountinput LINE  19 COLUMN 18 PIC 9(2) TO Windowcount.
05  Depthvalinput LINE 20 COLUMN 18 PIC 9(2) TO Depthval.
05  Levelcountinput LINE 21 COLUMN 18 PIC 9(2) TO Levelcount.
05  Doorposinput LINE 22 COLUMN 18 PIC 9(2) TO Doorpos.
05  Saveindicatorinput1 LINE 23 COLUMN 25 PIC X(1) TO Saveindicator.
05 Animatetext80_1 PIC X(135) FROM Animatetablerow(80) LINE 2 COL 1.
05 Animatetext81_1 PIC X(135) FROM Animatetablerow(81) LINE 3 COL 1.
05 Animatetext82_1 PIC X(135) FROM Animatetablerow(82) LINE 4 COL 1.
05 Animatetext83_1 PIC X(135) FROM Animatetablerow(83) LINE 5 COL 1.
05 Animatetext84_1 PIC X(135) FROM Animatetablerow(84) LINE 6 COL 1.
05 Animatetext85_1 PIC X(135) FROM Animatetablerow(85) LINE 7 COL 1.
05 Animatetext86_1 PIC X(135) FROM Animatetablerow(86) LINE 8 COL 1.
05 Animatetext87_1 PIC X(135) FROM Animatetablerow(87) LINE 9 COL 1.
05 Animatetext88_1 PIC X(135) FROM Animatetablerow(88) LINE 10 COL 1.
05 Animatetext89_1 PIC X(135) FROM Animatetablerow(89) LINE 11 COL 1.
05 Animatetext90_1 PIC X(135) FROM Animatetablerow(90) LINE 12 COL 1.
05 Animatetext91_1 PIC X(135) FROM Animatetablerow(91) LINE 13 COL 1.
05 Animatetext92_1 PIC X(135) FROM Animatetablerow(92) LINE 14 COL 1.
05 Animatetext93_1 PIC X(135) FROM Animatetablerow(93) LINE 15 COL 1.
05 Animatetext94_1 PIC X(135) FROM Animatetablerow(94) LINE 16 COL 1.
05 Animatetext95_1 PIC X(135) FROM Animatetablerow(95) LINE 17 COL 1.

01 Houseresultscreen BACKGROUND-COLOR 3 FOREGROUND-COLOUR 0.
05  VALUE "Houseresult" BLANK SCREEN     LINE 1 COL 15.
05  Saveindicatorinput2 LINE 23 COLUMN 19 PIC X(1) TO Saveindicator.
05 Animatetext80 PIC X(135) FROM Animatetablerow(80) LINE 2 COL 1.
05 Animatetext81 PIC X(135) FROM Animatetablerow(81) LINE 3 COL 1.
05 Animatetext82 PIC X(135) FROM Animatetablerow(82) LINE 4 COL 1.
05 Animatetext83 PIC X(135) FROM Animatetablerow(83) LINE 5 COL 1.
05 Animatetext84 PIC X(135) FROM Animatetablerow(84) LINE 6 COL 1.
05 Animatetext85 PIC X(135) FROM Animatetablerow(85) LINE 7 COL 1.
05 Animatetext86 PIC X(135) FROM Animatetablerow(86) LINE 8 COL 1.
05 Animatetext87 PIC X(135) FROM Animatetablerow(87) LINE 9 COL 1.
05 Animatetext88 PIC X(135) FROM Animatetablerow(88) LINE 10 COL 1.
05 Animatetext89 PIC X(135) FROM Animatetablerow(89) LINE 11 COL 1.
05 Animatetext90 PIC X(135) FROM Animatetablerow(90) LINE 12 COL 1.
05 Animatetext91 PIC X(135) FROM Animatetablerow(91) LINE 13 COL 1.
05 Animatetext92 PIC X(135) FROM Animatetablerow(92) LINE 14 COL 1.
05 Animatetext93 PIC X(135) FROM Animatetablerow(93) LINE 15 COL 1.
05 Animatetext94 PIC X(135) FROM Animatetablerow(94) LINE 16 COL 1.
05 Animatetext95 PIC X(135) FROM Animatetablerow(95) LINE 17 COL 1.


01 Houseanimatescreen BACKGROUND-COLOR 3 FOREGROUND-COLOUR 0.
05  VALUE "Houseanimationscene    Housenumber: " BLANK SCREEN LINE 1 COL 15.
05  Saveindicatorinput4 LINE 26 COLUMN 19 PIC X(1) TO Saveindicator.
05 VALUE "Housenumber: " LINE 27 COL 2.
05 Housenumberinput LINE 27 COLUMN 18 PIC 9(4) TO Houserownumber.
05 Animatetext80_3 PIC X(135) FROM Newanimatetablerow(1) LINE 2 COL 1.
05 Animatetext81_3 PIC X(135) FROM Newanimatetablerow(2) LINE 3 COL 1.
05 Animatetext82_3 PIC X(135) FROM Newanimatetablerow(3) LINE 4 COL 1.
05 Animatetext83_3 PIC X(135) FROM Newanimatetablerow(4) LINE 5 COL 1.
05 Animatetext84_3 PIC X(135) FROM Newanimatetablerow(5) LINE 6 COL 1.
05 Animatetext85_3 PIC X(135) FROM Newanimatetablerow(6) LINE 7 COL 1.
05 Animatetext86_3 PIC X(135) FROM Newanimatetablerow(7) LINE 8 COL 1.
05 Animatetext87_3 PIC X(135) FROM Newanimatetablerow(8) LINE 9 COL 1.
05 Animatetextin_3put PIC X(135) FROM Newanimatetablerow(9) LINE 10 COL 1.
05 Animatetext89_3 PIC X(135) FROM Newanimatetablerow(10) LINE 11 COL 1.
05 Animatetext90_3 PIC X(135) FROM Newanimatetablerow(11) LINE 12 COL 1.
05 Animatetext91_3 PIC X(135) FROM Newanimatetablerow(12) LINE 13 COL 1.
05 Animatetext92_3 PIC X(135) FROM Newanimatetablerow(13) LINE 14 COL 1.
05 Animatetext93_3 PIC X(135) FROM Newanimatetablerow(14) LINE 15 COL 1.
05 Animatetext94_3 PIC X(135) FROM Newanimatetablerow(15) LINE 16 COL 1.
05 Animatetext95_3 PIC X(135) FROM Newanimatetablerow(16) LINE 17 COL 1.
05 Animatetext96_3 PIC X(135) FROM Newanimatetablerow(17) LINE 18 COL 1.
05 Animatetext97_3 PIC X(135) FROM Newanimatetablerow(18) LINE 19 COL 1.
05 Animatetext98_3 PIC X(135) FROM Newanimatetablerow(19) LINE 20 COL 1.
05 Animatetext99_3 PIC X(135) FROM Newanimatetablerow(20) LINE 21 COL 1.
05 Animatetext10_30 PIC X(135) FROM Newanimatetablerow(21) LINE 22 COL 1.
05 Animatetext10_31 PIC X(135) FROM Newanimatetablerow(22) LINE 23 COL 1.
05 Animatetext10_32 PIC X(135) FROM Newanimatetablerow(23) LINE 24 COL 1.

01 Doorposcorrectionscreen BACKGROUND-COLOR 3 FOREGROUND-COLOUR 0.
05  VALUE "Doorposcorrection" BLANK SCREEN LINE 1 COL 15.
05 VALUE "Fenster:" LINE 19 COL 1.
05 VALUE "Tiefe: " LINE 20 COL 1.
05 VALUE "Stockwerke: " LINE 21 COL 1.
05 VALUE "Tuerposition: " LINE 22 COL 1.
05  Windowcountinput2 LINE  19 COLUMN 18 PIC 9(2) TO Windowcount.
05  Depthvalinput2 LINE 20 COLUMN 18 PIC 9(2) TO Depthval.
05  Levelcountinput2 LINE 21 COLUMN 18 PIC 9(2) TO Levelcount.
05  Doorposinput2 LINE 22 COLUMN 18 PIC 9(2) TO Doorpos.
05  Saveindicatorinput4 LINE 23 COLUMN 19 PIC X(1) TO Saveindicator.
05 Animatetext80 PIC X(135) FROM Newanimatetablerow(1) LINE 2 COL 1.
05 Animatetext81 PIC X(135) FROM Newanimatetablerow(2) LINE 3 COL 1.
05 Animatetext82_2 PIC X(135) FROM Newanimatetablerow(3) LINE 4 COL 1.
05 Animatetext83_2 PIC X(135) FROM Newanimatetablerow(4) LINE 5 COL 1.
05 Animatetext84_2 PIC X(135) FROM Newanimatetablerow(5) LINE 6 COL 1.
05 Animatetext85_2 PIC X(135) FROM Newanimatetablerow(6) LINE 7 COL 1.
05 Animatetext86_2 PIC X(135) FROM Newanimatetablerow(7) LINE 8 COL 1.
05 Animatetext87_2 PIC X(135) FROM Newanimatetablerow(8) LINE 9 COL 1.
05 Animatetext88_2 PIC X(135) FROM Newanimatetablerow(9) LINE 10 COL 1.
05 Animatetext89_2 PIC X(135) FROM Newanimatetablerow(10) LINE 11 COL 1.
05 Animatetext90_2 PIC X(135) FROM Newanimatetablerow(11) LINE 12 COL 1.
05 Animatetext91_2 PIC X(135) FROM Newanimatetablerow(12) LINE 13 COL 1.
05 Animatetext92_2 PIC X(135) FROM Newanimatetablerow(13) LINE 14 COL 1.
05 Animatetext93_2 PIC X(135) FROM Newanimatetablerow(14) LINE 15 COL 1.
05 Animatetext94_2 PIC X(135) FROM Newanimatetablerow(15) LINE 16 COL 1.
05 Animatetext95_2 PIC X(135) FROM Newanimatetablerow(16) LINE 17 COL 1.

01  DATA-ENTRY-SCREEN BACKGROUND-COLOR 3 FOREGROUND-COLOUR 0.
    05  VALUE "ASCII-ART ANIMATION" BLANK SCREEN     LINE 1 COL 15.
    05  Rolltext1 LINE 6 COL 17 PIC X(135).
    05  Rolltext2 LINE 7 COL 17 PIC X(135).
    05  Rolltext3 LINE 8 COL 17 PIC X(135).
    05  Rolltext4 LINE 9 COL 17 PIC X(135).
    05  Rolltext5 LINE 10 COL 17 PIC X(135).
    05  Lineinput13 PICTURE X(135) FROM Baseline3 LINE 13 COL 1.
    05  Lineinput14 PICTURE X(135) FROM Baseline2 LINE 14 COL 1.
    05  Lineinput15 PICTURE X(135) FROM Baseline1 LINE 15 COL 1.
    05   ID-INPUT  PIC  9(4)       FROM Num                         LINE 3 COL 1.
    05   ID-INPUT2  PIC  9(4)       FROM Num                        LINE 4 COL 1.

    01 Animatescreen BACKGROUND-COLOR 3 FOREGROUND-COLOR 0.
*>05 Animatetext1 LINE 1 COL 1 PIC X(135).
05  VALUE "EISENBAHNANIMATION" BLANK SCREEN     LINE 1 COL 15.
05 Animatetext2 PIC X(135) FROM Animatetablerow(2) LINE 2 COL 1.
05 Animatetext3 PIC X(135) FROM Animatetablerow(3) LINE 3 COL 1.
05 Animatetext4 PIC X(135) FROM Animatetablerow(4) LINE 4 COL 1.
05 Animatetext5 PIC X(135) FROM Animatetablerow(5) LINE 5 COL 1.
05 Animatetext6 PIC X(135) FROM Animatetablerow(6) LINE 6 COL 1.
05 Animatetext7 PIC X(135) FROM Animatetablerow(7) LINE 7 COL 1.
05 Animatetext8 PIC X(135) FROM Animatetablerow(8) LINE 8 COL 1.
05 Animatetext9 PIC X(135) FROM Animatetablerow(9) LINE 9 COL 1.
05 Animatetext10 PIC X(135) FROM Animatetablerow(10) LINE 10 COL 1.
05 Animatetext11 PIC X(135) FROM Animatetablerow(11) LINE 11 COL 1.
05 Animatetext12 PIC X(135) FROM Animatetablerow(12) LINE 12 COL 1.
05 Animatetext13 PIC X(135) FROM Animatetablerow(13) LINE 13 COL 1.
05 Animatetext14 PIC X(135) FROM Animatetablerow(14) LINE 14 COL 1.
05 Animatetext15 PIC X(135) FROM Animatetablerow(15) LINE 15 COL 1.
05 Animatetext16 PIC X(135) FROM Animatetablerow(16) LINE 16 COL 1.
05 Animatetext17 PIC X(135) FROM Animatetablerow(17) LINE 17 COL 1.
05 Animatetext18 PIC X(135) FROM Animatetablerow(18) LINE 18 COL 1.
05 Animatetext19 PIC X(135) FROM Animatetablerow(19) LINE 19 COL 1.
05 Animatetext20 PIC X(135) FROM Animatetablerow(20) LINE 20 COL 1.
05 Animatetext21 PIC X(135) FROM Animatetablerow(21) LINE 21 COL 1.
05 Animatetext22 PIC X(135) FROM Animatetablerow(22) LINE 22 COL 1.
05 Animatetext23 PIC X(135) FROM Animatetablerow(23) LINE 23 COL 1.
05 Animatetext24 PIC X(135) FROM Animatetablerow(24) LINE 24 COL 1.
05 Animatetext25 PIC X(135) FROM Animatetablerow(25) LINE 25 COL 1.
05 Animatetext26 PIC X(135) FROM Animatetablerow(26) LINE 26 COL 1.
05 Animatetext27 PIC X(135) FROM Animatetablerow(27) LINE 27 COL 1.
05 Animatetext28 PIC X(135) FROM Animatetablerow(28) LINE 28 COL 1.
05 Animatetext29 PIC X(135) FROM Animatetablerow(29) LINE 29 COL 1.
05 Animatetext30 PIC X(135) FROM Animatetablerow(30) LINE 30 COL 1.
05   ID-INPUT3  PIC  9(4)       FROM Num                         LINE 1 COL 58.

PROCEDURE DIVISION.
    MAIN-PROCEDURE.
    *>PERFORM Houserowdesignerprog.
    MOVE 1 TO Countnum.
    MOVE 2 TO Newbaserowpart.
    OPEN INPUT Inputfile.
    PERFORM Inputmove 50 TIMES.
    CLOSE Inputfile.
    MOVE 1 TO Initnum.
    MOVE 1 TO Countnumbase.
    MOVE 23 TO Objectheigth.
    PERFORM Scenemoveprog.
    MOVE 10 TO Scenewidth.
    MOVE 6 TO Objectheigth.
    MOVE 2 TO Scenepartstart.
    MOVE 2 TO Scenestartpos.
    MOVE 1 TO Modenum.
    MOVE 1 TO Reacttime.
    *>MOVE 1 TO Newbaserowpart.
    *>PERFORM Cabinrightmove 3 TIMES.
    *>MOVE 1 TO Countnum.
    *>PERFORM Sidemovespacesubprog5 40 TIMES.
    *>MOVE 3 TO Newrow.
    *>PERFORM Cabinleftmove.
*>Variablen zur Richtungsangabe
*>1 gleich hoch, 2 gleich runter, 0 neutral
*>1 gleich links, 2 gleich rechts, 0 neutral
    MOVE "11100222000000000000000000002222222222220000000000000000" TO Verticalstring.
    MOVE "22211112022222222222222222220000011111112222222222222222" TO Horizontalstring.
    MOVE 5 TO Newbaserowpart.
    MOVE Newbaserowpart TO Newrow.
    MOVE 9 TO Scenestartpos.
    MOVE 1 TO Cabmovecount.
    MOVE 0 TO Horizontalnum.
    MOVE 0 TO Verticalnum.
    PERFORM Severalcabmovesprog UNTIL Cabmovecount = 50.
    *>Startzeile und Anzahl Schritte nach oben links, da Countnum gleich 2
    *>MOVE 2 TO Modenum.
    *>MOVE 4 TO Newrowbase.
    *>MOVE 5 TO Cabmovecount.
    *>PERFORM Severalleftmovesprog.

    DISPLAY Houseanimatescreen.
    ACCEPT Houseanimatescreen.
    STOP RUN.

    Severalcabmovesprog.
    *>Needs values for Newrow, Newrowbase and Scenestartpos.
    MOVE 1 TO Countnum.
    PERFORM Sidemovespacesubprog5 40 TIMES.
    *>Bewegung nach links oder rechts durch Anpassung von Scenestartpos.
    MOVE Horizontalstring(Cabmovecount:1) TO Horizontalnum.
    MOVE Verticalstring(Cabmovecount:1) TO Verticalnum.
    MOVE Horizontalnum TO Newanimatetablerow(3)(3:1).
    IF Horizontalnum = 1 THEN SUBTRACT 1 FROM Scenestartpos.
    IF Horizontalnum = 2 THEN ADD 1 TO Scenestartpos.
    PERFORM Cabinmove.
    *>MOVE Newbaserowpart TO Newrow.
    *>Weitere Bewegung durch Angabe von Newrow bestimmen
    IF Verticalnum = 1 THEN SUBTRACT 1 FROM Newrow.
    IF Verticalnum = 2 THEN ADD 1 TO Newrow.
    ADD 1 TO Cabmovecount.

    Cabinmove.
    *>Newrow außerhalb definieren
    *>MOVE Newbaserowpart TO Newrow.
    MOVE 44 TO Sceneorigin.
    PERFORM Cabinsubmove 6 TIMES.
    SUBTRACT 6 FROM Newrow.

    DISPLAY Houseanimatescreen.
    ACCEPT OMITTED WITH TIMEOUT Reacttime.
    MOVE 1 TO Initnum.
    *>IF Scenestartpos LESS THAN Scenewidth THEN ADD 1 TO Scenepartstart.
    *>IF Scenestartpos LESS THAN Scenewidth THEN SUBTRACT 1 FROM Scenewidth.

    Cabinsubmove.
    *>Bewegung aktuell nach links.
    *>MOVE 5 TO Scenestartpos.
    MOVE 10 TO Scenewidth.
    MOVE Animatetablerow(Sceneorigin)(Scenepartstart:Scenewidth) TO Newanimatetablerow(Newrow)(Scenestartpos:Scenewidth).
    *>MOVE SPACE TO Newanimatetablerow(Newrow)(Scenestartpos + Scenewidth:1).
    ADD 1 TO Newrow.
    ADD 1 TO Sceneorigin.

    Nextsteps.
    *>Startzeile und Anzahl Schritte nach unten links
    MOVE 2 TO Newrowbase.
    MOVE 1 TO Cabmovecount.
    PERFORM Severalleftmovesprogold.

    MOVE 3 TO Newbaserowpart.
    ADD 1 TO Scenestartpos.
    PERFORM Cabinrightmove 5 TIMES.

    MOVE 4 TO Newrowbase.
    MOVE 1 TO Cabmovecount.
    PERFORM Severalleftmovesprogold.

    Severalleftmovesprogold.

    *>==========================================
    *>Program for the Cabin to move to left and down.
    *>==========================================
    SUBTRACT 1 FROM Scenestartpos.
    MOVE 1 TO Sceneoutputbaserow.
    MOVE Newrowbase TO Newrow.
    ADD 1 TO Newrowbase.
    PERFORM Diagonalleftmove.
    MOVE Newrowbase TO Newbaserowpart.
    PERFORM Furtherdiagonalleftmove3 Cabmovecount TIMES.


    Furtherdiagonalleftmove3.
    ADD 1 TO Sceneoutputbaserow.
    MOVE Newbaserowpart TO Newrow.
    PERFORM Diagonalleftmove.
    IF Modenum = 2 THEN SUBTRACT 1 FROM Newbaserowpart ELSE ADD 1 TO Newbaserowpart.

    Furtherdiagonalleftmove4.
    ADD 1 TO Sceneoutputbaserow.
    MOVE 4 TO Newrow.
    PERFORM Diagonalleftmove.

    Diagonalleftmove.
    MOVE Sceneoutputbaserow TO Sceneoutputrow.
    ADD 1 TO Objectheigth.
    *>PERFORM Sidemovespacesubprog4 Objectheigth TIMES.
    MOVE 1 TO Countnum.
    PERFORM Sidemovespacesubprog5 40 TIMES.
    SUBTRACT 1 FROM Objectheigth.
    MOVE 0 TO Initnum.
    PERFORM Cabinmove.
    MOVE 1 TO Initnum.



    Cabinrightmove.

    IF Initnum = 1 THEN MOVE Newbaserowpart TO Newrow.
    MOVE 44 TO Sceneorigin.
    PERFORM Cabinrightsubmove 6 TIMES.
    ADD 1 TO Scenestartpos.
    *>IF Scenestartpos LESS THAN Scenewidth THEN ADD 1 TO Scenepartstart.
    *>IF Scenestartpos LESS THAN Scenewidth THEN SUBTRACT 1 FROM Scenewidth.
    DISPLAY Houseanimatescreen.
    ACCEPT OMITTED WITH TIMEOUT 1.

    Cabinrightsubmove.
    *>Bewegung aktuell nach links.
    MOVE Animatetablerow(Sceneorigin)(Scenepartstart:Scenewidth) TO Newanimatetablerow(Newrow)(Scenestartpos:Scenewidth).
    MOVE SPACE TO Newanimatetablerow(Newrow)(Scenestartpos - 1:1).
    ADD 1 TO Newrow.
    ADD 1 TO Sceneorigin.

    *>==========================================
    *>Program for the Cabin to move in a direction.
    *>==========================================



    Sidemovespacesubprog4.
    *>ADD 1 TO Totalscenewidth.
    MOVE SPACES TO Newanimatetablerow(Sceneoutputrow)(Scenestartpos - 1:Scenewidth + 2).
    ADD 1 TO Sceneoutputrow.

    Fieldresetprog.
    *>Emptyprogram
    Sidemovespacesubprog5.
    *>Programm zum positionieren der festen Szene
    MOVE SPACES TO Newanimatetablerow(Countnum).
    SUBTRACT 1 FROM Newrow GIVING Minrow.
    ADD 6 TO Newrow GIVING Maxrow.
    IF Countnum GREATER THAN Minrow AND LESS THAN Maxrow THEN MOVE Animatetablerow(Countnum)(1:Scenestartpos - 1) TO Newanimatetablerow(Countnum)(1:Scenestartpos - 1).
    IF Countnum GREATER THAN Minrow AND LESS THAN Maxrow THEN MOVE Animatetablerow(Countnum)(Scenestartpos + Scenewidth:135 - Scenestartpos - Scenewidth) TO Newanimatetablerow(Countnum)(Scenestartpos + Scenewidth:135 - Scenestartpos - Scenewidth).
    IF Countnum < Newrow THEN MOVE Animatetablerow(Countnum) TO Newanimatetablerow(Countnum).
    IF Countnum > Newrow + 5 THEN MOVE Animatetablerow(Countnum) TO Newanimatetablerow(Countnum).
    ADD 1 TO Countnum.
    *>ADD 1 TO Sceneoutputrow.

    Scenemoveprog.
    *>Programm zum Gesamtszene verschieben
    MOVE Countnumbase TO Countnum.
    MOVE 1 TO Rowposbase.
    PERFORM Displayinit Objectheigth TIMES.
    ADD 1 TO Countnumbase.
    DISPLAY Houseanimatescreen.
    ACCEPT OMITTED WITH TIMEOUT 1.

    Inputmove.
    READ Inputfile INTO Animatetablerow(Countnum).
    ADD 1 TO Countnum.

    Displayinit.
    MOVE Animatetablerow(Countnum)(1:135) TO Newanimatetablerow(Rowposbase)(1:135).
    ADD 1 TO Countnum.
    ADD 1 TO Rowposbase.

    Houserowdesignerprog.
    MOVE 1 TO Relrow.
    PERFORM Newanimatetableinit 1000 TIMES.
    MOVE 1 TO Minpos.
    MOVE 40 TO Newrow.
    MOVE 40 TO Relrow.
    MOVE 1 TO Housenumber.
    *>PERFORM Initprogscene.
    *>PERFORM Layoutinit.
    PERFORM Generatortopprog 1 TIMES.
    PERFORM Housetransferprog.
    *>PERFORM Houserowdisplaystart 3 TIMES.
    MOVE 30 TO Scenecorrection.
    PERFORM Sidemovestart 20 TIMES.
    PERFORM Sidemoveinit.
    MOVE 40 TO Relrow.
    OPEN OUTPUT Inputfile.
    PERFORM Housescenewriteprog 50 TIMES.
    CLOSE Inputfile.

    Newanimatetableinit.
    MOVE SPACES TO Newanimatetablerow(Relrow).
    ADD 1 TO Relrow.

    Housescenewriteprog.

    WRITE Record-Name3 FROM Newanimatetablerow(Relrow).
    ADD 1 TO Relrow.

    Houserowdisplaystart.
    PERFORM Houserownumberaddprog.
    PERFORM Houserowdisplayprog.
    DISPLAY Houseanimatescreen.
    ACCEPT Houseanimatescreen.

    Houserowdisplayprog.
    MOVE 1 TO Newbaserow.
    PERFORM Houserowdisplaysub Totalheigthres TIMES.

    Houserowdisplaysub.
    MOVE Newanimatetablerow(Relrow) TO Newanimatetablerow(Newbaserow).
    ADD 1 TO Relrow.
    ADD 1 TO Newbaserow.

    Houserownumberaddprog.
    *>Program to calculate Relrow
    MOVE 40 TO Relrow.
    MOVE Rowposbaseminrows(Houserownumber) TO Totalheigthres.
    SUBTRACT 1 FROM Houserownumber.
    MOVE 1 TO Houserowaddnumber.
    IF Houserownumber GREATER THAN ZERO THEN PERFORM Subaddprog Houserownumber TIMES.

    Subaddprog.
    MOVE Rowposbaseminrows(Houserowaddnumber) TO Addbase.
    ADD Addbase TO Relrow.
    ADD 1 TO Houserowaddnumber.

    Cabinmoveprog.
    MOVE 7 TO Scenewidth.
    MOVE 12 TO Scenestartpos.
    MOVE 1 TO Scenepartstart.
    PERFORM Cabininitprog.
    MOVE 18 TO Newrow.
    PERFORM Cabinrightmove 12 TIMES.





    Generatortopprog.

    MOVE 0 TO Doorpos.
    MOVE 0 TO Windowcountinput.
    MOVE 0 TO Levelcountinput.
    MOVE 0 TO Depthvalinput.
    MOVE 0 TO Doorposinput.
    MOVE 0 TO Houserowwidth.
    MOVE " " TO Saveindicatorinput1.
    MOVE " " TO Saveindicatorinput2.
    MOVE 95 TO Rowposbasemin.
    PERFORM Housetransferprog.
    MOVE 1 TO Subcount.
    PERFORM Sceneextendprog UNTIL Saveindicator = "x".
    *>MOVE Rowposbaseminrows(Minpos) TO Rowposbasemin.
    *>SUBTRACT 1 FROM Minpos.
    MOVE Totalheigthres TO Rowposbaseminrows(Minpos).
    *>ADD 1 TO Minpos.
    *>MOVE Rowposbaseminrows(Minpos) TO Animatetablerow(95)(1:4).
    *>MOVE Minpos TO Animatetablerow(95)(5:4).
    ADD Depthval TO Houserowwidth.
    MOVE Houserowwidth TO Rowwidthtable(Minpos).
    *>MOVE Rowwidthtable(Minpos) TO Animatetablerow(95)(9:4).
    ADD 1 TO Minpos.
    DISPLAY Houseresultscreen.
    ACCEPT Houseresultscreen.
    *>MOVE 1 TO Newrow.
    *>Zusatz für Testfall
    *>IF Totalheigthres = 1 THEN ADD 2 TO Totalheigthres.
    PERFORM Newtabledisplayprog Totalheigthres TIMES.
    PERFORM Houserowdisplayprog.
    *>MOVE Housenumber TO Newanimatetablerow(1)(1:4).
    DISPLAY Houseanimatescreen.
    ACCEPT Houseanimatescreen.
    ADD 1 TO Housenumber.

    Initprogscene.
    MOVE "__________________________________________________________________________________________" TO Baseline1.
    MOVE "        __________________" TO Animatetablerow(3)(1:120).
    MOVE "       /                 /|" TO Animatetablerow(4)(1:120).
    MOVE "      /                 / |" TO Animatetablerow(5)(1:120).
    MOVE "     /                 /  |" TO Animatetablerow(6)(1:120).
    MOVE "    /_________________/   |" TO Animatetablerow(7)(1:120).
    MOVE "   |                  |   |________________" TO Animatetablerow(8)(1:120).
    MOVE "   |   _____   _____  |  /                /|                       " TO Animatetablerow(9)(1:120).
    MOVE "   |   |   |   |   |  | /                / |      ________________________ " TO Animatetablerow(10)(1:120).
    MOVE "   |   |___|   |___|  |/________________/  |     /_/______________//_____/\" TO Animatetablerow(11)(1:120).
    MOVE "   |   _____   _____  |  ____________   |  |    /_| ____________  ||   __\/\" TO Animatetablerow(12)(1:120).
    MOVE "   |   |   |   |   |  |  |          |   |  /___/__| | ||___|___|  ||   |__\/|______________________________________XX"TO Animatetablerow(13)(1:120).
    MOVE "   |   |___|   |  .|  |  |__________|   | /__/_|____|_|___________||_______|/__/__/__/__/__/__\__/__/__/__/__/__/__XX" TO Animatetablerow(14)(1:120).
    MOVE "   |___________|___|__|_________________|/                                              \__\_" TO Animatetablerow(15)(1:120).
    MOVE "                                                                                          \__\_" TO Animatetablerow(16)(1:120).
    MOVE "____________________________________________________________________________________________\__\________________________" TO Animatetablerow(17)(1:120).
    MOVE "__/__/__/__/__/__/__\__\__/__/__/__/__/__/__/__/__/__/__/__/__/__/__/__/__/__/__/__/__/__/__/\_/__/__/__/__/__/__/__/__/" TO Animatetablerow(18)(1:120).
    MOVE "                      \__\_" TO Animatetablerow(19)(1:120).
    MOVE "________________________\__\____________________________________________________________________________________________" TO Animatetablerow(20)(1:120).
    MOVE "__/__/__/__/__/__/__/__/_\___\_/__/__/__/__/__/__/__/__/__/__/__/__/__/__/__/__/__/__/__/__/_/__/__/__/__/__/__/__/__/__" TO Animatetablerow(21)(1:120).

    Cabininitprog.
    MOVE "  ____ " TO Animatetablerow(1).
    MOVE " /   /|" TO Animatetablerow(2).
    MOVE "/___/ |" TO Animatetablerow(3).
    MOVE "|   | |" TO Animatetablerow(4).
    MOVE "|   | /" TO Animatetablerow(5).
    MOVE "|___|/ " TO Animatetablerow(6).

    Sceneextendprog.
    DISPLAY Housegeneratorscreen.
    ACCEPT Housegeneratorscreen.
    PERFORM Housegeneratorprog.

    Sceneprog.
    MOVE 1 TO Num.
    PERFORM Layoutinit.
    MOVE 1 TO Countnum.
    PERFORM Railwaylayoutprog 120 TIMES.
    PERFORM Textmove.
    PERFORM Displayprog 50 TIMES.
    MOVE " " TO Rolltext1.
    MOVE " " TO Rolltext2.
    MOVE " " TO Rolltext3.
    MOVE " " TO Rolltext4.
    MOVE " " TO Rolltext5.
    MOVE "xxxxxxx|~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~" TO Baseline1.
    MOVE "xxxxxxx|" TO Baseline2.
    MOVE "xxxxxxx|" TO Baseline3.
    PERFORM Displayprog 20 TIMES.
    PERFORM Ferrymove 12 TIMES.

    Layoutinit.
    MOVE "       __________________________________" TO Animatetablerow(31)(1:120).
    MOVE "      /                 _______________  /|" TO Animatetablerow(32)(1:120).
    MOVE "     /                 /              /|/ |" TO Animatetablerow(33)(1:120).
    MOVE "    /                 /______________/ |  |" TO Animatetablerow(34)(1:120).
    MOVE "   /__________________|              | |  |_________                           " TO Animatetablerow(35)(1:120).
    MOVE "   |                  |              | | /          \__________________________" TO Animatetablerow(36)(1:120).
    MOVE "   |   _____   _____  |              | |/                                    /|" TO Animatetablerow(37)(1:120).
    MOVE "   |   |   |   |   |  |              | /                                    / |" TO Animatetablerow(38)(1:120).
    MOVE "   |   |___|   |___|  |              |/____________________________________/  |" TO Animatetablerow(39)(1:120).
    MOVE "   |   _____   _____  |              |   ____________   ____________       |  |" TO Animatetablerow(40)(1:120).
    MOVE "   |   |   |   |   |  |              |   |          |   |          |       |  /" TO Animatetablerow(41)(1:120).
    MOVE "   |   |___|   |  .|  |              |   |__________|   |__________|       | /" TO Animatetablerow(42)(1:120).
    MOVE "   |___________|___|__|______________|_____________________________________|/ " TO Animatetablerow(43)(1:120).
    MOVE "       __________________" TO Animatetablerow(44)(1:120).
    MOVE "      /                 /|" TO Animatetablerow(45)(1:120).
    MOVE "     /                 / |" TO Animatetablerow(46)(1:120).
    MOVE "    /_________________/  |" TO Animatetablerow(47)(1:120).
    MOVE "    |  ____________   |  |" TO Animatetablerow(48)(1:120).
    MOVE "    |  |          |   |  /" TO Animatetablerow(49)(1:120).
    MOVE "    |  |__________|   | /" TO Animatetablerow(50)(1:120).
    MOVE "    |_________________|/ " TO Animatetablerow(51)(1:120).
    MOVE "  ______________________________________________________________________" TO Animatetablerow(52).
    MOVE "  /                                                                    /|" TO Animatetablerow(53).
    MOVE " /                                                                    / |" TO Animatetablerow(54).
    MOVE "/____________________________________________________________________/  |" TO Animatetablerow(55).
    MOVE "|                                                                    |  |" TO Animatetablerow(56).
    MOVE "|                                                                    |  |" TO Animatetablerow(57).
    MOVE "|                                                                    |  |" TO Animatetablerow(58).
    MOVE "|                                                                    |  |" TO Animatetablerow(59).
    MOVE "|                                                                    |  |" TO Animatetablerow(60).
    MOVE "|                                                                    |  /" TO Animatetablerow(61).
    MOVE "|                                                                    | /" TO Animatetablerow(62).
    MOVE "|____________________________________________________________________|/" TO Animatetablerow(63).
    MOVE "    _______________" TO Animatetablerow(64)(1:120).
    MOVE "   /              /|" TO Animatetablerow(65)(1:120).
    MOVE "  /              / |" TO Animatetablerow(66)(1:120).
    MOVE " /              /  |" TO Animatetablerow(67)(1:120).
    MOVE "/______________/   |" TO Animatetablerow(68)(1:120).
    MOVE "| _____  _____ |   |" TO Animatetablerow(69)(1:120).
    MOVE "| |   |  |   | |   |" TO Animatetablerow(70)(1:120).
    MOVE "| |___|  |___| |   |" TO Animatetablerow(71)(1:120).
    MOVE "| _____  _____ |   /" TO Animatetablerow(72)(1:120).
    MOVE "| |   |  |   | |  /"TO Animatetablerow(73)(1:120).
    MOVE "| |___|  |  .| | /" TO Animatetablerow(74)(1:120).
    MOVE "|________|___|_|/" TO Animatetablerow(75)(1:120).
    *>Zeilen in denen das Fenster gespeichert wird, war davor ab 77
    MOVE " _____ " TO Animatetablerow(1)(1:7).
    MOVE " |   | " TO Animatetablerow(2)(1:7).
    MOVE " |___| " TO Animatetablerow(3)(1:7).

    Housetransferprog.
    MOVE " " TO Animatetablerow(80).
    MOVE " " TO Animatetablerow(81).
    MOVE " " TO Animatetablerow(82).
    MOVE " " TO Animatetablerow(83).
    MOVE " " TO Animatetablerow(84).
    MOVE " " TO Animatetablerow(85).
    MOVE " " TO Animatetablerow(86).
    MOVE " " TO Animatetablerow(87).
    MOVE " " TO Animatetablerow(88).
    MOVE " " TO Animatetablerow(89).
    MOVE " " TO Animatetablerow(90).
    MOVE " " TO Animatetablerow(91).
    MOVE " " TO Animatetablerow(92).
    MOVE " " TO Animatetablerow(93).
    MOVE " " TO Animatetablerow(94).
    MOVE " " TO Animatetablerow(95).
    MOVE " " TO Newanimatetablerow(1).
    MOVE " " TO Newanimatetablerow(2).
    MOVE " " TO Newanimatetablerow(3).
    MOVE " " TO Newanimatetablerow(4).
    MOVE " " TO Newanimatetablerow(5).
    MOVE " " TO Newanimatetablerow(6).
    MOVE " " TO Newanimatetablerow(7).
    MOVE " " TO Newanimatetablerow(8).
    MOVE " " TO Newanimatetablerow(9).
    MOVE " " TO Newanimatetablerow(10).
    MOVE " " TO Newanimatetablerow(11).
    MOVE " " TO Newanimatetablerow(12).
    MOVE " " TO Newanimatetablerow(13).
    MOVE " " TO Newanimatetablerow(14).
    MOVE " " TO Newanimatetablerow(15).
    MOVE " " TO Newanimatetablerow(16).
    MOVE " " TO Newanimatetablerow(17).
    MOVE " " TO Newanimatetablerow(18).
    MOVE " " TO Newanimatetablerow(19).
    MOVE " " TO Newanimatetablerow(20).
    MOVE " " TO Newanimatetablerow(21).
    MOVE " " TO Newanimatetablerow(22).
    MOVE " " TO Newanimatetablerow(23).
    MOVE " " TO Newanimatetablerow(24).

    MOVE 0 TO Correctionval.

    Doorposcorrectionprog.
    DISPLAY Doorposcorrectionscreen.
    ACCEPT Doorposcorrectionscreen.
    MOVE 1 TO Doorpos.

    Housegeneratorprog.
    *>Levelcount
    *>Windowcount
    *>Depthval
    *>Dach ist Depthval plus 2 Ebenen hoch
    *>Hausbreite ist Windowcount mal 7 plus 2 breit
    *>Seitenwand ist Depthval plus 1 Ebene
    *>Fassade ist Levelcount mal 3 plus 1 hoch
    MULTIPLY Windowcount BY 7 GIVING Objectwidth.
    MULTIPLY Levelcount BY 3 GIVING Objectheigth.
    ADD 1 TO Objectheigth.
    ADD 3 TO Objectwidth.
    PERFORM Rowposbasecalc.
    IF Subcount = 1 AND Saveindicator = "x" THEN MOVE Objectheigth TO Totalheigthres.
    IF Subcount = 1 AND Saveindicator = "x" THEN ADD Depthval TO Totalheigthres.
    IF Subcount = 1 AND Saveindicator = "x" THEN ADD 2 TO Totalheigthres.
    MOVE 80 TO Rowposbase.
    ADD Rowposbaseaddition TO Rowposbase.
    IF Rowposbase LESS THAN Rowposbasemin THEN MOVE Rowposbase TO Rowposbasemin.
    *>Rows beyond top to be added to get house on same ground level
    *>ADD 3 TO Rowposbase.
    MOVE 77 TO Rowposres.
    ADD 2 TO Depthval GIVING Indentlevel.
    *>Correctionvalue
    ADD Correctionval TO Indentlevel.
    PERFORM Linedrawprog Objectwidth TIMES.
    ADD 1 TO Rowposbase.
    SUBTRACT 1 FROM Objectwidth.
    SUBTRACT Objectwidth FROM Indentlevel.
    *>MOVE 81 TO Rowposbase.
    *>MOVE 3 TO Depthval.
    SUBTRACT 2 FROM Indentlevel.
    ADD 1 TO Depthval.
    PERFORM Roofprog Depthval TIMES.
    SUBTRACT 1 FROM Rowposbase.
    ADD 2 TO Indentlevel.
    PERFORM Linedrawprog Objectwidth TIMES.
    MOVE 1 TO Indentlevel.
    *>Correctionvalue
    ADD Correctionval TO Indentlevel.
    ADD 1 TO Rowposbase.
    PERFORM Wallprog Objectheigth TIMES.
    SUBTRACT 1 FROM Rowposbase.
    ADD 1 TO Indentlevel.
    PERFORM Linedrawprog Objectwidth TIMES.
    ADD 1 TO Indentlevel.
    PERFORM Sidewallproghorizontal Depthval TIMES.
    SUBTRACT 1 FROM Indentlevel.
    PERFORM Sidewallprogvertical Objectheigth TIMES.
    ADD Depthval TO Rowposbase.
    SUBTRACT Objectwidth FROM Indentlevel.
    SUBTRACT Depthval FROM Indentlevel.
    ADD 1 TO Rowposbase.
    MOVE 1 TO Rowposres.
    *>MOVE Indentlevel TO Animatetablerow(95)(1:4).
    *>MOVE 2 TO Indentlevel.
    PERFORM Objectposprog Levelcount TIMES.
    ADD Objectwidth TO Correctionval.
    ADD 2 TO Correctionval.
    ADD Objectwidth TO Houserowwidth.
    IF Doorpos GREATER THAN 0 THEN PERFORM Doorposprog.
    PERFORM Indentlevelcheckprog.
    *>IF Indentlevel > 100 THEN MOVE "X" TO Saveindicator.
    IF Saveindicator = "x" THEN MOVE "x" TO Saveindicator2.
    ADD 1 TO Subcount.
    Rowposbaseminmove.
    *>==========================================
    *>Program for moving Rowposbasemin.
    *>==========================================

    SUBTRACT Rowposbasemin FROM Bottomrow GIVING Totalheigthres.
    ADD 1 TO Totalheigthres.
    *>MOVE Totalheigthres TO Rowposbaseminrows(Minpos).
    *>ADD 1 TO Minpos.

    Rowposbasecalc.
    *>==========================================
    *>Program for calculating Rowposbase.
    *>==========================================

    MOVE 95 TO Bottomrow.
    MOVE 80 TO Totalheigth.
    ADD Objectheigth TO Totalheigth.
    ADD Depthval TO Totalheigth.
    ADD 1 TO Totalheigth.
    SUBTRACT Totalheigth FROM Bottomrow GIVING Rowposbaseaddition.
    *>MOVE Totalheigth TO Animatetablerow(95)(1:4).
    PERFORM Rowposbaseminmove.

    Doorposprog.
    ADD 1 TO Indentlevel.
    MULTIPLY 7 BY Doorpos.
    SUBTRACT 7 FROM Doorpos.
    ADD Doorpos TO Indentlevel.
    MOVE "|" TO Animatetablerow(Rowposbase)(Indentlevel:1).
    SUBTRACT 1 FROM Rowposbase.
    ADD 1 TO Indentlevel.
    MOVE "  ." TO Animatetablerow(Rowposbase)(Indentlevel:3).
    ADD 1 TO Rowposbase.
    ADD 3 TO Indentlevel.
    MOVE "|" TO Animatetablerow(Rowposbase)(Indentlevel:1).
    SUBTRACT 5 FROM Indentlevel.
    SUBTRACT Doorpos FROM indentlevel.
    *>MOVE Rowposbase TO Animatetablerow(94)(1:4).
    *>MOVE Indentlevel TO Animatetablerow(95)(1:4).

    Indentlevelcheckprog.
    IF Indentlevel GREATER THAN 80 THEN MOVE "x" TO Saveindicator.

    Sidewallproghorizontal.
    MOVE "/" TO Animatetablerow(Rowposbase)(Indentlevel:1).
    SUBTRACT 1 FROM Rowposbase.
    ADD 1 TO Indentlevel.

    Sidewallprogvertical.
    MOVE "|" TO Animatetablerow(Rowposbase)(Indentlevel:1).
    SUBTRACT 1 FROM Rowposbase.

    Objectposprog.
    PERFORM Objectposlineprog Windowcount TIMES.
    ADD 3 TO Rowposbase.
    SUBTRACT Objectwidth FROM Indentlevel.
    ADD 2 TO Indentlevel.

    Objectposlineprog.
    PERFORM Objectpossubprog 3 TIMES.
    ADD 7 TO Indentlevel.
    SUBTRACT 3 FROM Rowposres.
    SUBTRACT 3 FROM Rowposbase.

    Objectpossubprog.
    IF Rowposres = 1 THEN MOVE " _____ " TO Animatetablerow(Rowposbase)(Indentlevel:7).
    IF Rowposres = 2 THEN MOVE " |   | " TO Animatetablerow(Rowposbase)(Indentlevel:7).
    IF Rowposres = 3 THEN MOVE " |___| " TO Animatetablerow(Rowposbase)(Indentlevel:7).
    ADD 1 TO Rowposres.
    ADD 1 TO Rowposbase.

    Wallprog.
    MOVE "|" TO Animatetablerow(Rowposbase)(Indentlevel:1).
    ADD Objectwidth TO Indentlevel.
    ADD 1 TO Indentlevel.
    MOVE "|" TO Animatetablerow(Rowposbase)(Indentlevel:1).
    SUBTRACT Objectwidth FROM Indentlevel.
    SUBTRACT 1 FROM Indentlevel.
    ADD 1 TO Rowposbase.

    Linedrawprog.
    MOVE "_" TO Animatetablerow(Rowposbase)(Indentlevel:1).
    ADD 1 TO Indentlevel.

    Roofprog.
    MOVE "/" TO Animatetablerow(Rowposbase)(Indentlevel:1).
    ADD 1 TO Indentlevel.
    MOVE SPACES TO Animatetablerow(Rowposbase)(Indentlevel:Objectwidth).
    ADD Objectwidth TO Indentlevel.
    MOVE "/" TO Animatetablerow(Rowposbase)(Indentlevel:1).
    SUBTRACT 2 FROM Indentlevel.
    SUBTRACT Objectwidth FROM Indentlevel.
    ADD 1 TO Rowposbase.

    Sidemoveinit.
    DISPLAY Houseanimatescreen.
    ACCEPT Houseanimatescreen.

    Sidemovestart.
    PERFORM Sidemove.
    *>ADD 1 TO Scenecorrection.
    SUBTRACT 1 FROM Scenecorrection.

    Sidemove.
    *>MOVE 133 TO Scenewidth.
    MOVE 1 TO Insertpos.
    MOVE Rowwidthtable(1) TO Scenewidth.
    *>Breite wird vorher noch nicht richtig berechnet.
    ADD 4 TO Scenewidth.
    MULTIPLY Scenewidth BY 2 GIVING Totalscenewidth.
    *>SUBTRACT 5 FROM Totalscenewidth.
    *>MOVE Scenewidth TO Newanimatetablerow(1)(1:4).
    MOVE 1 TO Scenestartpos.
    MOVE 1 TO Scenecountpos.
    *>Wird so oft verschoben wie das Objekt breit ist
    *>PERFORM Sidemoverowprog Scenewidth TIMES.
    PERFORM Sidemoverowprog 1 TIMES.

    Sidemoverowprog.
    *>Programm welches das positionieren der relvanten Zeilen steuert.
    MOVE 1 TO Insertpos.
    ADD Scenecorrection TO Insertpos.
    MOVE Rowwidthtable(1) TO Scenewidth.
    ADD 5 TO Scenewidth.
    SUBTRACT Scenecountpos FROM Scenewidth.
    PERFORM Sidemoverowsubprog.
    ADD Scenewidth TO Insertpos.
    MOVE 1 TO Scenestartpos.
    PERFORM Sidemoverowsubprog.
    MOVE Rowwidthtable(1) TO Scenewidth.
    ADD 10 TO Scenewidth.
    ADD Scenewidth TO Insertpos GIVING Totalscenewidth.
    PERFORM Sidemovespaceprog.
    *>SUBTRACT 1 FROM Totalscenewidth.
    *>SUBTRACT 1 FROM Scenewidth.
    ADD 1 TO Scenecountpos.
    MOVE Scenecountpos TO Scenestartpos.
    DISPLAY Houseanimatescreen.
    ACCEPT OMITTED WITH TIMEOUT 40.

    Sidemovespaceprog.
    MOVE 40 TO Scenebaserow.
    MOVE 1 TO Sceneoutputrow.
    MOVE Rowposbaseminrows(1) TO Objectheigth.
    PERFORM Sidemovespacesubprog3 Objectheigth TIMES.

    Sidemovespacesubprog3.
    *>ADD 1 TO Totalscenewidth.
    MOVE SPACES TO Newanimatetablerow(Sceneoutputrow)(Scenecorrection + Totalscenewidth + 1:135 - Scenecorrection - Totalscenewidth).
    ADD 1 TO Sceneoutputrow.

    Sidemovespacesubprog2.
    *>ADD 1 TO Totalscenewidth.
    MOVE SPACES TO Newanimatetablerow(Sceneoutputrow)(1:Scenecorrection).
    ADD 1 TO Sceneoutputrow.

    Sidemovespacesubprog.

    *>ADD 1 TO Totalscenewidth.
    MOVE SPACES TO Newanimatetablerow(Sceneoutputrow)(Totalscenewidth + 1:135 - Totalscenewidth).
    ADD 1 TO Sceneoutputrow.

    Sidemoverowsubprog.
    MOVE 40 TO Scenebaserow.
    MOVE 1 TO Sceneoutputrow.
    MOVE Rowposbaseminrows(1) TO Objectheigth.
    *>So viele Zeilen wie das Objekt hoch ist
    PERFORM Sidemovesubprog Objectheigth TIMES.

    Newtabledisplayprog.
    *>==========================================
    *>Program for moving Result to Newanimatetablerow.
    *>==========================================
    MOVE Animatetablerow(Rowposbasemin) TO Newanimatetablerow(Newrow).
    ADD 1 TO Rowposbasemin.
    ADD 1 TO Newrow.

    Sidemovesubprog.
    *>Change the content of the Row
    MOVE Newanimatetablerow(Scenebaserow)(Scenestartpos:Scenewidth) TO Newanimatetablerow(Sceneoutputrow)(Insertpos:Scenewidth).
    *>MOVE Scenewidth TO Newanimatetablerow(1)(1:4).
    *>MOVE SPACES TO Newanimatetablerow(Sceneoutputrow)(Scenewidth + 1:1).
    ADD 1 TO Scenebaserow.
    ADD 1 TO Sceneoutputrow.

    *>==========================================
    *>Programs for the previous animation.
    *>==========================================
    Railwaylayoutprog.
    MOVE Animatetablerow(3)(2:119) TO Newanimatetablerow(3)(1:119).
    MOVE Animatetablerow(4)(2:119) TO Newanimatetablerow(4)(1:119).
    MOVE Animatetablerow(5)(2:119) TO Newanimatetablerow(5)(1:119).
    MOVE Animatetablerow(6)(2:119) TO Newanimatetablerow(6)(1:119).
    MOVE Animatetablerow(7)(2:119) TO Newanimatetablerow(7)(1:119).
    MOVE Animatetablerow(8)(2:119) TO Newanimatetablerow(8)(1:119).
    MOVE Animatetablerow(9)(2:119) TO Newanimatetablerow(9)(1:119).
    MOVE Animatetablerow(10)(2:119) TO Newanimatetablerow(10)(1:119).
    MOVE Animatetablerow(11)(2:119) TO Newanimatetablerow(11)(1:119).
    MOVE Animatetablerow(12)(2:119) TO Newanimatetablerow(12)(1:119).
    MOVE Animatetablerow(13)(2:119) TO Newanimatetablerow(13)(1:119).
    MOVE Animatetablerow(14)(2:119) TO Newanimatetablerow(14)(1:119).
    MOVE Animatetablerow(15)(2:119) TO Newanimatetablerow(15)(1:119).
    MOVE Animatetablerow(16)(2:119) TO Newanimatetablerow(16)(1:119).
    MOVE Animatetablerow(17)(2:119) TO Newanimatetablerow(17)(1:119).
    MOVE Animatetablerow(18)(2:119) TO Newanimatetablerow(18)(1:119).
    MOVE Animatetablerow(19)(2:119) TO Newanimatetablerow(19)(1:119).
    MOVE Animatetablerow(20)(2:119) TO Newanimatetablerow(20)(1:119).
    MOVE Animatetablerow(21)(2:119) TO Newanimatetablerow(21)(1:119).
    MOVE Animatetablerow(22)(2:119) TO Newanimatetablerow(22)(1:119).
    MOVE Animatetablerow(23)(2:119) TO Newanimatetablerow(23)(1:119).
    MOVE Animatetablerow(24)(2:119) TO Newanimatetablerow(24)(1:119).
    MOVE Animatetablerow(25)(2:119) TO Newanimatetablerow(25)(1:119).
    MOVE Animatetablerow(26)(2:119) TO Newanimatetablerow(26)(1:119).
    MOVE Animatetablerow(27)(2:119) TO Newanimatetablerow(27)(1:119).
    MOVE Animatetablerow(28)(2:119) TO Newanimatetablerow(28)(1:119).
    MOVE Animatetablerow(29)(2:119) TO Newanimatetablerow(29)(1:119).
    MOVE Animatetablerow(30)(2:119) TO Newanimatetablerow(30)(1:119).
    MOVE Animatetablerow(31)(Countnum:1)  TO Newanimatetablerow(3)(120:1).
    MOVE Animatetablerow(32)(Countnum:1)  TO Newanimatetablerow(4)(120:1).
    MOVE Animatetablerow(33)(Countnum:1)  TO Newanimatetablerow(5)(120:1).
    MOVE Animatetablerow(34)(Countnum:1)  TO Newanimatetablerow(6)(120:1).
    MOVE Animatetablerow(35)(Countnum:1)  TO Newanimatetablerow(7)(120:1).
    MOVE Animatetablerow(36)(Countnum:1)  TO Newanimatetablerow(8)(120:1).
    MOVE Animatetablerow(37)(Countnum:1)  TO Newanimatetablerow(9)(120:1).
    MOVE Animatetablerow(38)(Countnum:1)  TO Newanimatetablerow(10)(120:1).
    MOVE Animatetablerow(39)(Countnum:1)  TO Newanimatetablerow(11)(120:1).
    MOVE Animatetablerow(40)(Countnum:1)  TO Newanimatetablerow(12)(120:1).
    MOVE Animatetablerow(41)(Countnum:1)  TO Newanimatetablerow(13)(120:1).
    MOVE Animatetablerow(42)(Countnum:1)  TO Newanimatetablerow(14)(120:1).
    *>IF Num = 3 THEN MOVE "/"  TO Newanimatetablerow(14)(120:1) ELSE MOVE "_"  TO Newanimatetablerow(14)(120:1).
    MOVE Animatetablerow(43)(Countnum:1)  TO Newanimatetablerow(15)(120:1).
    MOVE " "  TO Newanimatetablerow(16)(120:1).
    MOVE "_"  TO Newanimatetablerow(17)(120:1).
    IF Num = 3 THEN MOVE "/"  TO Newanimatetablerow(18)(120:1) ELSE MOVE "_"  TO Newanimatetablerow(18)(120:1).
    MOVE " "  TO Newanimatetablerow(19)(120:1).
    MOVE "_"  TO Newanimatetablerow(20)(120:1).
    IF Num = 1 THEN MOVE "/"  TO Newanimatetablerow(21)(120:1) ELSE MOVE "_"  TO Newanimatetablerow(21)(120:1).
    MOVE " "  TO Newanimatetablerow(22)(120:1).
    MOVE " "  TO Newanimatetablerow(23)(120:1).
    MOVE " "  TO Newanimatetablerow(24)(120:1).
    MOVE " "  TO Newanimatetablerow(25)(120:1).
    MOVE " "  TO Newanimatetablerow(26)(120:1).
    MOVE " "  TO Newanimatetablerow(27)(120:1).
    MOVE " "  TO Newanimatetablerow(28)(120:1).
    MOVE " "  TO Newanimatetablerow(29)(120:1).
    MOVE " "  TO Newanimatetablerow(30)(120:1).
    MOVE Newanimatetablerow(3)  TO Animatetablerow(3).
    MOVE Newanimatetablerow(4)  TO Animatetablerow(4).
    MOVE Newanimatetablerow(5)  TO Animatetablerow(5).
    MOVE Newanimatetablerow(6)  TO Animatetablerow(6).
    MOVE Newanimatetablerow(7)  TO Animatetablerow(7).
    MOVE Newanimatetablerow(8)  TO Animatetablerow(8).
    MOVE Newanimatetablerow(9)  TO Animatetablerow(9).
    MOVE Newanimatetablerow(10)  TO Animatetablerow(10).
    MOVE Newanimatetablerow(11)  TO Animatetablerow(11).
    MOVE Newanimatetablerow(12)  TO Animatetablerow(12).
    MOVE Newanimatetablerow(13)  TO Animatetablerow(13).
    MOVE Newanimatetablerow(14)  TO Animatetablerow(14).
    MOVE Newanimatetablerow(15)  TO Animatetablerow(15).
    MOVE Newanimatetablerow(16)  TO Animatetablerow(16).
    MOVE Newanimatetablerow(17)  TO Animatetablerow(17).
    MOVE Newanimatetablerow(18)  TO Animatetablerow(18).
    MOVE Newanimatetablerow(19)  TO Animatetablerow(19).
    MOVE Newanimatetablerow(20)  TO Animatetablerow(20).
    MOVE Newanimatetablerow(21)  TO Animatetablerow(21).
    MOVE Newanimatetablerow(22)  TO Animatetablerow(22).
    MOVE Newanimatetablerow(23)  TO Animatetablerow(23).
    MOVE Newanimatetablerow(24)  TO Animatetablerow(24).
    MOVE Newanimatetablerow(25)  TO Animatetablerow(25).
    MOVE Newanimatetablerow(26)  TO Animatetablerow(26).
    MOVE Newanimatetablerow(27)  TO Animatetablerow(27).
    MOVE Newanimatetablerow(28)  TO Animatetablerow(28).
    MOVE Newanimatetablerow(29)  TO Animatetablerow(29).
    MOVE Newanimatetablerow(30)  TO Animatetablerow(30).
    IF Num = 3 THEN MOVE 1 TO Num ELSE ADD 1 TO Num.
    ADD 1 TO Countnum.
    DISPLAY Animatescreen.
    ACCEPT OMITTED WITH TIMEOUT 30.

    Ferrymove.
    MOVE "~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~" TO Baseline1.
    MOVE "A" TO Str2.
    MOVE "_____" TO Baseline3(1:5).
    MOVE 2 TO Num.
    MOVE "________________" TO Baseline3(Num - 1:16).
    MOVE "\              /" TO Baseline2(Num - 1:16).
    MOVE "\____________/" TO Baseline1(Num:14).
    DISPLAY DATA-ENTRY-SCREEN.
    ACCEPT OMITTED WITH TIMEOUT 30.
    PERFORM 75 TIMES
    MOVE " " TO Baseline2
    MOVE " " TO Baseline3
    IF Num < 120  THEN MOVE "\              /" TO Baseline2(Num - 1:16)
    ELSE MOVE "X" TO Str2 END-IF
    IF Num < 120  THEN MOVE "\____________/" TO Baseline1(Num:14)
    ELSE MOVE "X" TO Str2 END-IF
    IF Num < 120  THEN MOVE "________________" TO Baseline3(Num - 1:16)
    ELSE MOVE "X" TO Str2 END-IF
    MOVE "~" TO Baseline1(Num - 1:1)
    DISPLAY DATA-ENTRY-SCREEN
    ACCEPT OMITTED WITH TIMEOUT 30
    ADD 1 TO Num
    END-PERFORM.

    Textmove.
    *>This is the first Text to be displayed when the Animation starts.
    MOVE "||| | | |||   ||  ||| | |   | | | ||| | |   ||| ||| ||  ||  | |" TO Rolltext1.
    MOVE " |  | | |     | | | | | |   | | | | | | |   |   |   | | | | | |" TO Rolltext2.
    MOVE " |  ||| |||   | | ||| | |   | | | ||| |||   ||| ||| | | | | |||" TO Rolltext3.
    MOVE " |  | | |     ||  | | | |   | | | | |  |    |   |   ||  ||   | " TO Rolltext4.
    MOVE " |  | | |||   | | | | | |||  |||  | |  |    |   ||| | | | |  | " TO Rolltext5.
    Textmove2.
    MOVE "||| | | |||   ||  ||| | |   | | | ||| | |   ||| ||| ||  ||  | |" TO Rolltext1.
    MOVE " |  | | |     | | | | | |   | | | | | | |   |   |   | | | | | |" TO Rolltext2.
    MOVE " |  ||| |||   | | ||| | |   | | | ||| |||   ||| ||| | | | | |||" TO Rolltext3.
    MOVE " |  | | |     ||  | | | |   | | | | |  |    |   |   ||  ||   | " TO Rolltext4.
    MOVE " |  | | |||   | | | | | |||  |||  | |  |    |   ||| | | | |  | " TO Rolltext5.
    Displayprog.
    DISPLAY DATA-ENTRY-SCREEN.
    ACCEPT OMITTED.
    Ferryanimation.
    MOVE "xxxx~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~" TO Baseline1.
    MOVE "A" TO Str2.
    MOVE 2 TO Num.
    DISPLAY DATA-ENTRY-SCREEN.
    ACCEPT OMITTED.
    PERFORM 95 TIMES
    IF Num > 100 THEN MOVE " " TO Baseline2
    MOVE " " TO Baseline3
    IF Num < 120  THEN MOVE "\              /" TO Baseline2(Num - 1:16)
    ELSE MOVE "X" TO Str2 END-IF
    IF Num < 120  THEN MOVE "\____________/" TO Baseline1(Num:14)
    ELSE MOVE "X" TO Str2 END-IF
    IF Num < 120  THEN MOVE "________________" TO Baseline3(Num - 1:16)
    ELSE MOVE "X" TO Str2 END-IF
    MOVE "~" TO Baseline1(Num - 1:1)
    DISPLAY DATA-ENTRY-SCREEN
    ACCEPT OMITTED WITH TIMEOUT 30
    ADD 1 TO Num
    END-PERFORM.
    END PROGRAM YOUR-PROGRAM-NAME.
