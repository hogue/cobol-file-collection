    IDENTIFICATION DIVISION.
    PROGRAM-ID. YOUR-PROGRAM-NAME.
    ENVIRONMENT DIVISION.
    INPUT-OUTPUT SECTION.
    FILE-CONTROL.
    SELECT Inputfile ASSIGN TO "D:\Cobolprogramme\Charparser\wikifunctionswiki-latest-pages-meta-current_20241215.xml"
    *>SELECT Inputfile ASSIGN TO "D:\Cobolprogramme\Charparser\Coboltemplate20240809.txt"
    ORGANIZATION IS SEQUENTIAL.
    SELECT Outputfile ASSIGN TO "D:\Cobolprogramme\Charparser\Output.txt"
    ORGANIZATION IS LINE SEQUENTIAL.
    SELECT Resultfile ASSIGN TO "D:\Cobolprogramme\Charparser\Result.txt"
    ORGANIZATION IS LINE SEQUENTIAL.
    SELECT Langfile ASSIGN TO "D:\Cobolprogramme\Charparser\Langlist.txt"
    ORGANIZATION IS LINE SEQUENTIAL.
    SELECT Langstatfile ASSIGN TO "D:\Cobolprogramme\Charparser\Langstatfile.txt"
    ORGANIZATION IS LINE SEQUENTIAL.
    DATA DIVISION.
    FILE SECTION.
    FD  Inputfile.
    01  Record-Name3.
    02 Field1                  PIC X(5000).
    FD  Outputfile.
    01  Record-Name4.
    02 Field2                  PIC X(5000).
    FD  Resultfile.
    01  Record-Name5.
    02 Field3                  PIC X(5000).
    FD  Langfile.
    01  Record-Name6.
    02 Field4                PIC X(20).
    FD  Langstatfile.
    01  Record-Name7.
    02 Field5                PIC X(200).
    *>02 Keyrow PIC 9(9).
    WORKING-STORAGE SECTION.
    01 Posnum PIC  9(4).
    01 Quotcount PIC  9(4).
    01 Quotcountres PIC  9(4).
    01 Subposnum PIC  9(4).
    01 Groupposnum PIC  9(4).
    01 Pagecount PIC 9(8).
    01 Textvalue PIC X(100).
    01 Singlechar PIC X(1).
    01 Singlecharalt PIC X(1).
    01 Charlist PIC X(5).
    01 Chargroup PIC X(5000).
    01 Textbase PIC X(500).
    01 Totalsum PIC 9(8).
    01 Readcount PIC 9(10).
    01 Funccount PIC 9(12).
    01 Prefunccount PIC 9(10).
    01 Fileendnum PIC 9(1).
    01 Tagstatus PIC 9(1).
    01 Tagoutput PIC 9(1).
    01 Escapenum PIC 9(1).
    01 Pageindicator PIC 9(1).
    01 Namespaceindicator PIC 9(1).
    01 Prestring PIC X(6).
    01 Numstring PIC X(8).
    01 Numstringres PIC X(8).
    01 Furtherstring PIC X(6).
    01 Currpos PIC 9(3).
    01 Namespacebase PIC X(100).
    01 Tagbase PIC X(1000).
    01 Relspace PIC 9(1).
    01 Relspacecheck PIC 9(1).
    01 Commentindicator PIC 9(1).
    01 Titlerel PIC 9(1).
    01 Langrel PIC 9(1).
    01 Categoryrel PIC 9(1).
    01 Fileend PIC 9(1).
    01 Zahl PIC 9(2).
    01 Quotremainder PIC 9(2).
    01 Option PIC 9(2).
    01 Rowcheckvar PIC 9(1).
    01 Statnum PIC 9(2).
    01 Inspectnum PIC 9(2).
    01 Codeextract PIC 9(1).
    01 Keyid PIC X(10000).
    01 Codepart PIC X(10000).
    01 Resultoutput PIC X(5000).
    01 Langstattable PIC 9(8) OCCURS 10000 TIMES.
    01 Langstattable2 PIC 9(8) OCCURS 10000 TIMES.
    01 Langstattable3 PIC 9(8) OCCURS 10000 TIMES.
    01 Langstattable4 PIC 9(8) OCCURS 10000 TIMES.
    01 Langstattable5 PIC 9(8) OCCURS 10000 TIMES.
    01 Langcodetable PIC X(20) OCCURS 10000 TIMES.
    PROCEDURE DIVISION.
    MAIN-PROCEDURE.
    PERFORM Subprog.
    *>MOVE "00000000" TO Numstring.
    *>PERFORM Removeleadzeroes.
    *>DISPLAY "Numstringres: " Numstringres.
    STOP RUN.
    
    Subprog.
    PERFORM Basefileprog.
    PERFORM Textoutputprog.
    MOVE 1 TO Groupposnum.
    OPEN INPUT Langfile.
    PERFORM Langstattableinit 9999 TIMES.
    CLOSE Langfile.
    MOVE 0 TO Groupposnum.
    PERFORM Statcountprog.
    CLOSE Resultfile.
    PERFORM Langstatinit.
    DISPLAY "Zerovalues: " Langstattable(1).
    DISPLAY "Resultcount: " Langstattable2(1002).
    DISPLAY "Ready.".
    
    Removeleadzeroes.
    MOVE 0 TO Statnum.
    MOVE 8 TO Inspectnum.
    MOVE "_" TO Prestring.
    MOVE SPACES TO Numstringres.
    *>MOVE "01001254" TO Numstring.
    PERFORM Removeleadzeroessub UNTIL Prestring = "|".
    SUBTRACT Statnum FROM Inspectnum.
    ADD 1 TO Inspectnum.
    MOVE Numstring(Statnum:Inspectnum) TO Numstringres.
    Removeleadzeroessub.
    ADD 1 TO Statnum.
    IF Numstring(Statnum:1) EQUAL TO "0" AND Prestring NOT EQUAL TO "|" THEN MOVE "_" TO Prestring.
    IF Numstring(Statnum:1) NOT EQUAL TO "0" THEN MOVE "|" TO Prestring.
    IF Statnum = 8 THEN MOVE "|" TO Prestring.
    
    Langstattableinit.
    MOVE 0 TO Langstattable(Groupposnum).
    MOVE 0 TO Langstattable2(Groupposnum).
    MOVE 0 TO Langstattable3(Groupposnum).
    MOVE 0 TO Langstattable4(Groupposnum).
    MOVE 0 TO Langstattable5(Groupposnum).
    READ Langfile INTO Langcodetable(Groupposnum).
    ADD 1 TO Groupposnum.
    
    Statcountprog.
    OPEN INPUT Resultfile.
    READ Resultfile.
    MOVE 0 TO Fileendnum.
    PERFORM Statcountsubprog UNTIL Fileendnum = 1.
    
    Statcountsubprog.
    READ Resultfile INTO Namespacebase AT END MOVE 1 TO Fileendnum.
    MOVE Namespacebase(11:4) TO Groupposnum.
    MOVE Namespacebase(19:4) TO Keyid.
    *>IF Groupposnum = 0 THEN DISPLAY Namespacebase.
    IF Groupposnum = 0 THEN MOVE 1 TO Groupposnum.
    MOVE 1 TO Langstattable(Groupposnum).
    IF Keyid = "Z2K2" THEN ADD 1 TO Langstattable2(Groupposnum).
    IF Keyid = "Z2K3" THEN ADD 1 TO Langstattable3(Groupposnum).
    IF Keyid = "Z2K4" THEN ADD 1 TO Langstattable4(Groupposnum).
    IF Keyid = "Z2K5" THEN ADD 1 TO Langstattable5(Groupposnum).
    MOVE 0 TO Groupposnum.
    
    Langstatinit.
    OPEN OUTPUT Langstatfile.
    WRITE Record-Name7 FROM "{| class=""wikitable sortable""".
    WRITE Record-Name7 FROM "|-".
    WRITE Record-Name7 FROM "!Zobject".
    WRITE Record-Name7 FROM "!Languagecode".
    WRITE Record-Name7 FROM "!Z2K2 - value".
    WRITE Record-Name7 FROM "!Z2K3 - Label".
    WRITE Record-Name7 FROM "!Z2K4 - Alias".
    WRITE Record-Name7 FROM "!Z2K5 - short description".
    WRITE Record-Name7 FROM "!Sum".
    MOVE 1000 TO Groupposnum.
    PERFORM Langstatoutputprog 2000 TIMES.
    PERFORM Langstatclose.
    
    Langstatoutputprog.
    IF Langstattable(Groupposnum) = 1 THEN MOVE 1 TO Rowcheckvar.
    IF Rowcheckvar = 1 THEN PERFORM Langstatwrite.
    MOVE 0 TO Rowcheckvar.
    ADD 1 TO Groupposnum.
    Langstatclose.
    WRITE Record-Name7 FROM "|}".
    CLOSE Langstatfile.
    
    Langstatwrite.
    MOVE 0 TO Totalsum.
    WRITE Record-Name7 FROM "|-".
    MOVE "|Z" TO Resultoutput(1:2).
    MOVE Groupposnum TO Resultoutput(3:8).
    WRITE Record-Name7 FROM Resultoutput(1:10).
    MOVE SPACES TO Resultoutput.
    MOVE "|" TO Resultoutput(1:1).
    MOVE Langcodetable(Groupposnum) TO Resultoutput(2:20).
    WRITE Record-Name7 FROM Resultoutput(1:21).
    MOVE SPACES TO Resultoutput.
    MOVE "|" TO Resultoutput(1:1).
    MOVE Langstattable2(Groupposnum) TO Numstring.
    PERFORM Removeleadzeroes.
    MOVE Numstringres TO Resultoutput(2:8).
    WRITE Record-Name7 FROM Resultoutput(1:9).
    MOVE Langstattable3(Groupposnum) TO Numstring.
    PERFORM Removeleadzeroes.
    MOVE Numstringres TO Resultoutput(2:8).
    WRITE Record-Name7 FROM Resultoutput(1:9).
    MOVE Langstattable4(Groupposnum) TO Numstring.
    PERFORM Removeleadzeroes.
    MOVE Numstringres TO Resultoutput(2:8).
    WRITE Record-Name7 FROM Resultoutput(1:9).
    MOVE Langstattable5(Groupposnum) TO Numstring.
    PERFORM Removeleadzeroes.
    MOVE Numstringres TO Resultoutput(2:8).
    WRITE Record-Name7 FROM Resultoutput(1:9).
    ADD Langstattable2(Groupposnum) TO Totalsum.
    ADD Langstattable3(Groupposnum) TO Totalsum.
    ADD Langstattable4(Groupposnum) TO Totalsum.
    ADD Langstattable5(Groupposnum) TO Totalsum.
    MOVE Totalsum TO Numstring.
    PERFORM Removeleadzeroes.
    MOVE Numstringres TO Resultoutput(2:8).
    WRITE Record-Name7 FROM Resultoutput(1:9).
    
    Textoutputprog.
    *>Program to output all Labels, Descriptions, Alias per Language and ZID
    OPEN INPUT Outputfile.
    OPEN OUTPUT Resultfile.
    WRITE Record-Name5 FROM "ZID|Language|Category|Content".
    PERFORM Categorycheck UNTIL Fileend = 1.
    *>DISPLAY "Codepart " Resultoutput.
    CLOSE Outputfile.
    CLOSE Resultfile.

    
    Categorycheck.
    READ Outputfile INTO Codepart AT END MOVE 1 TO Fileend.
    IF Titlerel = 1 THEN MOVE Codepart(1:8) TO Resultoutput(1:8).
    IF Titlerel = 1 THEN MOVE "|" TO Resultoutput(9:1).
    IF Titlerel = 1 THEN MOVE 0 TO Titlerel.
    IF Langrel = 1 THEN MOVE Codepart(1:8) TO Resultoutput(10:8).
    IF Langrel = 1 THEN MOVE "|" TO Resultoutput(18:1).
    IF Codepart = "Z2K2" THEN MOVE "Z2K2|" TO Resultoutput(19:5).
    IF Codepart = "Z2K3" THEN MOVE "Z2K3|" TO Resultoutput(19:5).
    IF Codepart = "Z2K4" THEN MOVE "Z2K4|" TO Resultoutput(19:5).
    IF Codepart = "Z2K5" THEN MOVE "Z2K5|" TO Resultoutput(19:5).
    IF Langrel = 1 THEN MOVE 0 TO Langrel.
    IF Categoryrel = 1 THEN MOVE Codepart(1:1000) TO Resultoutput(25:1000).
    IF Categoryrel = 1 THEN WRITE Record-Name5 FROM Resultoutput.
    IF Categoryrel = 1 THEN MOVE 0 TO Categoryrel.
    IF Codepart = "Title:" THEN MOVE 1 TO Titlerel.
    IF Codepart = "Z11K1" THEN MOVE 1 TO Langrel.
    IF Codepart = "Z11K2" THEN MOVE 1 TO Categoryrel.
    Outputread.
    READ Outputfile.
    Basefileprog.
    *>program to generate the Basefile Output.txt
    OPEN OUTPUT Outputfile.
    MOVE 2 TO Option.
    MOVE 0 TO Posnum.
    MOVE 1 TO Subposnum.
    MOVE 1 TO Readcount.
    MOVE 1 TO Currpos.
    MOVE 0 TO Pagecount.
    MOVE 0 TO Funccount.
    MOVE 0 TO Statnum.
    MOVE 1 TO Inspectnum.
    MOVE 1 TO Subposnum.
    OPEN INPUT Inputfile.
    PERFORM Codeextractprog UNTIL Fileendnum = 1.
    *>IF Option = 2 THEN PERFORM Codeextractprog.
    CLOSE Inputfile.
    DISPLAY "Quot: " Quotcount.
    MOVE 2 TO Zahl.
    CLOSE Outputfile.
    Evaluateprog.
    EVALUATE Zahl
    WHEN 1 DISPLAY "Low"
    WHEN 2 DISPLAY "Middle"
    WHEN 3 DISPLAY "High"
    END-EVALUATE.
    Namespacecheck.
    MOVE Singlechar TO Namespacebase(Currpos:1).
    ADD 1 TO Currpos.
    Namespaceresult.
    MOVE " " TO Namespacebase(Currpos - 5:Currpos).
    IF Namespacebase = "0" THEN MOVE 1 TO Relspace ELSE MOVE 0 TO Relspace.
    MOVE 1 TO Currpos.
    MOVE " " TO Namespacebase.
    Resultprog.
    MOVE 1 TO Fileendnum.
    Readprog.
    MOVE 1 TO Posnum.
    *>READ Inputfile INTO Textvalue NOT AT END ADD 1 TO Readcount.
    READ Inputfile INTO Textvalue AT END PERFORM Resultprog.
    DISPLAY "X: " Textvalue.
    PERFORM Textparser.
    Textparser.
    MOVE " " TO Prestring.
    MOVE " " TO Furtherstring.
    PERFORM Textparsersub 95 TIMES.
    MOVE Textvalue(Posnum:5) TO Prestring(2:5).
    Textparsersub.
    IF Posnum < 7 THEN MOVE Prestring(2:5) TO Furtherstring(1:5).
    IF Posnum < 7 THEN MOVE Textvalue(Posnum:1) TO Furtherstring.
    IF Posnum < 7 THEN MOVE Furtherstring TO Prestring.
    MOVE Textvalue(Posnum:6) TO Textbase.
    IF Textbase = "<page>" OR Furtherstring ="<page>" THEN ADD 1 TO Pagecount.
    ADD 1 TO Posnum.
    Codeextractprog.
    *>READ Inputfile INTO Chargroup.
    MOVE " " TO Chargroup.
    READ Inputfile INTO Chargroup AT END PERFORM Resultprog.
    MOVE 1 TO Groupposnum.
    MOVE 0 TO Inspectnum.
    *>Runs for every Character in the Characterrow read at once
    MOVE 1 TO Inspectnum.
    PERFORM Codeextractsubprog3 5000 TIMES.
    ADD 1 TO Prefunccount.
    *>DISPLAY "Count: " Prefunccount.
    Codeextractsubprog3.
    *>DISPLAY Singlechar.
    MOVE Chargroup(Groupposnum:1) TO Singlechar.
    MOVE Singlechar TO Singlecharalt
    IF Escapenum = 1 THEN MOVE "X" TO Singlechar.
    MOVE 0 TO Escapenum.
    IF Singlechar = "\" THEN MOVE 1 TO Escapenum.
    IF Singlechar = "<" AND Tagoutput = 1 THEN PERFORM Tagresultprog.
    IF Tagoutput = 1 THEN MOVE Singlechar TO Codepart(Subposnum:1).
    IF Tagoutput = 1 THEN ADD 1 TO Subposnum.
    IF Singlechar = """" AND Relspace = 1 THEN ADD 1 TO Quotcount.
    IF Singlechar = """" AND Relspace = 1 THEN DIVIDE Quotcount BY 2 GIVING Quotcountres REMAINDER Quotremainder.
    IF Singlechar = """" AND Quotremainder = 1 THEN MOVE 1 TO Inspectnum.
    IF Singlechar = """" AND Quotremainder = 1 THEN MOVE 1 TO Subposnum.
    IF Singlechar NOT EQUAL TO """" AND Quotremainder = 1 AND Subposnum = 9999 THEN PERFORM Additionalwriteprog.
    IF Singlechar NOT EQUAL TO """" AND Quotremainder = 1 THEN MOVE Singlecharalt TO Codepart(Subposnum:1).
    IF Singlechar NOT EQUAL TO """" AND Quotremainder = 1 THEN ADD 1 TO Subposnum.
    IF Singlechar = """" AND Quotremainder = 0 AND Relspace = 1 AND Commentindicator = 0 THEN PERFORM Keycheckprog.
    IF Singlechar = """" AND Quotremainder = 0 AND Relspace = 1 AND Commentindicator = 0 THEN WRITE Record-Name4 FROM Codepart.
    IF Singlechar = """" AND Quotremainder = 0 AND Commentindicator = 0 THEN MOVE " " TO Codepart.
    IF Singlechar = "<" AND Quotremainder = 0 THEN MOVE 1 TO Tagstatus.
    IF Singlechar = "<" AND Quotremainder = 0 THEN MOVE 1 TO Inspectnum.
    IF Singlechar = "<" AND Quotremainder = 0 THEN MOVE 1 TO Subposnum.
    IF Singlechar = ">" AND Quotremainder = 0 THEN MOVE 0 TO Tagstatus.
    IF Singlechar NOT EQUAL TO "<" AND Tagstatus = 1 THEN MOVE Singlechar TO Codepart(Subposnum:1).
    IF Singlechar NOT EQUAL TO "<" AND Tagstatus = 1 THEN ADD 1 TO Subposnum.
    IF Singlechar = ">" AND Tagstatus = 0 THEN PERFORM Tagcheckprog.
    *>IF Singlechar = ">" AND Tagstatus = 0 THEN WRITE Record-Name4 FROM Codepart.
    IF Singlechar = ">" AND Tagstatus = 0 THEN MOVE " " TO Codepart.
    ADD 1 TO Groupposnum.
    MOVE 0 TO Inspectnum.
    Additionalwriteprog.
    WRITE Record-Name4 FROM Codepart.
    MOVE 1 TO Inspectnum.
    MOVE 1 TO Subposnum.
    MOVE " " TO Codepart.
    Keycheckprog.
    IF Codepart = "Z16K2" THEN WRITE Record-Name4 FROM "Code: ".
    Tagresultprog.
    IF Codepart = "0" AND Relspacecheck = 1 THEN MOVE 1 TO Relspace.
    IF Codepart NOT EQUAL TO "0" AND Relspacecheck = 1 THEN MOVE 0 TO Relspace.
    IF Relspacecheck = 1 THEN WRITE Record-Name4 FROM "Namespace: ".
    IF Titlerel = 1 THEN WRITE Record-Name4 FROM "Title: ".
    *>WRITE Record-Name4 FROM "Title: ".
    WRITE Record-Name4 FROM Codepart.
    MOVE " " TO Codepart.
    MOVE 0 TO Tagoutput.
    MOVE 0 TO Titlerel.
    Tagoutputstartprog.
    MOVE 1 TO Tagoutput.
    MOVE 1 TO Inspectnum.
    MOVE 1 TO Subposnum.
    Tagcheckprog.
    IF Codepart = "title" THEN PERFORM Tagoutputstartprog.
    IF Codepart = "title" THEN MOVE 1 TO Titlerel.
    IF Codepart = "ns" THEN MOVE 1 TO Relspacecheck.
    IF Codepart = "ns" THEN PERFORM Tagoutputstartprog.
    IF Codepart = "comment" THEN MOVE 1 TO Commentindicator.
    IF Codepart = "/comment" THEN MOVE 0 TO Commentindicator.
    END PROGRAM YOUR-PROGRAM-NAME.
