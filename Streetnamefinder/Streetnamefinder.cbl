*>****************************************************************
*> Author:
*> Date:
*> Purpose:
*> Tectonics: cobc
*>****************************************************************
    IDENTIFICATION DIVISION.
    PROGRAM-ID. YOUR-PROGRAM-NAME.
    ENVIRONMENT DIVISION.
    INPUT-OUTPUT SECTION.
    FILE-CONTROL.
    SELECT Inputfile ASSIGN TO "D:\Cobolprogramme\Strassennamenfinder\StrasseninNeu-Ulm2.csv"
    ORGANIZATION IS LINE SEQUENTIAL.
    SELECT Outputfile ASSIGN TO "D:\Cobolprogramme\Strassennamenfinder\Output.txt"
    ORGANIZATION IS LINE SEQUENTIAL.
    DATA DIVISION.
    FILE SECTION.
    FD  Inputfile.
    01 Record-Name3.
    02 Field1 PIC X(2000).
    FD  Outputfile.
    01  Record-Name4.
    02 Field2 PIC X(2000).
    WORKING-STORAGE SECTION.
    01 Cellposaltdiff PICTURE 9(4).
    01 Cellposalt2 PICTURE 9(4).
    01 Countnum PICTURE 9(4).
    01 Totalcount PICTURE 9(4).
    01 Laststreetnum PICTURE 9(4).
    01 Itemid PICTURE X(20).
    01 Inputnum1 PIC 9(4).
    01 Inputnum2 PIC 9(4).
    01 Funccount PIC 9(4).
    01 Streetnamealt PIC X(200).
    01 Streetinput PIC X(2000).
    01 Inspectres PIC X(70).
    01 Extstr PIC X(70).
    01 Streetqid PIC X(200) OCCURS 10000 TIMES.
    01 Streetname PIC X(200) OCCURS 10000 TIMES.
    01 Streetdescription PIC X(200) OCCURS 10000 TIMES.
    SCREEN SECTION.
    01 Streetnamestart BACKGROUND-COLOR 3 FOREGROUND-COLOUR 0.
    05  VALUE "Strassennamen" BLANK SCREEN LINE  1 COL 15.
    05 VALUE "Strassenname: " LINE 3 COLUMN 5.
    05  Streetnameinput1 LINE  4 COLUMN 5 PIC X(70) TO Streetnamealt.
    01 Streetresult BACKGROUND-COLOR 3 FOREGROUND-COLOUR 0.
    05  VALUE "Streetresult" BLANK SCREEN LINE  1 COL 15.
    05 VALUE "Itemid:" LINE  4 COL 5.
    05 LINE 5 COLUMN 5 PIC X(20) FROM Itemid.
    05 LINE 5 COLUMN 45 PIC X(70) FROM Streetnamealt.
    05  Inputoptionval1 LINE  7 COLUMN 25 PIC X(1) TO Inputnum1.
    PROCEDURE DIVISION.
    MAIN-PROCEDURE.        
    OPEN OUTPUT Outputfile.
    OPEN INPUT Inputfile.
    MOVE 1 TO Countnum.
    READ Inputfile.
    PERFORM Streetflatlistprog UNTIL Laststreetnum = 1.
    CLOSE Inputfile.
    CLOSE Outputfile.
    MOVE Countnum TO Totalcount.
    DISPLAY Streetnamestart.
    ACCEPT Streetnamestart.
    *>MOVE "Gotenstraße" TO Streetnamealt.
    MOVE 1 TO Countnum.
    PERFORM Streetsearchprog Totalcount TIMES.
    DISPLAY Streetresult.
    ACCEPT Streetresult.
    STOP RUN.
    Streetflatlistprog.
    READ Inputfile INTO Streetinput AT END MOVE 1 TO Laststreetnum.
    UNSTRING Streetinput DELIMITED BY ";" INTO 
    Streetqid(Countnum)
    Streetname(Countnum)
    Streetdescription(Countnum)
    END-UNSTRING.
    WRITE Record-Name4 FROM Streetqid(Countnum).
    WRITE Record-Name4 FROM Streetname(Countnum).
    WRITE Record-Name4 FROM Streetdescription(Countnum).
    ADD 1 TO Countnum.
    Streetsearchprog.
    IF Streetname(Countnum) = Streetnamealt THEN DISPLAY "Position: " Countnum.
    IF Streetname(Countnum) = Streetnamealt THEN MOVE Streetqid(Countnum)(32:14) TO Itemid.
    *>IF Streetname(Countnum) = Streetnamealt THEN DISPLAY "Item: " Itemid.
    ADD 1 TO Countnum.
    Checkprog.
    IF Streetname(Countnum) = Streetnamealt THEN DISPLAY "Equal" ELSE DISPLAY "Not Equal".
    END PROGRAM YOUR-PROGRAM-NAME.
