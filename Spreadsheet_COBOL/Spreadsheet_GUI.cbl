*>****************************************************************
*> Author: Hogue
*> Date: June-July 2023
*> Purpose: A experimental Spreadsheet using Cobol Screens
*> These are text based user interfaces.
*> This version is not a full Spreadsheet. You can enter a MID-Function
*> or a SUM Function using the German Function names TEIL and SUMME and
*> the Semicolon as a separator for arguments. For some one Range is 
*>Supported.
*>****************************************************************
IDENTIFICATION DIVISION.
PROGRAM-ID. YOUR-PROGRAM-NAME.
ENVIRONMENT DIVISION.
       INPUT-OUTPUT SECTION.
       FILE-CONTROL.
                 SELECT File-Name2 ASSIGN TO "D:\Cobolprogramme\Spreadsheetgui\Inputvalues.txt"
                 ORGANIZATION IS LINE SEQUENTIAL.
                 SELECT File-Name ASSIGN TO "D:\Cobolprogramme\Guiprogramm\Reservedwords_Spreadsheet_DE.txt"
                 ORGANIZATION IS LINE SEQUENTIAL.
                 SELECT Cobolprog ASSIGN TO "D:\Cobolprogramme\Guiprogramm\Spreadsheetprog.txt"
                 ORGANIZATION IS LINE SEQUENTIAL.
                 SELECT Outputprog ASSIGN TO "D:\Cobolprogramme\Guiprogramm\Outputprog_Spreadsheet.txt"
                 ORGANIZATION IS LINE SEQUENTIAL.
                 SELECT Suboutputprog ASSIGN TO "D:\Cobolprogramme\Guiprogramm\Suboutputprog_Spreadsheet.txt"
                 ORGANIZATION IS LINE SEQUENTIAL.
                 SELECT Blocklist ASSIGN TO "D:\Cobolprogramme\Guiprogramm\Blocklist_Spreadsheet.txt"
                 ORGANIZATION IS LINE SEQUENTIAL.
                 SELECT Blockbase ASSIGN TO Outputpath
                 ORGANIZATION IS LINE SEQUENTIAL.
DATA DIVISION.
FILE SECTION.
           FD  File-Name.
           01  Record-Name.
           02 Field1                  PIC X(400).
           FD  File-Name2.
           01  Record-Name3.
           02 Field1                  PIC X(20).
           FD  Cobolprog.
           01  Record-Name2.
           02 Field1                  PIC X(400).
           FD  Outputprog.
           01  Proglineoutput.
           02 Field1                  PIC X(400).
           FD  Suboutputprog.
           01  Proglineoutputalt.
           02 Field1                  PIC X(400).
           FD  Blocklist.
           01  Strresoutput.
           02 Field1                  PIC X(400).
           FD  Blockbase.
           01  Proglineoutput2.
           02 Field1                  PIC X(400).
WORKING-STORAGE SECTION.
    01 Str PICTURE X(400).
    01 Initstr PICTURE X(400).
    01 Funcname PICTURE X(400).
    01 Compstr PICTURE X(400).
    01 Compstr2 PICTURE X(400).
    01 Compstr3 PICTURE X(400).
    01 Prerawextstr PICTURE X(400).
    01 Rawextstr PICTURE X(400).
    01 Argsearchbase PICTURE X(400).
    01 Extstr PICTURE X(400).
    01 Extbase PICTURE X(400).
    01 Stralt PICTURE X(400).
    01 Str2 PICTURE X(1).
    01 Lastsign PICTURE X(1).
    01 Preextr PICTURE X(1).
    01 Checkstr PICTURE X(1).
    01 Checkstr2 PICTURE X(400).
    01 Checkstr3 PICTURE X(400).
    01 Strres PICTURE X(400).
    01 Progline PICTURE X(400).
    01 Progargalt1 PICTURE X(400).
    01 Progargalt2 PICTURE X(400).
    01 Progargalt3 PICTURE X(400).
    01 Progargalt4 PICTURE X(400).
    01 Outputpath PICTURE X(400).
    01 Progarg1 PICTURE X(400).
    01 Progarg2 PICTURE X(400).
    01 Progarg3 PICTURE X(400).
    01 Progarg4 PICTURE X(400).
    01 Prestrres PICTURE X(400).
    01 Cellcontent PICTURE X(20).
    01 Cellcontent2 PICTURE X(20).
    01 Subfunc PICTURE X(400).
    01 Startcol PICTURE X(1).
    01 Endcol PICTURE X(1).
    01 Relcol PICTURE X(1).
    01 Startcolnum PICTURE 9(1).
    01 Endcolnum PICTURE 9(1).
    01 Startcellnum PICTURE 9(4).
    01 Endcellnum PICTURE 9(4).
    01 Num PICTURE 9(4).
    01 Positionnum PICTURE 9(4).
    01 Prepositionnum PICTURE 9(4).
    01 Rowcount PICTURE 9(4).
    01 Cellcount PICTURE 9(4).
    01 Startnum PICTURE 9(4).
    01 Startnum2 PICTURE 9(4).
    01 Startnum3 PICTURE 9(4).
    01 Prestartnum PICTURE 9(4).
    01 Prestartnum2 PICTURE 9(4).
    01 Endnum PICTURE 9(4).
    01 Lastchar PICTURE 9(4).
    01 Quotcount PICTURE 9(4).
    01 Quotcount2 PICTURE 9(4).
    01 Quotmod PICTURE 9(4).
    01 Countval PICTURE 9(4).
    01 Lastadd PICTURE 9(4).
    01 Opsign PICTURE 9(4).
    *>01 Num33 PICTURE 9(4).
    01 Subcount PICTURE 9(4).
    01 Opsign2 PICTURE 9(4).
    01 Bracketnum PICTURE 9(4).
    01 Bracketclose PICTURE 9(4).
    01 Operandnum PICTURE 9(4).
    01 Operandnum2 PICTURE 9(4).
    01 Catnum PICTURE 9(4).
    01 Tallnum PICTURE 9(4).
    01 Tallnum2 PICTURE 9(4).
    01 Tallnum3 PICTURE 9(4).
    01 Tallnum4 PICTURE 9(4).
    01 Tallnum5 PICTURE 9(4).
    01 Basenum1 PICTURE 9(4).
    01 Basenum2 PICTURE 9(4).
    01 Basenum3 PICTURE 9(4).
    01 Basenum4 PICTURE 9(4).
    01 Countread PICTURE 9(4).
    01 Pretallnum PICTURE 9(4).
    01 Coltablecount PICTURE 9(4).
    01 Coltableres PICTURE 9(4).
    01 Coldiffnum PICTURE 9(1).
    01 Relnum PICTURE 9(1).
    01 Rangename PICTURE X(10).
    01 Rowcumnum PICTURE 9(1).
    01 Colcumnum PICTURE 9(1).
    01 Lastwordstart PICTURE 9(4).
    01 Spreadsheettable.
    05 Zeilen OCCURS 4 TIMES INDEXED BY Num39.
    10 Spalte1 PIC X(20).
    10 Spalte2 PIC X(20).
    10 Spalte3 PIC X(20).
    10 Spalte4 PIC X(20).
    01 Valuetable.
    05 Zeilen OCCURS 40 TIMES INDEXED BY Num40.
    10 Numcolumn PIC 9(9).
    01 Coltable2.
    05 Coltable OCCURS 40 TIMES INDEXED BY Num41.
    10 Colname PIC X(1).
    10 Colval PIC 9(1).
    01 Contenttable.
    05 Rangetable OCCURS 500 TIMES INDEXED BY Num43.
    10 Colcontent PIC X(20).
    01 Parsetable.
    05 Parsetest OCCURS 200 TIMES INDEXED BY Num44.
    10 Parsearg PIC X(20).
    10 Operand PIC X(20).
    10 Cellkind PIC X(20).
01 Funcvar PIC 9(4).
01 Num33 PIC X(20).
01 Linenum PIC 9(2).
01 Linenum2 PIC 9(2).
01 Textinput1 PIC X(5).
*>01 Baseline PIC X(60).
01  RESPONSEZ.
    05  RESPONSE-IN-WS  PIC X(20)        VALUE "C".
01  DATA-FROM-SCREEN.
    05  ID-IN-WS        PIC X(20).
    05  ID-IN-WS2       PIC X(20).
    05  ID-IN-WS3       PIC X(20).
    05  ID-IN-WS4       PIC X(20).
    05  ID-IN-WS5       PIC X(20).
    05  ID-IN-WS6       PIC X(20).
    05  ID-IN-WS7       PIC X(20).
    05  ID-IN-WS8       PIC X(20).
    05  ID-IN-WS9       PIC X(20).
    05  ID-IN-WS10       PIC X(20).
    05  ID-IN-WS11       PIC X(20).
    05  ID-IN-WS12       PIC X(20).
    05  ID-IN-WS13       PIC X(20).
    05  NAME-IN-WS      PIC X(20).
SCREEN SECTION.
01  DATA-ENTRY-SCREEN BACKGROUND-COLOR 3 FOREGROUND-COLOUR 0.
    05  VALUE "SPREADSHEET" BLANK SCREEN     LINE 1 COL 15.
    05  VALUE "A"                    LINE 2 COL 5.
    05  VALUE "B"                    LINE 2 COL 30.
    05  VALUE "C"                    LINE 2 COL 55.
    05  VALUE "D"                    LINE 2 COL 80.
    05  VALUE "__________________________________________" LINE 3 COL 4.
    *>05  VALUE "____________________________________________________________" LINE 3 COL 46.
    05 Baseline LINE 3 COL 46 PIC X(60).
    05  VALUE "1  |"                 LINE 4 COL 1.
    05  VALUE "|"                    LINE 4 COL 30.
    05  VALUE "|"                    LINE 4 COL 55.
    05  VALUE "|"                    LINE 4 COL 80.
    05  VALUE "|"                    LINE 4 COL 105.
    05  VALUE "|_________________________|_______________" LINE 5 COL 4.
    05  VALUE "_________|________________________|________________________|" LINE 5 COL 46.
    05  VALUE "2  |"                 LINE 6 COL 1.
    05  VALUE "|"                    LINE 6 COL 30.
    05  VALUE "|"                    LINE 6 COL 55.
    05  VALUE "|"                    LINE 6 COL 80.
    05  VALUE "|"                    LINE 6 COL 105.
    05  VALUE "|_________________________|_______________" LINE 7 COL 4.
    05  VALUE "_________|________________________|________________________|" LINE 7 COL 46.
    05  VALUE "3  |"                 LINE 8 COL 1.
    05  VALUE "|"                    LINE 8 COL 30.
    05  VALUE "|"                    LINE 8 COL 55.
    05  VALUE "|"                    LINE 8 COL 80.
    05  VALUE "|"                    LINE 8 COL 105.
    05  VALUE "|_________________________|_______________" LINE 9 COL 4.
    05  VALUE "_________|________________________|________________________|" LINE 9 COL 46.
    05  VALUE "4  |"                 LINE 10 COL 1.
    05  VALUE "|"                    LINE 10 COL 30.
    05  VALUE "|"                    LINE 10 COL 55.
    05  VALUE "|"                    LINE 10 COL 80.
    05  VALUE "|"                    LINE 10 COL 105.
    05  VALUE "|_________________________|_______________" LINE 11 COL 4.
    05  VALUE "_________|________________________|________________________|" LINE 11 COL 46.
    05  Textinput2                               LINE 4 COL 10 PIC X(5).
    05   ID-INPUT  LINE 4 COL 9  PIC  X(20)    TO Spalte1(1).
    05   ID-INPUT2 LINE 4 COL 34 PIC  X(20)    TO Spalte2(1).
    05   ID-INPUT3 LINE 4 COL 58 PIC  X(20)    TO Spalte3(1).
    05   ID-INPUT4 LINE 4 COL 82 PIC  X(20)    TO Spalte4(1).
    05   ID-INPUT5 LINE 6 COL 9  PIC  X(20)    TO Spalte1(2).
    05   ID-INPUT6 LINE 6 COL 34 PIC  X(20)    TO Spalte2(2).
    05   ID-INPUT7 LINE 6 COL 58 PIC  X(20)    TO Spalte3(2).
    05   ID-INPUT8 LINE 6 COL 82 PIC  X(20)    TO Spalte4(2).
    05   ID-INPUT9 LINE 8 COL 9  PIC  X(20)    TO Spalte1(3).
    05   ID-INPUT10 LINE 8 COL 34 PIC  X(20)    TO Spalte2(3).
    05   ID-INPUT11 LINE 8 COL 58 PIC  X(20)    TO Spalte3(3).
    05   ID-INPUT12 LINE 8 COL 82 PIC  X(20)    TO Spalte4(3).
    05   ID-INPUT13 LINE 10 COL 9 PIC  X(20)    TO Spalte1(4).
    05  VALUE "C - TO CONTINUE"                    LINE 15 COL 30.
    05  VALUE "Q - TO QUIT"                        LINE 16 COL 30.
    05  VALUE "ENTER RESPONSE"                     LINE 17 COL 30.
    05  RESPONSE-INPUT                             LINE 17 COL 45
                    PIC X         TO RESPONSE-IN-WS.
   01 CLEAR-SCREEN.
    02 BLANK SCREEN.
01  DATA-ENTRY-SCREEN2.
    05  VALUE "SPREADSHEET" BLANK SCREEN     LINE 1 COL 35.
    05  VALUE "A"                    LINE 2 COL 5.
    05  VALUE "B"                    LINE 2 COL 30.
    05  VALUE "C"                    LINE 2 COL 55.
    05  VALUE "D"                    LINE 2 COL 80.
    05  VALUE "__________________________________________" LINE 3 COL 4.
    05  VALUE "____________________________________________________________" LINE 3 COL 46.
    05   ID-INPUT33 LINE 5 COL Linenum PIC  X(4)     TO ID-IN-WS.
    05   ID-INPUT44 LINE 5 COL Linenum2 PIC  X(4)     TO ID-IN-WS2.
    PROCEDURE DIVISION.
    MOVE 1 TO Coltablecount.
    MOVE 1 TO Coltableres.
    MOVE 1 TO Num44.
    PERFORM Spreadsheetprog.
    *>This program can be used to Test if the functions work.
    *>PERFORM Testprog.
STOP RUN.
Testprog.
    *>Program to test if functions work as expected.
    MOVE "Textextrakt" TO Spalte3(2).
    MOVE "Textvalue" TO Spalte3(1).
    MOVE "1" TO Spalte1(2).
    MOVE "222" TO Spalte1(3).
    MOVE "12" TO Spalte1(4).
    MOVE "=SUMME(A2:A4)" TO Spalte2(1).
    PERFORM Screenprog.
    MOVE "=TEIL(C1;1;A4)" TO Spalte2(1).
    PERFORM Screenprog.
    MOVE "=SUMME(A2:A3)" TO Spalte2(1).
    PERFORM Screenprog.
    DISPLAY Parsearg(1) Parsearg(2) Parsearg(3) Parsearg(4) Parsearg(6) Parsearg(7).
    DISPLAY Cellkind(1) Cellkind(2) Cellkind(3) Cellkind(4).
    DISPLAY Operand(1) Operand(2) Operand(3) Operand(4).
    DISPLAY "Input: ".
    DISPLAY Colcontent(1) Colcontent(2) Colcontent(3) Colcontent(4) Colcontent(5) Colcontent(6).      
Rangetableinit.
    MOVE 1 TO Coltablecount.
Rangetableprog.
    PERFORM Cellextract.
    SET Num41 TO 1.
    PERFORM Coltablesearchprog 4 TIMES.
    PERFORM Rowdiffcalc.
    PERFORM Coldiffcalc.
    PERFORM Rangeconstruct.
    MOVE " " TO ID-Input4.
    MOVE " " TO ID-Input8.
    MOVE " " TO ID-Input12.
    MOVE Rangetable(1) TO ID-Input4.
    MOVE Rangetable(2) TO ID-Input8.
    MOVE Rangetable(3) TO ID-Input12.
    MOVE " " TO Prerawextstr.
Cellassign.
    PERFORM Cellextract.
    *> Number where the search starts.
    MOVE 1 TO Num41.
    PERFORM Coltablesearchprog 4 TIMES.
    PERFORM Rowdiffcalc.
    PERFORM Coldiffcalc.
    PERFORM Rangeconstruct.
Rangeconstruct.
MOVE Startcolnum TO Colcumnum.
DISPLAY "Start: " Startcolnum.
SET Num41 TO Startcolnum.
PERFORM Colnumselect UNTIL Colcumnum > Endcolnum.
*>PERFORM Rownuminit.
Rownuminit.
MOVE Startcellnum TO Relnum.
PERFORM Rownumselect Rowcount TIMES.
Colnumselect.
IF Colcumnum = Startcolnum THEN MOVE Startcol TO Relcol.
IF Colcumnum = Endcolnum THEN MOVE Endcol TO Relcol.
IF Colcumnum > Startcolnum AND Colcumnum < Endcolnum THEN MOVE Colname(Num41) TO Relcol.
PERFORM Rownuminit.
ADD 1 TO Colcumnum.
ADD 1 TO Num41.
Rownumselect.
STRING Relcol Relnum INTO Rangename.
DISPLAY "Base: " Rangename.
PERFORM Rangemove.
ADD 1 TO Relnum.
ADD 1 TO Rowcumnum.
Rangemove.
IF Relcol = "A" THEN MOVE Spalte1(Relnum) TO Colcontent(Coltablecount).
IF Relcol = "B" THEN MOVE Spalte2(Relnum) TO Colcontent(Coltablecount).
IF Relcol = "C" THEN MOVE Spalte3(Relnum) TO Colcontent(Coltablecount).
IF Relcol = "D" THEN MOVE Spalte4(Relnum) TO Colcontent(Coltablecount).
ADD 1 TO Coltablecount.
DISPLAY Spalte3(2).
DISPLAY Colcontent(1).
Valmove.
MOVE Prerawextstr TO Colcontent(Coltablecount).
ADD 1 TO Coltablecount.
Coltableinit.
SET Num41 TO 1.
MOVE "A" TO Colname(Num41).
MOVE 1 TO Colval(Num41).
ADD 1 TO Num41.
MOVE "B" TO Colname(Num41).
MOVE 2 TO Colval(Num41).
ADD 1 TO Num41.
MOVE "C" TO Colname(Num41).
MOVE 3 TO Colval(Num41).
ADD 1 TO Num41.
MOVE "D" TO Colname(Num41).
MOVE 4 TO Colval(Num41).
Coltablesearchprog.
SEARCH Coltable WHEN Colname(Num41) = Startcol  MOVE Colval(Num41) TO Startcolnum.
SEARCH Coltable WHEN Colname(Num41) = Endcol  MOVE Colval(Num41) TO Endcolnum.
ADD 1 TO Num41.
Coldiffcalc.
    SUBTRACT Startcolnum FROM Endcolnum GIVING Coldiffnum.
    ADD 1 TO Coldiffnum.
    DISPLAY "Number of Columns " Coldiffnum.
    MULTIPLY Rowcount BY Coldiffnum GIVING Cellcount.
DISPLAY "Number of Cells: " Cellcount.
Cellextract.
STRING Prerawextstr(1:1) INTO Startcol.
STRING Prerawextstr(2:1) INTO Startcellnum(4:1).
IF Extstr = "|," THEN MOVE Startcol TO Endcol ELSE STRING Prerawextstr(4:1) INTO Endcol.
IF Extstr = "|," THEN MOVE Startcellnum TO Endcellnum ELSE STRING Prerawextstr(5:1) INTO Endcellnum(4:1).
DISPLAY "Startcol: " Startcol.
Rowdiffcalc.
SUBTRACT Startcellnum FROM Endcellnum GIVING Rowcount.
ADD 1 TO Rowcount.
DISPLAY "Number of Rows " Rowcount.
Valueselectprog.
SET Num39 TO 1.
MOVE Spalte1(2)(1:8) TO Numcolumn(1).
MOVE Spalte1(3)(1:8) TO Numcolumn(2).
MOVE Spalte1(4)(1:8) TO Numcolumn(3).
Screenprog.
    *>OPEN OUTPUT Suboutputprog.
    *>DISPLAY "ID: " ID-IN-WS2.
    *>Angepasst fuer Test
    MOVE Spalte2(1) TO Str.
    *>MOVE "=TEIL($C$2;3;5)" TO Str.
    *>WRITE Record-Name3 FROM "Text".
    *>WRITE  Record-Name3 FROM ID-IN-WS2.
    MOVE 2 TO Num.
    MOVE 1 TO Tallnum3.
    PERFORM Extprog UNTIL Opsign2 = 1.
    *>CLOSE Suboutputprog.
    *>DISPLAY "Func123: " Funcname.
    DISPLAY "Functioninputs: ".
    DISPLAY Colcontent(1).
    IF Funcname = "TEIL" THEN PERFORM Substringfuncprog.
    IF Funcname = "SUMME" THEN PERFORM Sumfuncprog.
    MOVE " " TO Extbase.
    MOVE " " TO Funcname.
    MOVE " " TO Extstr.
    MOVE " " TO Str.
    MOVE " " TO Str2.
    MOVE 0 TO Opsign2.
    MOVE 0 TO Startnum.
    MOVE 0 TO Endnum.
    MOVE " " TO Cellcontent.
    MOVE 0 TO Tallnum4.
    MOVE " " TO Spalte2(1).
    MOVE 0 TO Tallnum2.
    MOVE 0 TO Tallnum3.
    MOVE Coltablecount TO Coltableres.
    Rangesumprog.
    *>MOVE Spalte1(2)(1:4) TO Basenum1.
    *>MOVE Spalte1(3)(1:4) TO Basenum2.
    *>MOVE Spalte1(4)(1:4) TO Basenum3.
Sumfuncsub.
MOVE Rangetable(Coltableres)(1:4) TO Basenum2.
DISPLAY "Basenum2: " Basenum2.
ADD Basenum2 TO Basenum3.
ADD 1 TO Basenum1.
ADD 1 TO Coltableres.
Sumfuncprog.
DISPLAY "Coltableres22: " Coltableres.
*>ADD 3 TO Coltableres.
MOVE 1 TO Basenum1.
PERFORM Sumfuncsub UNTIL Basenum1 > Cellcount.
*>ADD Basenum1 TO Basenum2.
*>ADD Basenum2 TO Basenum3.
MOVE Basenum3 TO Extbase.
*>ADD Basenum2 TO Basenum3.
MOVE Extbase TO ID-Input2.
MOVE 0 TO Basenum3.
MOVE 0 TO Basenum2.
DISPLAY "Result: " Extbase.
Substringfuncprog.
*>MOVE "1" TO Colcontent(2).
*>MOVE "3" TO Colcontent(3).
MOVE Colcontent(Coltableres) TO Cellcontent2.
MOVE Colcontent(Coltableres + 1)(1:4) TO Startnum.
MOVE Colcontent(Coltableres + 2)(1:4) TO Endnum.
*>DISPLAY "Range: " Startnum Endnum.
STRING Cellcontent2(Startnum:Endnum) INTO Extbase.
DISPLAY "Functionoutput: " Extbase.
DISPLAY "Coltablecount: " Coltablecount.
MOVE 0 TO Startnum.
MOVE 0 TO Endnum.
MOVE Extbase TO ID-Input2.
*>DISPLAY "Result: " Extbase.
MOVE " " TO Extbase.
    Extprog.
    *>MOVE "TEIL(C2;3;5)" TO Str.
    STRING Str(Num:1) INTO Str2.
    IF Str2 = """" THEN ADD 1 TO Quotcount.
    IF Str2 = """" THEN DIVIDE Quotcount BY 2 GIVING Quotcount2 REMAINDER Quotmod.
    IF Preextr = SPACE AND Str2 NOT EQUAL TO SPACE AND Str2 = """" AND Quotmod = 1 THEN MOVE Num TO Endnum.
    IF Preextr = SPACE AND Str2 NOT EQUAL TO SPACE AND Quotmod = 0 THEN MOVE Num TO Endnum.
    IF Quotmod = 0 AND Str2 = "(" THEN MOVE 1 TO Opsign.
    IF Quotmod = 0 AND Str2 = ")" THEN MOVE 1 TO Opsign.
    IF Quotmod = 0 AND Str2 = "+" THEN MOVE 1 TO Opsign.
    IF Quotmod = 0 AND Str2 = "-" THEN MOVE 1 TO Opsign.
    IF Quotmod = 0 AND Str2 = "*" THEN MOVE 1 TO Opsign.
    IF Quotmod = 0 AND Str2 = "/" THEN MOVE 1 TO Opsign.
    IF Quotmod = 0 AND Str2 = "^" THEN MOVE 1 TO Opsign.
    IF Quotmod = 0 AND Str2 = "<" THEN MOVE 1 TO Opsign.
    IF Quotmod = 0 AND Str2 = ">" THEN MOVE 1 TO Opsign.
    IF Quotmod = 0 AND Str2 = "=" THEN MOVE 1 TO Opsign.
    IF Quotmod = 0 AND Str2 = "&" THEN MOVE 1 TO Opsign.
    IF Quotmod = 0 AND Str2 = " " THEN MOVE 1 TO Opsign2.
    IF Opsign2 = 1 THEN DISPLAY "Position at End:" Num.
    IF Quotmod = 0 AND Str2 = ";" THEN MOVE 1 TO Opsign.
    IF Quotmod = 0 AND Str2 = ")" THEN MOVE 1 TO Opsign.
    IF Opsign = 1 THEN MOVE Num TO Positionnum.
    IF Opsign = 1 OR Opsign2 = 1 THEN PERFORM Substrprog.
    MOVE 0 TO Opsign.
    MOVE Positionnum TO Prepositionnum.
    ADD 1 TO Num.
    Substrprog.
    MOVE Tallnum3 TO Tallnum2.
    MOVE Num TO Tallnum3.
    IF Opsign2 = 0 AND Tallnum3 > Tallnum2 + 1 THEN STRING Str(Tallnum2 + 1:Tallnum3 - Tallnum2 - 1) INTO Extstr.
    IF Opsign2 = 1 AND Tallnum3 > Tallnum2 + 1 THEN STRING Str(Tallnum2 + 1:Tallnum3 - Tallnum2 - 1) INTO Extstr.
    STRING Str(Tallnum3:1) INTO Checkstr.
    MOVE Checkstr TO Operand(Num44).
    MOVE Checkstr TO Preextr.
    DISPLAY "Extract: " Extstr.
    MOVE Extstr TO Parsearg(Num44).
    *>MOVE "Abcontent" TO Cellcontent.
    DISPLAY "Operand: " Checkstr.
    MOVE Extstr TO Prerawextstr.
    PERFORM Substrprogalt.
    PERFORM Inspectprog.
    MOVE " " TO Extstr.
    ADD 1 TO Tallnum4.
    ADD 1 TO Num44.
    Substrprogalt.
    IF Tallnum4 = 0 THEN MOVE Extstr TO Funcname.
    IF Tallnum4 = 0 THEN DISPLAY "Function: " Funcname.
    *>IF Tallnum4 = 2 AND Funcname = "TEIL" THEN MOVE Extstr TO Startnum.
    IF Tallnum4 = 1 THEN DISPLAY "Range: " Extstr.
    *>IF Tallnum4 = 1 AND Extstr = "A2:A4" THEN PERFORM Rangesumprog.
    *>IF Tallnum4 = 2 THEN DISPLAY "Start:" Startnum.
    *>IF Tallnum4 = 3 THEN MOVE Extstr TO Endnum.
    *>IF Tallnum4 = 3 THEN DISPLAY "End:" Endnum.
    MOVE Extstr TO Prerawextstr.
    *>PERFORM Inspectprog.
    *>MOVE " " TO Extstr.
    *>ADD 1 TO Tallnum4.
    Extprog2.
    IF Opsign = 1 AND Operandnum > 0 AND Bracketnum = 0 THEN STRING Str(Startnum2 + 1:Num - Startnum2 - 1) INTO Subfunc.
    *>IF Opsign = 1 AND Operandnum > 0 AND Bracketnum = 0 THEN ADD 1 TO Subcount.
    IF Opsign2 >= 1 AND Operandnum2 > 0 AND Bracketnum = 1 THEN STRING Str(Startnum3 + 1:Num - Startnum3 - 1) INTO Subfunc.
    IF Opsign2 >= 1 AND Operandnum2 > 0 AND Bracketnum = 1 THEN WRITE Proglineoutputalt FROM Subfunc.
    IF Opsign2 >= 1 AND Operandnum2 > 0 AND Bracketnum = 1 THEN DISPLAY "Sub4: " Subfunc.
    IF Opsign2 >= 1 AND Operandnum2 > 0 AND Bracketnum = 1 THEN ADD 1 TO Subcount.
    IF Opsign2 >= 1 AND Operandnum2 > 0 AND Bracketnum = 1 THEN MOVE Num TO Startnum2.
    IF Opsign2 >= 1 AND Operandnum2 > 0 AND Bracketnum = 1 THEN MOVE Num TO Startnum3.
    IF Quotmod = 0 AND Str2 = ";" AND Bracketnum = 1 THEN MOVE 0 TO Operandnum2.
    IF Opsign = 1 AND Operandnum > 0 AND Bracketnum = 0 AND Subfunc NOT EQUAL TO SPACE THEN ADD 1 TO Subcount.
    IF Opsign = 1 AND Operandnum > 0 AND Bracketnum = 0 AND Subfunc NOT EQUAL TO SPACE THEN DISPLAY "Sub: " Subfunc.
    IF Opsign = 1 AND Operandnum > 0 AND Bracketnum = 0 AND Subfunc NOT EQUAL TO SPACE THEN WRITE Proglineoutputalt FROM Subfunc.
    IF Opsign = 1 AND Operandnum > 0 AND Bracketnum = 0 THEN MOVE Num TO Startnum2.
    MOVE " " TO Subfunc.
    IF Countval > 1 AND Endnum NOT EQUAL TO Startnum THEN STRING Str(Startnum:Endnum - Startnum) INTO Extstr.
    IF Countval > 1 AND Endnum NOT EQUAL TO Startnum THEN MOVE Extstr TO Extbase.
    IF Countval > 1 AND Endnum NOT EQUAL TO Startnum THEN STRING Str(Startnum:1) INTO Lastsign.
    IF Countval > 1 AND Endnum NOT EQUAL TO Startnum AND Lastsign NOT EQUAL TO """" THEN PERFORM Replaceprog.
    IF Countval > 1 AND Endnum NOT EQUAL TO Startnum AND Lastsign EQUAL TO """" THEN PERFORM Stringresinitprog.
    IF Countval > 1 AND Endnum NOT EQUAL TO Startnum THEN MOVE " " TO Extstr.
    MOVE Endnum TO Prestartnum.
    IF Preextr = SPACE AND Str2 NOT EQUAL TO SPACE AND Str2 = """" AND Quotmod = 1 THEN MOVE Endnum TO Startnum.
    IF Preextr = SPACE AND Str2 NOT EQUAL TO SPACE AND Quotmod = 0 THEN MOVE Endnum TO Startnum.
    IF Preextr = SPACE AND Str2 NOT EQUAL TO SPACE AND Str2 = """" AND Quotmod = 1 THEN ADD 1 TO Countval.
    IF Preextr = SPACE AND Str2 NOT EQUAL TO SPACE AND Quotmod = 0 THEN ADD 1 TO Countval.
    IF Preextr EQUAL TO Space AND Str2 NOT EQUAL TO SPACE AND Quotmod = 0 THEN MOVE Num TO Lastwordstart.
    IF Preextr EQUAL TO Space AND Str2 NOT EQUAL TO SPACE AND Str2 ="""" AND Quotmod = 1 THEN MOVE Num TO Lastwordstart.
    MOVE Str2 TO Preextr.
    IF Str2 NOT EQUAL TO Space THEN MOVE Num TO Lastchar.
    ADD 1 TO Num.
    MOVE 0 TO Opsign.
    IF Operandnum > 0 AND Num = 400 THEN STRING Str(Startnum2 + 2:Num - Startnum2 - 2) INTO Subfunc.
    IF Operandnum > 0 AND Num = 400 AND Subfunc NOT EQUAL TO SPACE THEN WRITE Proglineoutputalt FROM Subfunc.
    IF Operandnum > 0 AND Num = 400 THEN ADD 1 TO Subcount.
    IF Operandnum > 0 AND Num = 400 AND Subfunc NOT EQUAL TO SPACE THEN DISPLAY "Sub: " Subfunc.
    IF Opsign2 >= 1 AND Num = 400 AND Operandnum NOT LESS 0 THEN STRING Str(Startnum2 + 1:Bracketclose - Startnum2 - 1) INTO Subfunc.
    IF Num = 400 AND Opsign2 >= 1 THEN DISPLAY "Sub3: " Subfunc.
    IF Num = 400 AND Opsign2 >= 1 THEN ADD 1 TO Subcount.
    IF Num = 400 THEN WRITE Proglineoutputalt FROM "END".
    Inspectprog.
    INSPECT Extstr REPLACING All "A" BY "|", "B" BY "|", "C" BY "|", "D" BY "|".
    INSPECT Extstr REPLACING All "E" BY "|", "F" BY "|", "G" BY "|", "H" BY "|".
    INSPECT Extstr REPLACING All "I" BY "|", "J" BY "|", "K" BY "|", "L" BY "|".
    INSPECT Extstr REPLACING All "M" BY "|", "N" BY "|", "O" BY "|", "P" BY "|".
    INSPECT Extstr REPLACING All "Q" BY "|", "R" BY "|", "S" BY "|", "T" BY "|".
    INSPECT Extstr REPLACING All "U" BY "|", "V" BY "|", "W" BY "|", "X" BY "|".
    INSPECT Extstr REPLACING All "Y" BY "|", "Z" BY "|", "1" BY ",", "2" BY ",".
    INSPECT Extstr REPLACING All "3" BY ",", "4" BY ",", "5" BY ",", "6" BY ",".
    INSPECT Extstr REPLACING All "7" BY ",", "8" BY ",", "9" BY ",", "0" BY ",".
    DISPLAY "Replaced: " Extstr.
    MOVE Extstr TO Cellkind(Num44).
    *>IF Extstr = "|,:|," THEN DISPLAY "Cellrange".
    IF Extstr = "|,:|," THEN DISPLAY "Cellrange".
    IF Extstr = "|," THEN PERFORM Rangetableprog.
    IF Extstr = " " THEN DISPLAY "Empty".
    IF Extstr = "|,:|," THEN PERFORM Rangetableprog.
    IF Extstr = "," THEN PERFORM Valmove.
    *>MOVE " " TO Extstr.
    Replaceprog.
    DISPLAY "Output".
    Stringresinitprog.
    DISPLAY "Stringres".
Screengenerationprog.
    MOVE "____________________________________________________________" TO Baseline.
MOVE "Hello" TO Textinput1.
MOVE Textinput1 TO Textinput2.
MOVE "Hello" TO ID-INPUT.
MOVE "Base" TO ID-INPUT2.
MOVE 9 TO Linenum.
MOVE 11 TO Linenum2.
DISPLAY DATA-ENTRY-SCREEN2.
ACCEPT DATA-ENTRY-SCREEN2.
Spreadsheetprog.
MOVE "____________________________________________________________" TO Baseline.
MOVE "Hello" TO Textinput1.
MOVE Textinput1 TO Textinput2.
MOVE "Hello" TO ID-INPUT.
MOVE "Into" TO ID-INPUT2.
MOVE "3" TO ID-INPUT5.
DISPLAY DATA-ENTRY-SCREEN.
ACCEPT DATA-ENTRY-SCREEN.
SET Num39 TO 1.
PERFORM Screenprog.
DISPLAY DATA-ENTRY-SCREEN.
ACCEPT DATA-ENTRY-SCREEN.
PERFORM Screenprog.
MOVE " " TO Extstr.
DISPLAY DATA-ENTRY-SCREEN.
ACCEPT DATA-ENTRY-SCREEN.
PERFORM Screenprog.
MOVE " " TO Extstr.
DISPLAY DATA-ENTRY-SCREEN.
ACCEPT DATA-ENTRY-SCREEN.
PERFORM Screenprog.
MOVE " " TO Extstr.
DISPLAY DATA-ENTRY-SCREEN.
ACCEPT DATA-ENTRY-SCREEN.
PERFORM Screenprog.
MOVE " " TO Extstr.
DISPLAY DATA-ENTRY-SCREEN.
ACCEPT DATA-ENTRY-SCREEN.
PERFORM Screenprog.
MOVE " " TO Extstr.
DISPLAY DATA-ENTRY-SCREEN.
ACCEPT DATA-ENTRY-SCREEN.
PERFORM Screenprog.
Furtherscreens.
DISPLAY DATA-ENTRY-SCREEN.
ACCEPT DATA-ENTRY-SCREEN.
PERFORM Screenprog.
DISPLAY DATA-ENTRY-SCREEN.
ACCEPT DATA-ENTRY-SCREEN.
PERFORM Screenprog.
DISPLAY DATA-ENTRY-SCREEN.
ACCEPT DATA-ENTRY-SCREEN.
PERFORM Screenprog.
DISPLAY DATA-ENTRY-SCREEN.
ACCEPT DATA-ENTRY-SCREEN.
PERFORM Screenprog.
DISPLAY DATA-ENTRY-SCREEN.
ACCEPT DATA-ENTRY-SCREEN.
PERFORM Screenprog.
DISPLAY DATA-ENTRY-SCREEN.
ACCEPT DATA-ENTRY-SCREEN.
PERFORM Screenprog.
DISPLAY DATA-ENTRY-SCREEN.
ACCEPT DATA-ENTRY-SCREEN.
PERFORM Screenprog.
DISPLAY DATA-ENTRY-SCREEN.
ACCEPT DATA-ENTRY-SCREEN.
PERFORM Screenprog.
Subprog.
PERFORM Screenprog.
DISPLAY DATA-ENTRY-SCREEN.
ACCEPT DATA-ENTRY-SCREEN.
WRITE  Record-Name3 FROM Str.
*>DISPLAY CLEAR-SCREEN.
Subprog2.
PERFORM Screenprog.
DISPLAY DATA-ENTRY-SCREEN2.
ACCEPT DATA-ENTRY-SCREEN2.
WRITE  Record-Name3 FROM Str.
END PROGRAM YOUR-PROGRAM-NAME.
