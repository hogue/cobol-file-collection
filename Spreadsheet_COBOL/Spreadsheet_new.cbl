*>****************************************************************
*> Author:
*> Date:
*> Purpose:
*> Tectonics: cobc
*>****************************************************************
    IDENTIFICATION DIVISION.
    PROGRAM-ID. YOUR-PROGRAM-NAME.
    ENVIRONMENT DIVISION.
    INPUT-OUTPUT SECTION.
    FILE-CONTROL.
    SELECT Inputfile ASSIGN TO "D:\Cobolprogramme\Spreadsheetgui\Inputvalues.txt"
    ORGANIZATION IS LINE SEQUENTIAL.
    SELECT Outputfile ASSIGN TO "D:\Cobolprogramme\Charparser\Coboltemplate.txt"
    ORGANIZATION IS LINE SEQUENTIAL.
    SELECT Fodstemplate ASSIGN TO "D:\Spreadsheetmacro\Dateivorlagen_fods\Vorlage_fods.fods"
    ORGANIZATION IS LINE SEQUENTIAL.
    SELECT Fodsoutput ASSIGN TO "D:\Spreadsheetmacro\Dateivorlagen_fods\Dateiergebnis.fods"
    ORGANIZATION IS LINE SEQUENTIAL.
    DATA DIVISION.
    FILE SECTION.
    FD  Inputfile.
    01  Record-Name3.
    02 Field1                  PIC X(200).
    FD  Outputfile.
    01  Record-Name4.
    02 Field2                  PIC X(200).
    FD  Fodstemplate.
    01  Record-Name5.
    02 Filecontent                  PIC X(20000).
    FD  Fodsoutput.
    01  Record-Name6.
    02 Filecontent2                  PIC X(20000).
    WORKING-STORAGE SECTION.
    01 Cellposaltdiff PICTURE 9(4).
    01 Cellposalt2 PICTURE 9(4).
    01 Cellposalt3 PICTURE 9(4).
    01 Sumpart1 PICTURE 9(4).
    01 Sumpart2 PICTURE 9(4).
    01 Sumres PICTURE 9(4).
    01 Restres PICTURE 9(4).
    01 Restdiff PICTURE 9(4).
    01 Cellrangepos PICTURE 9(4).
    01 Cobolstatement PICTURE X(200).
    01 Cobolstatementout PICTURE X(400).
    01 Coboltemplate PICTURE X(200).
    01 Substr PICTURE X(2).
    01 Firstchar PICTURE X(2).
    01 Presubstr PICTURE X(2).
    01 Extstr PICTURE X(200).
    01 Subtext PICTURE X(200).
    01 Subtextalt PICTURE X(200).
    01 Scriptbase PICTURE X(200).
    01 Scriptoutput PICTURE X(200).
    01 Inspectres PICTURE X(70).
    01 Relstr PICTURE X(10).
    01 Lengthnum PICTURE 9(4).
    01 Num PICTURE 9(4).
    01 Cellcontentnum PICTURE 9(4).
    01 Stopnum PICTURE 9.
    01 Templatecount PICTURE 9(4).
    01 Substrinput PICTURE X(200).
    01 Substrconcat PICTURE X(200).
    01 Concatbase PICTURE X(200).
    01 Substrstart PICTURE 9(4).
    01 Substrcount PICTURE 9(4).
    01 Charcount PICTURE 9(4).
    01 Quotcount PICTURE 9(4).
    01 Quotresult PICTURE 9(4).
    01 Quotrest PICTURE 9(4).
    01 Prequotrest PICTURE 9(4).
    01 Lengthcalcnum PICTURE 9(4).
    01 Lengthcalcres PICTURE 9(4).
    01 Prelengthcalcres PICTURE 9(4).
    01 Cellcontentpos PICTURE 9(4).
    01 Rowstart PICTURE 9(4).
    01 Colstart PICTURE 9(4).
    01 Colstartnum PICTURE 9(4).
    01 Rowstartnum PICTURE 9(4).
    01 Colcountadd PICTURE 9(4).
    01 Coladresstext PICTURE X(4).
    01 Rowadressnumtext PICTURE X(4).
    01 Rowadressnum PICTURE 9(4).
    01 Coladressnum PICTURE 9(4).
    01 Celladressres PICTURE 9(4).
    01 Celloutput PICTURE X(200).
    01 Substraltstr PICTURE X(200).
    01 Cellposcalc PICTURE 9(4).
    01 Fodstemplatetext PICTURE X(20000).
    01 Relrow PICTURE 9(4).
    01 Relcol PICTURE 9(4).
    01 Scriptcount PICTURE 9(4).
    01 Scriptpos PICTURE 9(4).
    01 Valinput PICTURE 9(4).
    01 Valinputtext PICTURE X(200).
    01 Rangecontentpos PICTURE 9(4).
    01 Firstcol PICTURE 9(4).
    01 Lastcol PICTURE 9(4).
    01 Currentcol PICTURE 9(4).
    01 Coldiv PICTURE 9(4).
    01 Colrest PICTURE 9(4).
    01 Directinput PICTURE 9(1).
    01 Inputoption1 PICTURE X(1).
    01 Inputoption2 PICTURE X(1).
    01 Inputoption3 PICTURE X(4).
    01 Spaltenspec1 PICTURE X(3).
    01 Spaltenspec2 PICTURE X(7).
    01 Spaltenspec3 PICTURE X(7).
    01 Spaltenspec4 PICTURE X(7).
    01 Zeilennummer1 PICTURE X(7).
    01 Zeilennummer2 PICTURE X(7).
    01 Zeilennummer3 PICTURE X(7).
    01 Zeilennummer4 PICTURE X(7).
    01 Zeilennummer5 PICTURE X(7).
    01 Zeilennummer6 PICTURE X(7).
    01 Zeilennummer7 PICTURE X(7).
    01 Zeilennummer8 PICTURE X(7).
    01 Zeilennummer9 PICTURE X(7).
    01 Zeilennummer10 PICTURE X(7).
    01 Trimresult PICTURE X(200).
    01 Spacedetectnum PICTURE 9(4).
    01 Prespacedetectnum PICTURE 9(4).
    01 Trimpos PICTURE 9(4).
    01 Posnum PICTURE 9(4).
    01 Num2 PICTURE 9(4).
    01 Tablerow PICTURE 9(4).
    01 Viewspec PICTURE X(1).
    01 Cobolwords.
    05 Wordlist2 OCCURS 1000 TIMES INDEXED BY Indexnum.
    10 Cobolword PICTURE X(50).
    10 Charcount3 PICTURE 9(4).
    01 Wordtable.
    05 Wordlist OCCURS 50 TIMES.
    10 Subtext2 PICTURE X(200).
    10 Catword PICTURE X(2).
    10 Charcount2 PICTURE 9(4).
    01 Scriptwords.
    05 Wordlist3 OCCURS 1000 TIMES INDEXED BY Indexnum2.
    10 Cobolstatement2 PICTURE X(50).
    10 Charcount4 PICTURE 9(4).
    01 Resulttable.
    05 Wordlist4 OCCURS 200 TIMES.
    10 Subtext3 PICTURE X(200).
    10 Catword3 PICTURE X(2).
    10 Charcount4 PICTURE 9(4).
    01 Colcount PICTURE 9(4).
    01 Rowcount PICTURE 9(4).
    01 Contentinputtable.
    05 Cellcontentlist OCCURS 500 TIMES.
    10 Spalte1 PICTURE X(200).
    10 Spalte2 PICTURE X(200).
    10 Spalte3 PICTURE X(200).
    10 Spalte4 PICTURE X(200).
    01 Contenttable.
    05 Cellcontentlist2 OCCURS 10000 TIMES.
    10 Cellcontent PICTURE X(200).
    01 Cellrangetable.
    05 Cellcontentlist2 OCCURS 10000 TIMES.
    10 Cellrange  PICTURE X(200).
    *>Tabelle zum Zellbereich berechnen
    01 Cellpathtable.
    05 Cellcontentlist3 OCCURS 10000 TIMES.
    *>Zelle fuer Celladressres
    10 Celllocation PICTURE 9(4).
    *>Zelle fuer Scriptpos
    10 Cellposalt PICTURE 9(4).
    *>10 Cellrelation PICTURE X(200).
    01 Cellstatustable.
    05 Cellcontentlist4 OCCURS 1000 TIMES.
    10 Celladressname PIC X(200).
    10 Cellstatus PIC X(1).
    01 Inputnum PIC 9(4).
    SCREEN SECTION.
    01 Celladressoutput BACKGROUND-COLOR 3 FOREGROUND-COLOUR 0.
    05  VALUE "Zelladdressinformationen" BLANK SCREEN LINE  1 COL 15.
    05 LINE 5 COLUMN 45 PIC X(70) FROM Inspectres.
    05  Inputoptionval1 LINE  4 COLUMN 5 PIC X(4) TO Inputoption3.
    01 Cellspecscreen BACKGROUND-COLOR 3 FOREGROUND-COLOUR 0.
    05  VALUE "Zelladdressinformationen" BLANK SCREEN LINE  1 COL 15.
    05 VALUE "Zelladresse:" LINE  5 COL 5.
    05 LINE 5 COLUMN 25 PIC X(20) FROM Extstr.
    05 LINE 5 COLUMN 45 PIC X(70) FROM Inspectres.
    05  Inputoptionval1 LINE  7 COLUMN 25 PIC X(1) TO Inputoption1.
    01 Progstartcscreen BACKGROUND-COLOR 3 FOREGROUND-COLOUR 0.
    05  VALUE "Tabellenoptionen" BLANK SCREEN     LINE  1 COL 15.
    05 VALUE "Datei erstellen:" LINE  5 COL 5.
    05  Inputoptionval1 LINE  5 COLUMN 25 PIC X(1) TO Inputoption1.
    05 VALUE "Datei laden:" LINE 6 COL 5.
    05  Inputoptionval2 LINE 6 COLUMN 25 PIC X(1) TO Inputoption2.
    01 Tablespecscreen BACKGROUND-COLOR 3 FOREGROUND-COLOUR 0.
    05  VALUE "Tabellendetails" BLANK SCREEN LINE 1 COL 15.
    05 VALUE "Spaltenanzahl:" LINE  5 COL 5.
    05  Columncount LINE  5 COLUMN 25 PIC X(4) TO Colcount.
    05 VALUE "Zeilenanzahl:" LINE  5 COL 42.
    05  Rowcount2 LINE  5 COL 55 PIC X(4)         TO Rowcount.
    01 Tablespecresultscreen BACKGROUND-COLOR 3 FOREGROUND-COLOUR 0.
    05  VALUE "Tabellendetails Eingaben" BLANK SCREEN     LINE  1 COL 15.
    05 VALUE "Spaltenanzahl:" LINE  5 COL 5.
    05  Colcountres  PIC 9(4)                           LINE  5 COL 35.
    05 VALUE "Zeilenanzahl:" LINE  5 COL 42.
    05  Rowcountres2    PIC 9(4) LINE  5 COL 65.
    05 VALUE "Spaltendiff:" LINE  7 COL 5.
    05  Columncountdiff LINE  7 COLUMN 25 PIC X(4) FROM Colcountadd.
    05  RESPONSE-INPUT LINE  27 COL 45 PIC X TO Viewspec.
    01  DATA-ENTRY-SCREEN BACKGROUND-COLOR 3 FOREGROUND-COLOUR 0.
    05  VALUE "TABELLENKALKULATIONSPROGRAMM" BLANK SCREEN     LINE  1 COL 15.
    05  VALUE "SPALTE:"              LINE  2 COL 1.
    05  SpaltennameIn1 LINE  2 COL 9 PIC X(3) FROM Spaltenspec1.
    05  SpaltennameIn2 LINE  2 COL 34 PIC X(3) FROM Spaltenspec2.
    05  SpaltennameIn3 LINE  2 COL 59 PIC X(3) FROM Spaltenspec3.
    05  SpaltennameIn4 LINE  2 COL 84 PIC X(3) FROM Spaltenspec4.
    05  VALUE "ZEILE:"               LINE  3 COL 1.
    05  VALUE "_______________________________________" LINE  3 COL 7.
    05  VALUE "____________________________________________________________" LINE  3 COL 46.
    *>05 Baseline LINE  3 COL 46 PIC X(60).
    05  ZeilennummerIn1              LINE  4 COL 1 PIC X(7) FROM Zeilennummer1.
    05  VALUE "|"                    LINE  4 COL 30.
    05  VALUE "|"                    LINE  4 COL 55.
    05  VALUE "|"                    LINE  4 COL 80.
    05  VALUE "|"                    LINE  4 COL 105.
    05  VALUE "|______________________|_______________" LINE  5 COL 7.
    05  VALUE "_________|________________________|________________________|" LINE  5 COL 46.
    05  ZeilennummerIn2              LINE  6 COL 1 PIC X(7) FROM Zeilennummer2.
    05  VALUE "|"                    LINE  6 COL 30.
    05  VALUE "|"                    LINE  6 COL 55.
    05  VALUE "|"                    LINE  6 COL 80.
    05  VALUE "|"                    LINE  6 COL 105.
    05  VALUE "|______________________|_______________" LINE  7 COL 7.
    05  VALUE "_________|________________________|________________________|" LINE  7 COL 46.
    05  ZeilennummerIn3              LINE  8 COL 1 PIC X(7) FROM Zeilennummer3.
    05  VALUE "|"                    LINE  8 COL 30.
    05  VALUE "|"                    LINE  8 COL 55.
    05  VALUE "|"                    LINE  8 COL 80.
    05  VALUE "|"                    LINE  8 COL 105.
    05  VALUE "|______________________|_______________" LINE  9 COL 7.
    05  VALUE "_________|________________________|________________________|" LINE  9 COL 46.
    05  ZeilennummerIn4              LINE  10 COL 1 PIC X(7) FROM Zeilennummer4.
    05  VALUE "|"                    LINE  10 COL 30.
    05  VALUE "|"                    LINE  10 COL 55.
    05  VALUE "|"                    LINE  10 COL 80.
    05  VALUE "|"                    LINE  10 COL 105.
    05  VALUE "|______________________|_______________" LINE  11 COL 7.
    05  VALUE "_________|________________________|________________________|" LINE  11 COL 46.
    05  ZeilennummerIn5              LINE  12 COL 1 PIC X(7) FROM Zeilennummer5.
    05  VALUE "|"                    LINE  12 COL 30.
    05  VALUE "|"                    LINE  12 COL 55.
    05  VALUE "|"                    LINE  12 COL 80.
    05  VALUE "|"                    LINE  12 COL 105.
    05  VALUE "|______________________|_______________" LINE  13 COL 7.
    05  VALUE "_________|________________________|________________________|" LINE  13 COL 46.
    05  ZeilennummerIn6              LINE  14 COL 1 PIC X(7) FROM Zeilennummer6.
    05  VALUE "|"                    LINE  14 COL 30.
    05  VALUE "|"                    LINE  14 COL 55.
    05  VALUE "|"                    LINE  14 COL 80.
    05  VALUE "|"                    LINE  14 COL 105.
    05  VALUE "|______________________|_______________" LINE  15 COL 7.
    05  VALUE "_________|________________________|________________________|" LINE  15 COL 46.
    05  ZeilennummerIn7              LINE  16 COL 1 PIC X(7) FROM Zeilennummer7.
    05  VALUE "|"                    LINE  16 COL 30.
    05  VALUE "|"                    LINE  16 COL 55.
    05  VALUE "|"                    LINE  16 COL 80.
    05  VALUE "|"                    LINE  16 COL 105.
    05  VALUE "|______________________|_______________" LINE  17 COL 7.
    05  VALUE "_________|________________________|________________________|" LINE  17 COL 46.
    05  ZeilennummerIn8              LINE  18 COL 1 PIC X(7) FROM Zeilennummer8.
    05  VALUE "|"                    LINE  18 COL 30.
    05  VALUE "|"                    LINE  18 COL 55.
    05  VALUE "|"                    LINE  18 COL 80.
    05  VALUE "|"                    LINE  18 COL 105.
    05  VALUE "|______________________|_______________" LINE  19 COL 7.
    05  VALUE "_________|________________________|________________________|" LINE  19 COL 46.
    05  ZeilennummerIn9              LINE  20 COL 1 PIC X(7) FROM Zeilennummer9.
    05  VALUE "|"                    LINE  20 COL 30.
    05  VALUE "|"                    LINE  20 COL 55.
    05  VALUE "|"                    LINE  20 COL 80.
    05  VALUE "|"                    LINE  20 COL 105.
    05  VALUE "|______________________|_______________" LINE  21 COL 7.
    05  VALUE "_________|________________________|________________________|" LINE  21 COL 46.
    05  ZeilennummerIn10 LINE 22 COL 1 PIC X(7) FROM Zeilennummer10.
    *>05  VALUE "10 |"                 LINE  22 COL 1.
    05  VALUE "|"                    LINE  22 COL 30.
    05  VALUE "|"                    LINE  22 COL 55.
    05  VALUE "|"                    LINE  22 COL 80.
    05  VALUE "|"                    LINE  22 COL 105.
    05  VALUE "X"                    LINE  30 COL 120.
    05  VALUE "|______________________|_______________" LINE  23 COL 7.
    05  VALUE "_________|________________________|________________________|" LINE  23 COL 46.
    05  Textinput2                               LINE  4 COL 10 PIC X(5).
    05   ID-INPUT LINE  4 COL 9  PIC  X(20)    TO Spalte1(1).
    05   ID-INPUT2 LINE  4 COL 34 PIC  X(20)    TO Spalte2(1).
    05   ID-INPUT3 LINE  4 COL 58 PIC  X(20)    TO Spalte3(1).
    05   ID-INPUT4 LINE  4 COL 82 PIC  X(20)    TO Spalte4(1).
    05   ID-INPUT5 LINE  6 COL 9  PIC  X(20)    TO Spalte1(2).
    05   ID-INPUT6 LINE  6 COL 34 PIC  X(20)    TO Spalte2(2).
    05   ID-INPUT7 LINE  6 COL 58 PIC  X(20)    TO Spalte3(2).
    05   ID-INPUT8 LINE  6 COL 82 PIC  X(20)    TO Spalte4(2).
    05   ID-INPUT9 LINE  8 COL 9  PIC  X(20)    TO Spalte1(3).
    05   ID-INPUT10 LINE  8 COL 34 PIC  X(20)    TO Spalte2(3).
    05   ID-INPUT11 LINE  8 COL 58 PIC  X(20)    TO Spalte3(3).
    05   ID-INPUT12 LINE  8 COL 82 PIC  X(20)    TO Spalte4(3).
    05   ID-INPUT13 LINE  10 COL 9 PIC  X(20)    TO Spalte1(4).
    05   ID-INPUT14 LINE  10 COL 34 PIC  X(20)    TO Spalte2(4).
    05   ID-INPUT15 LINE  10 COL 58 PIC  X(20)    TO Spalte3(4).
    05   ID-INPUT16 LINE  10 COL 82 PIC  X(20)    TO Spalte4(4).
    05   ID-INPUT17 LINE  12 COL 9 PIC  X(20)    TO Spalte1(5).
    05   ID-INPUT18 LINE  12 COL 34 PIC  X(20)    TO Spalte2(5).
    05   ID-INPUT19 LINE  12 COL 58 PIC  X(20)    TO Spalte3(5).
    05   ID-INPUT20 LINE  12 COL 82 PIC  X(20)    TO Spalte4(5).
    05   ID-INPUT21 LINE  14 COL 9 PIC  X(20)    TO Spalte1(6).
    05   ID-INPUT22 LINE  14 COL 34 PIC  X(20)    TO Spalte2(6).
    05   ID-INPUT23 LINE  14 COL 58 PIC  X(20)    TO Spalte3(6).
    05   ID-INPUT24 LINE  14 COL 82 PIC  X(20)    TO Spalte4(6).
    05   ID-INPUT25 LINE  16 COL 9 PIC  X(20)    TO Spalte1(7).
    05   ID-INPUT26 LINE  16 COL 34 PIC  X(20)    TO Spalte2(7).
    05   ID-INPUT27 LINE  16 COL 58 PIC  X(20)    TO Spalte3(7).
    05   ID-INPUT28 LINE  16 COL 82 PIC  X(20)    TO Spalte4(7).
    05   ID-INPUT29 LINE  18 COL 9 PIC  X(20)    TO Spalte1(8).
    05   ID-INPUT30 LINE  18 COL 34 PIC  X(20)    TO Spalte2(8).
    05   ID-INPUT31 LINE  18 COL 58 PIC  X(20)    TO Spalte3(8).
    05   ID-INPUT32 LINE  18 COL 82 PIC  X(20)    TO Spalte4(8).
    05   ID-INPUT33 LINE  20 COL 9 PIC  X(20)    TO Spalte1(9).
    05   ID-INPUT34 LINE  20 COL 34 PIC  X(20)    TO Spalte2(9).
    05   ID-INPUT35 LINE  20 COL 58 PIC  X(20)    TO Spalte3(9).
    05   ID-INPUT36 LINE  20 COL 82 PIC  X(20)    TO Spalte4(9).
    05   ID-INPUT37 LINE  22 COL 9 PIC  X(20)    TO Spalte1(10).
    05   ID-INPUT38 LINE  22 COL 34 PIC  X(20)    TO Spalte2(10).
    05   ID-INPUT39 LINE  22 COL 58 PIC  X(20)    TO Spalte3(10).
    05   ID-INPUT40 LINE  22 COL 82 PIC  X(20)    TO Spalte4(10).
    05  VALUE "F - Functionview"                    LINE  25 COL 30.
    05  VALUE "V - Valueview"                        LINE  26 COL 30.
    05  VALUE "Change View"                     LINE  27 COL 30.
    05  RESPONSE-INPUT2                             LINE  27 COL 45
    PIC X         TO Viewspec.
    PROCEDURE DIVISION.
    MAIN-PROCEDURE.
    OPEN OUTPUT Outputfile.
    MOVE "C2:D3" TO Extstr.
    PERFORM Inspectprog.
    DISPLAY Cellspecscreen.
    ACCEPT Cellspecscreen.
    *>Definiert Zeilen- und Spaltennamen
    PERFORM Spreadviewupdate.
    *>INPUT Inputfile.
    *>Eingabe ob Datei erstellt oder Datei gelesen wird
    DISPLAY Progstartcscreen.
    *>Entgegennehmen der Eingabe
    ACCEPT Progstartcscreen.
    *>Eingabe wie viele Zeilen und Spalten in der neuen Datei sein sollen
    DISPLAY Tablespecscreen.
    *>Datei welche gelesen wird, wenn eine neue Datei erstellt wird
    PERFORM Inputfileread.
    *>Ausgabe von Inhalt vorbereiten.
    PERFORM Tablecontentoutputalt.
    *>Entgegennahme von Zeilen- und Spaltenzahl
    ACCEPT Tablespecscreen.
    MOVE Rowcount TO Rowcountres2.
    MOVE Colcount TO Colcountres.
    *>Berechnung der weiteren Zeilenzahl
    SUBTRACT 4 FROM Colcountres GIVING Colcountadd.
    DISPLAY Tablespecresultscreen.
    *>Ende der Anzeige
    ACCEPT Tablespecresultscreen.
    *>Programm um Funktionen einzugeben mit grafischer Eingabe
    PERFORM Screendisplayprog 5 TIMES.
    DISPLAY DATA-ENTRY-SCREEN.
    ACCEPT DATA-ENTRY-SCREEN.
    PERFORM Celladressinfo.
    *>READ Inputfile INTO Cobolstatement.
    *>PERFORM Performprog.
    *>CLOSE Inputfile.
    CLOSE Outputfile.
    MOVE 4 TO Num.
    MOVE 2 TO Num2.
    DIVIDE Num BY 2 GIVING Num.
    DISPLAY Num.
    PERFORM Odsfilewriteprog.
    STOP RUN.
    Celladressinfo.
    DISPLAY Celladressoutput.
    ACCEPT Celladressoutput.
    MOVE Inputoption3 TO Extstr(1:4).
    MOVE Extstr TO Substrinput.
    PERFORM Inspectprog.
    MOVE Cellposalt2 TO Inspectres.
    MOVE 0 TO Cellposalt2.
    DISPLAY Celladressoutput.
    ACCEPT Celladressoutput.
    Odsfilewriteprog.
    OPEN INPUT Fodstemplate.
    OPEN OUTPUT Fodsoutput.
    *>Programm aus welchem die Dateivorlage kommt.
    PERFORM Fodssubread 299 TIMES.
    STRING "<text:p>" Cellcontent(1)(1:4) "</text:p>" INTO Fodstemplatetext.
    WRITE Record-Name6 FROM Fodstemplatetext.
    MOVE " " TO Fodstemplatetext.
    READ Fodstemplate.
    *>Programm aus welchem die Dateivorlage kommt.
    PERFORM Fodssubread 44 TIMES.
    CLOSE Fodstemplate.
    CLOSE Fodsoutput.
    Fodssubread.
    READ Fodstemplate INTO Fodstemplatetext.
    WRITE Record-Name6 FROM Fodstemplatetext.
    MOVE " " TO Fodstemplatetext.
    Spreadviewupdate.
    MOVE "A  " TO Spaltenspec1.
    MOVE "B  " TO Spaltenspec2.
    MOVE "C  " TO Spaltenspec3.
    MOVE "D  " TO Spaltenspec4.
    MOVE "1     |" TO Zeilennummer1.
    MOVE "2     |" TO Zeilennummer2.
    MOVE "3     |" TO Zeilennummer3.
    MOVE "4     |" TO Zeilennummer4.
    MOVE "5     |" TO Zeilennummer5.
    MOVE "6     |" TO Zeilennummer6.
    MOVE "7     |" TO Zeilennummer7.
    MOVE "8     |" TO Zeilennummer8.
    MOVE "9     |" TO Zeilennummer9.
    MOVE "10    |" TO Zeilennummer10.
    Screendisplayprog.
    *>Anzeige des aktuellen Tabelleninhalt, nach Setup.
    DISPLAY DATA-ENTRY-SCREEN.
    *>Entgegennahme der Eingabe
    ACCEPT DATA-ENTRY-SCREEN.
    *>Programm zum neuen Inhalt in Tabelle verschieben, beginnend bei Cellcontent1
    PERFORM Tablecontentinputalt.
    *>Adresse die eine Funktion beinhaltet als Funktioneingabe
    MOVE 1 TO Relrow.
    MOVE 1 TO Relcol.
    MOVE 1 TO Scriptcount.
    *>Programm um Funktion zu finden
    PERFORM Cobolstatementeval.
    *>MOVE Spalte1(1) TO Cobolstatement.
    PERFORM Performprog.
    PERFORM Tablecontentoutput.
    IF Viewspec = "x" THEN PERFORM Cellformatprog.
    MOVE " " TO Viewspec.
    MOVE " " TO RESPONSE-INPUT2.
    Cellformatprog.
    PERFORM Celladressinfo.
    Inputfileread.
    OPEN INPUT Inputfile.
    MOVE 1 TO Cellcontentnum.
    PERFORM Subperformprog 100 TIMES.
    CLOSE Inputfile.
    Subperformprog.
    *>Einlesen von Inhalt aus Datei in die Tabelle Cellcontent
    *>in die aktuell relevante Zeile
    READ Inputfile INTO Cellcontent(Cellcontentnum).
    *>IF Cellcontentnum = 1 THEN MOVE "=TEIL(""Test"";2;1)" TO Cobolstatement.
    ADD 1 TO Cellcontentnum.
    *>DISPLAY Cobolstatement.
    *>IF Cellcontentnum = 2 THEN MOVE " " TO Cobolstatement.
    PERFORM Performprog.
    Performprog.
    *>Programm um zu testen, wie mehrere Ausgaben ausgegeben werden koennen.
    MOVE 1 TO Num.
    MOVE 1 TO Posnum.
    MOVE " " TO Coboltemplate.
    *>Programm zum Festlegen von Tabellenkalkulationssyntax
    PERFORM Cobolwordtableinit.
    *>MOVE "MOVE ( ( ""Das ist ein kurzer Text."" TO substr text." TO Cobolstatement.
    *>MOVE "DISPLAY ""Das ist ein kurzer Text""." TO Cobolstatement.
    *>Programm zum Aufteilen in Bestandteile getrennt mit Leer.
    PERFORM Subextendprog UNTIL Num2 = 4.
    MOVE " " TO Substr.
    MOVE 1 TO Num.
    MOVE 1 TO Posnum.
    MOVE 0 TO Num2.
    MOVE 1 TO Tablerow.
    MOVE 0 TO Templatecount.
    MOVE 2 TO Quotcount.
    PERFORM Charextractprog UNTIL Num2 = 9999.
    SET Indexnum2 TO 1.
    *>DISPLAY Coboltemplate.
    WRITE Record-Name4 FROM Coboltemplate.
    PERFORM Subprog.
    MOVE " " TO Cobolstatement.
    MOVE " " TO Cobolstatementout.
    MOVE " " TO Subtext2(1).
    MOVE " " TO Subtext2(2).
    MOVE " " TO Subtext2(3).
    MOVE " " TO Subtext2(4).
    MOVE " " TO Subtext2(5).
    MOVE " " TO Subtext2(6).
    MOVE " " TO Subtext2(7).
    MOVE " " TO Subtext2(8).
    MOVE " " TO Subtext2(9).
    MOVE " " TO Subtext2(10).
    MOVE " " TO Subtext2(11).
    MOVE " " TO Subtext2(12).
    MOVE " " TO Subtext2(13).
    MOVE " " TO Subtext2(14).
    MOVE " " TO Subtext2(15).
    MOVE " " TO Subtext2(16).
    MOVE " " TO Catword(1).
    MOVE " " TO Catword(2).
    MOVE " " TO Catword(3).
    MOVE " " TO Catword(4).
    MOVE " " TO Catword(5).
    MOVE " " TO Catword(6).
    MOVE " " TO Catword(7).
    MOVE " " TO Catword(8).
    MOVE " " TO Catword(9).
    MOVE " " TO Catword(10).
    MOVE " " TO Catword(11).
    MOVE " " TO Catword(12).
    MOVE " " TO Catword(13).
    MOVE " " TO Catword(14).
    MOVE " " TO Catword(15).
    MOVE " " TO Catword(16).
    MOVE " " TO Catword(17).
    MOVE " " TO Catword(18).
    MOVE " " TO Catword(19).
    MOVE " " TO Catword(20).
    MOVE " " TO Catword(21).
    MOVE " " TO Catword(22).
    MOVE " " TO Catword(23).
    MOVE " " TO Catword(24).
    MOVE " " TO Catword(25).
    *>DISPLAY Subtext2(4).
    *>DISPLAY Charcount2(4).
    *>PERFORM Subprog.
    *>DISPLAY Scriptoutput.
    Subextendprog.
    *>Programm zum Aufteilen in Bestandteile
    MOVE Cobolstatement(Num:1) TO Substr.
    IF Substr = """" THEN ADD 1 TO Quotcount.
    IF Quotcount > 0 THEN DIVIDE Quotcount BY 2 GIVING Quotresult REMAINDER Quotrest.
    IF Substr = "(" OR Substr = ")" OR Substr = ":" OR Substr = "." OR Substr = "="
    OR Substr = "<" OR Substr = ">" OR Substr = ";" OR Substr = "+" OR Substr = "-"
    OR Substr = "/" OR Substr = "*" OR Substr = "^" OR Substr = "&"
    THEN MOVE 3 TO Num2 ELSE MOVE 1 TO Num2
    END-IF.
    IF Quotrest = 1 THEN MOVE 1 TO Num2.
    IF Num2 = 3 THEN STRING " " Substr(1:1) " " INTO Relstr ELSE MOVE Substr(1:1) TO Relstr.
    MOVE Relstr(1:Num2) TO Cobolstatementout(Posnum:Num2).
    ADD Num2 TO Posnum.
    ADD 1 TO Num.
    IF Num = 200 THEN MOVE 4 TO Num2.
    *>IF Substr = "." AND Num2 = 1 THEN MOVE 4 TO Num2.
    Charextractprog.
    IF Num = 1 THEN MOVE 1 TO Num2.
    MOVE Cobolstatementout(Num:1) TO Substr.
    IF Num > 1 AND Quotrest = 0 THEN MOVE Substr TO Presubstr.
    IF Substr = """" THEN ADD 1 TO Quotcount.
    IF Quotcount > 0 THEN DIVIDE Quotcount BY 2 GIVING Quotresult REMAINDER Quotrest.
    MOVE Quotrest TO Prequotrest.
    ADD 1 TO Num.
    MOVE Num2 TO Charcount.
    IF Charcount > 1 THEN SUBTRACT 1 FROM Charcount ELSE MOVE 1 TO Charcount.
    IF Quotrest = 0 AND Substr = " " THEN MOVE 1 TO Num2.
    IF Substr NOT EQUAL TO " " OR Quotrest = 1 THEN ADD 1 TO Num2.
    MOVE Substr(1:1) TO Subtext(Num2:1).
    *>IF Presubstr = "." AND Quotrest = 0 THEN DISPLAY "Sub: " Charcount Subtext.
    *>IF Quotrest = 0 AND Substr = " " AND Subtext NOT EQUAL TO " " THEN DISPLAY "Sub: " Charcount Subtext.
    IF Quotrest = 0 AND Substr = " " AND Subtext NOT EQUAL TO " " THEN MOVE Subtext(2:Charcount) TO Subtextalt.
    IF Quotrest = 0 AND Substr = " " AND Subtext NOT EQUAL TO " " THEN MOVE Subtextalt To Subtext.
    IF Quotrest = 0 AND Substr = " " AND Subtext NOT EQUAL TO " " THEN MOVE Subtext TO Subtext2(Tablerow).
    IF Quotrest = 0 AND Substr = " " AND Subtext NOT EQUAL TO " " THEN MOVE Charcount TO Charcount2(Tablerow).
    IF Quotrest = 0 AND Substr = " " AND Subtext NOT EQUAL TO " " THEN PERFORM Wordsearch.
    IF Quotrest = 0 AND Substr = " " AND Subtext NOT EQUAL TO " " THEN ADD 1 TO Tablerow.
    IF Quotrest = 0 AND Substr = " " THEN MOVE " " TO Subtext.
    IF Num = 200 THEN MOVE 9999 TO Num2.
    Wordsearch.
    *>Programm, welches eine Vorlage generiert
    SET Indexnum TO 1.
    MOVE 0 TO Stopnum.
    *>MOVE " " TO Catword(1).
    *>DISPLAY "Searched for: " Subtext.
    PERFORM Subsearch UNTIL Stopnum = 1.
    IF Catword(Tablerow) = "x" THEN
    MOVE Subtext(1:Charcount) TO Coboltemplate(Templatecount + 1:Charcount)
    ELSE MOVE "_" TO Coboltemplate(Templatecount + 1:1)
    END-IF.
    *>DISPLAY "Cat: " Catword(Tablerow).
    *>IF Subtext(1:1) = """" THEN SUBTRACT 2 FROM Charcount.
    *>IF Subtext(1:1) = """" THEN STRING " " Subtext(3:Charcount) "   " INTO Subtext.
    IF Catword(Tablerow) = "x" THEN ADD Charcount TO Templatecount ELSE ADD 1 TO Templatecount.
    Subsearch.
    SET Indexnum UP BY 1.
    SEARCH Wordlist2 AT END MOVE 1 TO Stopnum WHEN Cobolword(Indexnum) = Subtext MOVE "x" TO Catword(Tablerow).
    Subprog.
    *>Programm zum Unterprogramm finden, aus Vorlagen.
    DISPLAY Coboltemplate.
    IF Coboltemplate = "_TEIL(_;_;_)" THEN PERFORM Substrprog.
    IF Coboltemplate = "_LAENGE(_)" THEN PERFORM Lenoutputprog.
    IF Coboltemplate = "_SUMME(_:_)" THEN PERFORM Sumprog.
    IF Coboltemplate = "_GLAETTEN(_)" THEN PERFORM Trimprog.
    IF Coboltemplate = "__" THEN PERFORM Cellreferenceprog.
    IF Coboltemplate = "__+_" THEN PERFORM Sumspecialprog.
    IF Coboltemplate = "__-_" THEN PERFORM Subtractspecialprog.
    IF Coboltemplate = "__*_" THEN PERFORM Multiplyspecialprog.
    IF Coboltemplate = "__/_" THEN PERFORM Dividespecialprog.
    IF Coboltemplate = "_REST(_;_)" THEN PERFORM Restspecialprog.
    IF Coboltemplate = "__&_" THEN PERFORM Concatprog.
    *>IF Coboltemplate NOT EQUAL TO "_TEIL(_;_;_)" OR "_LAENGE(_)" THEN MOVE Coboltemplate TO Cellcontent(Scriptpos).
    *>MOVE Coboltemplate TO Cellcontent(3).
    Cellreferenceprog.
    MOVE 1 TO Posnum.
    MOVE 1 TO Num2.
    MOVE 1 TO Lengthcalcnum.
    MOVE 0 TO Lengthcalcres.
    MOVE " " TO Scriptbase.
    MOVE Subtext2(2) TO Extstr.
    MOVE Subtext2(2) TO Substrinput.
    IF Substrinput(1:1) = """" THEN STRING Substrinput(2:Charcount2(4) - 2) "  " INTO Substrinput(1:Charcount2(4))
    ELSE PERFORM Inspectprog
    END-IF.
    MOVE Substrinput TO Cellcontent(Scriptpos).
    Concatprog.
    MOVE 1 TO Posnum.
    MOVE 1 TO Num2.
    MOVE 1 TO Lengthcalcnum.
    MOVE 0 TO Lengthcalcres.
    MOVE " " TO Scriptbase.
    MOVE Subtext2(2) TO Extstr.
    MOVE Subtext2(2) TO Substrinput.
    IF Substrinput(1:1) = """" THEN STRING Substrinput(2:Charcount2(4) - 2) "  " INTO Substrinput(1:Charcount2(4))
    ELSE PERFORM Inspectprog
    END-IF.
    MOVE Substrinput TO Concatbase.
    PERFORM Lengthcalc 200 TIMES.
    MOVE Lengthcalcres TO Prelengthcalcres.
    ADD 1 TO Prelengthcalcres.
    MOVE Concatbase(1:Lengthcalcres) TO Substrconcat(1:Lengthcalcres).
    MOVE " " TO Substrinput.
    MOVE " " TO Concatbase.
    MOVE 0 TO Lengthcalcres.
    MOVE Subtext2(4) TO Extstr.
    MOVE Subtext2(4) TO Substrinput.
    IF Substrinput(1:1) = """" THEN STRING Substrinput(2:Charcount2(4) - 2) "  " INTO Substrinput(1:Charcount2(4))
    ELSE PERFORM Inspectprog
    END-IF.
    MOVE Substrinput TO Concatbase.
    MOVE 1 TO Lengthcalcnum.
    PERFORM Lengthcalc 200 TIMES.
    MOVE Concatbase(1:Lengthcalcres) TO Substrconcat(Prelengthcalcres:Lengthcalcres).
    MOVE Substrconcat TO Cellcontent(Scriptpos).
    MOVE " " TO Concatbase.
    MOVE 1 TO Lengthcalcnum.
    Lenoutputprog.
    MOVE 1 TO Posnum.
    MOVE 1 TO Num2.
    MOVE 1 TO Lengthcalcnum.
    MOVE 0 TO Lengthcalcres.
    MOVE " " TO Scriptbase.
    MOVE Subtext2(4) TO Extstr.
    MOVE Subtext2(4) TO Substrinput.
    IF Substrinput(1:1) = """" THEN STRING Substrinput(2:Charcount2(4) - 2) "  " INTO Substrinput(1:Charcount2(4))
    ELSE PERFORM Inspectprog
    END-IF.
    PERFORM Lengthcalc 200 TIMES.
    MOVE Lengthcalcres TO Scriptoutput(1:4).
    MOVE Lengthcalcres TO Cellcontent(Scriptpos).
    Substrprog.
    MOVE 1 TO Posnum.
    MOVE 1 TO Num2.
    MOVE " " TO Scriptbase.
    MOVE " " TO Substrinput.
    MOVE Subtext2(4) TO Extstr.
    MOVE Subtext2(4) TO Substrinput.
    MOVE " " TO Substraltstr.
    IF Substrinput(1:1) = """" THEN STRING Substrinput(2:Charcount2(4) - 2) "  " INTO Substrinput(1:Charcount2(4))
    ELSE PERFORM Inspectprog
    END-IF.
    MOVE Substrinput TO Substraltstr.
    MOVE " " TO Substrinput.
    MOVE " " TO Extstr.
    MOVE Subtext2(6) TO Extstr.
    MOVE Subtext2(6) TO Substrinput.
    PERFORM Inspectprog.
    MOVE Substrinput TO Substrstart.
    MOVE " " TO Substrinput.
    MOVE " " TO Extstr.
    MOVE Subtext2(8) TO Extstr.
    MOVE Subtext2(8) TO Substrinput.
    PERFORM Inspectprog.
    MOVE Substrinput TO Substrcount.
    MOVE Substraltstr(Substrstart:Substrcount) TO Scriptoutput.
    MOVE Scriptoutput TO Cellcontent(Scriptpos).
    DISPLAY "Substroutput: " Scriptoutput.
    MOVE " " TO Scriptoutput.
    Subtractspecialprog.
    *>Summiert zwei Werte
    MOVE 1 TO Posnum.
    MOVE 1 TO Rangecontentpos.
    MOVE 1 TO Num2.
    MOVE 1 TO Lengthcalcnum.
    MOVE 0 TO Lengthcalcres.
    MOVE 1 TO Cellrangepos.
    MOVE 0 TO Sumres.
    MOVE 0 TO Sumpart1.
    MOVE 0 TO Sumpart2.
    MOVE " " TO Scriptbase.
    MOVE Subtext2(2) TO Extstr.
    MOVE Subtext2(2) TO Substrinput.
    IF Substrinput(1:1) = """" THEN DISPLAY "NoValue"
    ELSE PERFORM Inspectprog.
    MOVE Substrinput TO Sumpart1.
    MOVE Subtext2(4) TO Extstr.
    MOVE Subtext2(4) TO Substrinput.
    IF Substrinput(1:1) = """" THEN DISPLAY "NoValue"
    ELSE PERFORM Inspectprog
    END-IF.
    MOVE Substrinput TO Sumpart2.
    SUBTRACT Sumpart2 FROM Sumpart1 GIVING Sumres.
    MOVE Sumres TO Cellcontent(Scriptpos).
    Multiplyspecialprog.
    *>Summiert zwei Werte
    MOVE 1 TO Posnum.
    MOVE 1 TO Rangecontentpos.
    MOVE 1 TO Num2.
    MOVE 1 TO Lengthcalcnum.
    MOVE 0 TO Lengthcalcres.
    MOVE 1 TO Cellrangepos.
    MOVE 0 TO Sumres.
    MOVE 0 TO Sumpart1.
    MOVE 0 TO Sumpart2.
    MOVE " " TO Scriptbase.
    MOVE Subtext2(2) TO Extstr.
    MOVE Subtext2(2) TO Substrinput.
    IF Substrinput(1:1) = """" THEN DISPLAY "NoValue"
    ELSE PERFORM Inspectprog.
    MOVE Substrinput TO Sumpart1.
    MOVE Subtext2(4) TO Extstr.
    MOVE Subtext2(4) TO Substrinput.
    IF Substrinput(1:1) = """" THEN DISPLAY "NoValue"
    ELSE PERFORM Inspectprog
    END-IF.
    MOVE Substrinput TO Sumpart2.
    MULTIPLY Sumpart1 BY Sumpart2 GIVING Sumres.
    MOVE Sumres TO Cellcontent(Scriptpos).
    Dividespecialprog.
    *>Teilt einen wert durch einen anderen
    MOVE 1 TO Posnum.
    MOVE 1 TO Rangecontentpos.
    MOVE 1 TO Num2.
    MOVE 1 TO Lengthcalcnum.
    MOVE 0 TO Lengthcalcres.
    MOVE 1 TO Cellrangepos.
    MOVE 0 TO Sumres.
    MOVE 0 TO Sumpart1.
    MOVE 0 TO Sumpart2.
    MOVE " " TO Scriptbase.
    MOVE Subtext2(2) TO Extstr.
    MOVE Subtext2(2) TO Substrinput.
    IF Substrinput(1:1) = """" THEN DISPLAY "NoValue"
    ELSE PERFORM Inspectprog.
    MOVE Substrinput TO Sumpart1.
    MOVE Subtext2(4) TO Extstr.
    MOVE Subtext2(4) TO Substrinput.
    IF Substrinput(1:1) = """" THEN DISPLAY "NoValue"
    ELSE PERFORM Inspectprog
    END-IF.
    MOVE Substrinput TO Sumpart2.
    DIVIDE Sumpart1 BY Sumpart2 GIVING Sumres.
    MOVE Sumres TO Cellcontent(Scriptpos).
    Restspecialprog.
    *>Teilt einen wert durch einen anderen
    MOVE 1 TO Posnum.
    MOVE 1 TO Rangecontentpos.
    MOVE 1 TO Num2.
    MOVE 1 TO Lengthcalcnum.
    MOVE 0 TO Lengthcalcres.
    MOVE 1 TO Cellrangepos.
    MOVE 0 TO Sumres.
    MOVE 0 TO Sumpart1.
    MOVE 0 TO Sumpart2.
    MOVE 0 TO Restres.
    MOVE 0 TO Restdiff.
    MOVE " " TO Scriptbase.
    MOVE Subtext2(4) TO Extstr.
    MOVE Subtext2(4) TO Substrinput.
    IF Substrinput(1:1) = """" THEN DISPLAY "NoValue"
    ELSE PERFORM Inspectprog.
    MOVE Substrinput TO Sumpart1.
    MOVE Subtext2(6) TO Extstr.
    MOVE Subtext2(6) TO Substrinput.
    IF Substrinput(1:1) = """" THEN DISPLAY "NoValue"
    ELSE PERFORM Inspectprog
    END-IF.
    MOVE Substrinput TO Sumpart2.
    DIVIDE Sumpart1 BY Sumpart2 GIVING Sumres.
    MULTIPLY Sumres BY Sumpart2 GIVING Restdiff.
    SUBTRACT Restdiff FROM Sumpart1 GIVING Restres.
    MOVE Restres TO Cellcontent(Scriptpos).
    Sumspecialprog.
    *>Summiert zwei Werte
    MOVE 1 TO Posnum.
    MOVE 1 TO Rangecontentpos.
    MOVE 1 TO Num2.
    MOVE 1 TO Lengthcalcnum.
    MOVE 0 TO Lengthcalcres.
    MOVE 1 TO Cellrangepos.
    MOVE 0 TO Sumres.
    MOVE 0 TO Sumpart1.
    MOVE 0 TO Sumpart2.
    MOVE " " TO Scriptbase.
    MOVE Subtext2(2) TO Extstr.
    MOVE Subtext2(2) TO Substrinput.
    IF Substrinput(1:1) = """" THEN DISPLAY "NoValue"
    ELSE PERFORM Inspectprog.
    MOVE Substrinput TO Sumpart1.
    MOVE Subtext2(4) TO Extstr.
    MOVE Subtext2(4) TO Substrinput.
    IF Substrinput(1:1) = """" THEN DISPLAY "NoValue"
    ELSE PERFORM Inspectprog
    END-IF.
    MOVE Substrinput TO Sumpart2.
    ADD Sumpart2 TO Sumpart1 GIVING Sumres.
    MOVE Sumres TO Cellcontent(Scriptpos).
    Sumprog.
    MOVE 1 TO Posnum.
    MOVE 1 TO Rangecontentpos.
    MOVE 1 TO Num2.
    MOVE 1 TO Lengthcalcnum.
    MOVE 0 TO Lengthcalcres.
    MOVE 1 TO Cellrangepos.
    MOVE 0 TO Sumres.
    MOVE " " TO Scriptbase.
    MOVE 0 TO Sumpart1.
    MOVE 0 TO Sumpart2.
    MOVE Subtext2(4) TO Extstr.
    MOVE Subtext2(4) TO Substrinput.
    IF Substrinput(1:1) = """" THEN DISPLAY "NoValue"
    ELSE PERFORM Inspectprog.
    MOVE Cellposalt2 TO Cellposalt3.
    MOVE Substrinput TO Sumpart1.
    MOVE Subtext2(6) TO Extstr.
    MOVE Subtext2(6) TO Substrinput.
    IF Substrinput(1:1) = """" THEN DISPLAY "NoValue"
    ELSE PERFORM Inspectprog
    END-IF.
    MOVE Substrinput TO Sumpart2.
    *>MOVE 2 TO Celladressres.
    MOVE Cellposalt3 TO Celladressres.
    PERFORM Colrelcalc.
    ADD 1 TO Cellposalt2.
    SUBTRACT Cellposalt3 FROM Cellposalt2 GIVING Cellposaltdiff.
    *>ADD 1 TO Cellposaltdiff.
    *>PERFORM Cellrangemovealt UNTIL Celladressres = Cellposalt2.
    PERFORM Cellrangemovealt Cellposaltdiff TIMES.
    MOVE 1 TO Rangecontentpos.
    *>MOVE Cellrange(1) TO Sumpart1.
    *>MOVE Cellrange(2) TO Sumpart2.
    PERFORM Sumsubprog Cellposaltdiff TIMES.
    *>MOVE Firstcol TO Sumres.
    MOVE Sumres TO Cellcontent(Scriptpos).
    Sumsubprog.
    MOVE Cellrange(Rangecontentpos) TO Sumpart1.
    ADD Sumpart1 TO Sumres.
    ADD 1 TO Rangecontentpos.
    Colrelcalc.
    *>Programm zum Berechnen der relevanten Spalten.
    DIVIDE Cellposalt3 BY 4 GIVING Coldiv REMAINDER Colrest.
    IF Colrest = 0 THEN MOVE 4 TO Colrest.
    MOVE Colrest TO Firstcol.
    MOVE 0 TO Colrest.
    DIVIDE Cellposalt2 BY 4 GIVING Coldiv REMAINDER Colrest.
    IF Colrest = 0 THEN MOVE 4 TO Colrest.
    MOVE Colrest TO Lastcol.
    MOVE Firstcol TO Currentcol.
    SUBTRACT 1 FROM Firstcol.
    ADD 1 TO Lastcol.
    Cobolstatementeval.
    *>Lange Funktionen können direkt eingegeben werden.
    *>Kann später hilfreich sein
    *>MOVE "=TEIL(""Testinputinhaltextraktvalue"";16;6)" TO Spalte1(1).
    *>Hier wird die Zahl der Zeilen angegeben
    PERFORM Cobolstatementevalsub 10 TIMES.
    Cobolstatementevalsub.
    MOVE 1 TO Relcol.
    MOVE Spalte1(Relrow)(1:1) TO Firstchar.
    IF Firstchar = "=" AND Relcol = 1 THEN MOVE Spalte1(Relrow) TO Cobolstatement.
    IF Firstchar EQUAL TO "=" THEN MOVE Scriptcount TO Scriptpos.
    MOVE " " TO Firstchar.
    ADD 1 TO Scriptcount.
    MOVE 2 TO Relcol.
    IF Relcol = 2 THEN MOVE Spalte2(Relrow)(1:1) TO Firstchar.
    IF Firstchar = "=" AND Relcol = 2 THEN MOVE Spalte2(Relrow) TO Cobolstatement.
    IF Firstchar EQUAL TO "=" THEN MOVE Scriptcount TO Scriptpos.
    MOVE " " TO Firstchar.
    ADD 1 TO Scriptcount.
    MOVE 3 TO Relcol.
    IF Relcol = 3 THEN MOVE Spalte3(Relrow)(1:1) TO Firstchar.
    IF Firstchar = "=" AND Relcol = 3 THEN MOVE Spalte3(Relrow) TO Cobolstatement.
    IF Firstchar EQUAL TO "=" THEN MOVE Scriptcount TO Scriptpos.
    MOVE " " TO Firstchar.
    ADD 1 TO Scriptcount.
    MOVE 4 TO Relcol.
    IF Relcol = 4 THEN MOVE Spalte4(Relrow)(1:1) TO Firstchar.
    IF Firstchar = "=" AND Relcol = 4 THEN MOVE Spalte4(Relrow) TO Cobolstatement.
    IF Firstchar EQUAL TO "=" THEN MOVE Scriptcount TO Scriptpos.
    MOVE " " TO Firstchar.
    ADD 1 TO Scriptcount.
    ADD 1 TO Relrow.
    Tablecontentinput.
    MOVE Spalte1(1) TO Cellcontent(1).
    MOVE Spalte2(1) TO Cellcontent(2).
    MOVE Spalte3(1) TO Cellcontent(3).
    MOVE Spalte4(1) TO Cellcontent(4).
    MOVE Spalte1(2) TO Cellcontent(5).
    MOVE Spalte1(2) TO Cellcontent(5).
    MOVE Spalte2(2) TO Cellcontent(6).
    MOVE Spalte3(2) TO Cellcontent(7).
    MOVE Spalte4(2) TO Cellcontent(8).
    MOVE Spalte1(3) TO Cellcontent(9).
    MOVE Spalte2(3) TO Cellcontent(10).
    MOVE Spalte3(3) TO Cellcontent(11).
    MOVE Spalte4(3) TO Cellcontent(12).
    MOVE Spalte1(4) TO Cellcontent(13).
    MOVE Spalte2(4) TO Cellcontent(14).
    MOVE Spalte3(4) TO Cellcontent(15).
    MOVE Spalte4(4) TO Cellcontent(16).
    MOVE Spalte1(5) TO Cellcontent(17).
    MOVE Spalte2(5) TO Cellcontent(18).
    MOVE Spalte3(5) TO Cellcontent(19).
    MOVE Spalte4(5) TO Cellcontent(20).
    MOVE Spalte1(6) TO Cellcontent(21).
    MOVE Spalte2(6) TO Cellcontent(22).
    MOVE Spalte3(6) TO Cellcontent(23).
    MOVE Spalte4(6) TO Cellcontent(24).
    MOVE Spalte1(7) TO Cellcontent(25).
    MOVE Spalte2(7) TO Cellcontent(26).
    MOVE Spalte3(7) TO Cellcontent(27).
    MOVE Spalte4(7) TO Cellcontent(28).
    MOVE Spalte1(8) TO Cellcontent(29).
    MOVE Spalte2(8) TO Cellcontent(30).
    MOVE Spalte3(8) TO Cellcontent(31).
    MOVE Spalte4(8) TO Cellcontent(32).
    MOVE Spalte1(9) TO Cellcontent(33).
    MOVE Spalte2(9) TO Cellcontent(34).
    MOVE Spalte3(9) TO Cellcontent(35).
    MOVE Spalte4(9) TO Cellcontent(36).
    MOVE Spalte1(10) TO Cellcontent(37).
    MOVE Spalte2(10) TO Cellcontent(38).
    MOVE Spalte3(10) TO Cellcontent(39).
    MOVE Spalte4(10) TO Cellcontent(40).
    Tablecontentoutput.
    MOVE Cellcontent(1) TO ID-INPUT.
    MOVE Cellcontent(2) TO ID-INPUT2.
    MOVE Cellcontent(3) TO ID-INPUT3.
    MOVE Cellcontent(4) TO ID-INPUT4.
    MOVE Cellcontent(5) TO ID-INPUT5.
    MOVE Cellcontent(6) TO ID-INPUT6.
    MOVE Cellcontent(7) TO ID-INPUT7.
    MOVE Cellcontent(8) TO ID-INPUT8.
    MOVE Cellcontent(9) TO ID-INPUT9.
    MOVE Cellcontent(10) TO ID-INPUT10.
    MOVE Cellcontent(11) TO ID-INPUT11.
    MOVE Cellcontent(12) TO ID-INPUT12.
    MOVE Cellcontent(13) TO ID-INPUT13.
    MOVE Cellcontent(14) TO ID-INPUT14.
    MOVE Cellcontent(15) TO ID-INPUT15.
    MOVE Cellcontent(16) TO ID-INPUT16.
    MOVE Cellcontent(17) TO ID-INPUT17.
    MOVE Cellcontent(18) TO ID-INPUT18.
    MOVE Cellcontent(19) TO ID-INPUT19.
    MOVE Cellcontent(20) TO ID-INPUT20.
    MOVE Cellcontent(21) TO ID-INPUT21.
    MOVE Cellcontent(22) TO ID-INPUT22.
    MOVE Cellcontent(23) TO ID-INPUT23.
    MOVE Cellcontent(24) TO ID-INPUT24.
    MOVE Cellcontent(25) TO ID-INPUT25.
    MOVE Cellcontent(26) TO ID-INPUT26.
    MOVE Cellcontent(27) TO ID-INPUT27.
    MOVE Cellcontent(28) TO ID-INPUT28.
    MOVE Cellcontent(29) TO ID-INPUT29.
    MOVE Cellcontent(30) TO ID-INPUT30.
    MOVE Cellcontent(31) TO ID-INPUT31.
    MOVE Cellcontent(32) TO ID-INPUT32.
    MOVE Cellcontent(33) TO ID-INPUT33.
    MOVE Cellcontent(34) TO ID-INPUT34.
    MOVE Cellcontent(35) TO ID-INPUT35.
    MOVE Cellcontent(36) TO ID-INPUT36.
    MOVE Cellcontent(37) TO ID-INPUT37.
    MOVE Cellcontent(38) TO ID-INPUT38.
    MOVE Cellcontent(39) TO ID-INPUT39.
    MOVE Cellcontent(40) TO ID-INPUT40.
    Tablecontentinputalt.
    *>Programm bei anderem Eingabebereich
    MOVE 1 TO Inputnum.
    MOVE Spalte1(1) TO Cellcontent(Inputnum).
    ADD 1 TO Inputnum.
    MOVE Spalte2(1) TO Cellcontent(Inputnum).
    ADD 1 TO Inputnum.
    MOVE Spalte3(1) TO Cellcontent(Inputnum).
    ADD 1 TO Inputnum.
    MOVE Spalte4(1) TO Cellcontent(Inputnum).
    ADD 1 TO Inputnum.
    MOVE Spalte1(2) TO Cellcontent(Inputnum).
    ADD 1 TO Inputnum.
    MOVE Spalte2(2) TO Cellcontent(Inputnum).
    ADD 1 TO Inputnum.
    MOVE Spalte3(2) TO Cellcontent(Inputnum).
    ADD 1 TO Inputnum.
    MOVE Spalte4(2) TO Cellcontent(Inputnum).
    ADD 1 TO Inputnum.
    MOVE Spalte1(3) TO Cellcontent(Inputnum).
    ADD 1 TO Inputnum.
    MOVE Spalte2(3) TO Cellcontent(Inputnum).
    ADD 1 TO Inputnum.
    MOVE Spalte3(3) TO Cellcontent(Inputnum).
    ADD 1 TO Inputnum.
    MOVE Spalte4(3) TO Cellcontent(Inputnum).
    ADD 1 TO Inputnum.
    MOVE Spalte1(4) TO Cellcontent(Inputnum).
    ADD 1 TO Inputnum.
    MOVE Spalte2(4) TO Cellcontent(Inputnum).
    ADD 1 TO Inputnum.
    MOVE Spalte3(4) TO Cellcontent(Inputnum).
    ADD 1 TO Inputnum.
    MOVE Spalte4(4) TO Cellcontent(Inputnum).
    ADD 1 TO Inputnum.
    MOVE Spalte1(5) TO Cellcontent(Inputnum).
    ADD 1 TO Inputnum.
    MOVE Spalte2(5) TO Cellcontent(Inputnum).
    ADD 1 TO Inputnum.
    MOVE Spalte3(5) TO Cellcontent(Inputnum).
    ADD 1 TO Inputnum.
    MOVE Spalte4(5) TO Cellcontent(Inputnum).
    ADD 1 TO Inputnum.
    MOVE Spalte1(6) TO Cellcontent(Inputnum).
    ADD 1 TO Inputnum.
    MOVE Spalte2(6) TO Cellcontent(Inputnum).
    ADD 1 TO Inputnum.
    MOVE Spalte3(6) TO Cellcontent(Inputnum).
    ADD 1 TO Inputnum.
    MOVE Spalte4(6) TO Cellcontent(Inputnum).
    ADD 1 TO Inputnum.
    MOVE Spalte1(7) TO Cellcontent(Inputnum).
    ADD 1 TO Inputnum.
    MOVE Spalte2(7) TO Cellcontent(Inputnum).
    ADD 1 TO Inputnum.
    MOVE Spalte3(7) TO Cellcontent(Inputnum).
    ADD 1 TO Inputnum.
    MOVE Spalte4(7) TO Cellcontent(Inputnum).
    ADD 1 TO Inputnum.
    MOVE Spalte1(8) TO Cellcontent(Inputnum).
    ADD 1 TO Inputnum.
    MOVE Spalte2(8) TO Cellcontent(Inputnum).
    ADD 1 TO Inputnum.
    MOVE Spalte3(8) TO Cellcontent(Inputnum).
    ADD 1 TO Inputnum.
    MOVE Spalte4(8) TO Cellcontent(Inputnum).
    ADD 1 TO Inputnum.
    MOVE Spalte1(9) TO Cellcontent(Inputnum).
    ADD 1 TO Inputnum.
    MOVE Spalte2(9) TO Cellcontent(Inputnum).
    ADD 1 TO Inputnum.
    MOVE Spalte3(9) TO Cellcontent(Inputnum).
    ADD 1 TO Inputnum.
    MOVE Spalte4(9) TO Cellcontent(Inputnum).
    ADD 1 TO Inputnum.
    MOVE Spalte1(10) TO Cellcontent(Inputnum).
    ADD 1 TO Inputnum.
    MOVE Spalte2(10) TO Cellcontent(Inputnum).
    ADD 1 TO Inputnum.
    MOVE Spalte3(10) TO Cellcontent(Inputnum).
    ADD 1 TO Inputnum.
    MOVE Spalte4(10) TO Cellcontent(Inputnum).
    Tablecontentoutputalt.
    *>Programm bei anderem Anzeigebereich
    MOVE 1 TO Inputnum.
    MOVE Cellcontent(Inputnum) TO ID-INPUT.
    ADD 1 TO Inputnum.
    MOVE Cellcontent(Inputnum) TO ID-INPUT2.
    ADD 1 TO Inputnum.
    MOVE Cellcontent(Inputnum) TO ID-INPUT3.
    ADD 1 TO Inputnum.
    MOVE Cellcontent(Inputnum) TO ID-INPUT4.
    *>andere Spaltenzahl erfordert Zusatz
    ADD 1 TO Inputnum.
    MOVE Cellcontent(Inputnum) TO ID-INPUT5.
    ADD 1 TO Inputnum.
    MOVE Cellcontent(Inputnum) TO ID-INPUT6.
    ADD 1 TO Inputnum.
    MOVE Cellcontent(Inputnum) TO ID-INPUT7.
    ADD 1 TO Inputnum.
    MOVE Cellcontent(Inputnum) TO ID-INPUT8.
    *>andere Spaltenzahl erfordert Zusatz
    ADD 1 TO Inputnum.
    MOVE Cellcontent(Inputnum) TO ID-INPUT9.
    ADD 1 TO Inputnum.
    MOVE Cellcontent(Inputnum) TO ID-INPUT10.
    ADD 1 TO Inputnum.
    MOVE Cellcontent(Inputnum) TO ID-INPUT11.
    ADD 1 TO Inputnum.
    MOVE Cellcontent(Inputnum) TO ID-INPUT12.
    *>andere Spaltenzahl erfordert Zusatz
    ADD 1 TO Inputnum.
    MOVE Cellcontent(Inputnum) TO ID-INPUT13.
    ADD 1 TO Inputnum.
    MOVE Cellcontent(Inputnum) TO ID-INPUT14.
    ADD 1 TO Inputnum.
    MOVE Cellcontent(Inputnum) TO ID-INPUT15.
    ADD 1 TO Inputnum.
    MOVE Cellcontent(Inputnum) TO ID-INPUT16.
    *>andere Spaltenzahl erfordert Zusatz
    ADD 1 TO Inputnum.
    MOVE Cellcontent(Inputnum) TO ID-INPUT17.
    ADD 1 TO Inputnum.
    MOVE Cellcontent(Inputnum) TO ID-INPUT18.
    ADD 1 TO Inputnum.
    MOVE Cellcontent(Inputnum) TO ID-INPUT19.
    ADD 1 TO Inputnum.
    MOVE Cellcontent(Inputnum) TO ID-INPUT20.
    *>andere Spaltenzahl erfordert Zusatz
    ADD 1 TO Inputnum.
    MOVE Cellcontent(Inputnum) TO ID-INPUT21.
    ADD 1 TO Inputnum.
    MOVE Cellcontent(Inputnum) TO ID-INPUT22.
    ADD 1 TO Inputnum.
    MOVE Cellcontent(Inputnum) TO ID-INPUT23.
    ADD 1 TO Inputnum.
    MOVE Cellcontent(Inputnum) TO ID-INPUT24.
    *>andere Spaltenzahl erfordert Zusatz
    ADD 1 TO Inputnum.
    MOVE Cellcontent(Inputnum) TO ID-INPUT25.
    ADD 1 TO Inputnum.
    MOVE Cellcontent(Inputnum) TO ID-INPUT26.
    ADD 1 TO Inputnum.
    MOVE Cellcontent(Inputnum) TO ID-INPUT27.
    ADD 1 TO Inputnum.
    MOVE Cellcontent(Inputnum) TO ID-INPUT28.
    *>andere Spaltenzahl erfordert Zusatz
    ADD 1 TO Inputnum.
    MOVE Cellcontent(Inputnum) TO ID-INPUT29.
    ADD 1 TO Inputnum.
    MOVE Cellcontent(Inputnum) TO ID-INPUT30.
    ADD 1 TO Inputnum.
    MOVE Cellcontent(Inputnum) TO ID-INPUT31.
    ADD 1 TO Inputnum.
    MOVE Cellcontent(Inputnum) TO ID-INPUT32.
    *>andere Spaltenzahl erfordert Zusatz
    ADD 1 TO Inputnum.
    MOVE Cellcontent(Inputnum) TO ID-INPUT33.
    ADD 1 TO Inputnum.
    MOVE Cellcontent(Inputnum) TO ID-INPUT34.
    ADD 1 TO Inputnum.
    MOVE Cellcontent(Inputnum) TO ID-INPUT35.
    ADD 1 TO Inputnum.
    MOVE Cellcontent(Inputnum) TO ID-INPUT36.
    *>andere Spaltenzahl erfordert Zusatz
    ADD 1 TO Inputnum.
    MOVE Cellcontent(Inputnum) TO ID-INPUT37.
    ADD 1 TO Inputnum.
    MOVE Cellcontent(Inputnum) TO ID-INPUT38.
    ADD 1 TO Inputnum.
    MOVE Cellcontent(Inputnum) TO ID-INPUT39.
    ADD 1 TO Inputnum.
    MOVE Cellcontent(Inputnum) TO ID-INPUT40.
    Cobolwordtableinit.
    MOVE "(" TO Cobolword(1).
    MOVE 1 TO Charcount3(1).
    MOVE ":" TO Cobolword(2).
    MOVE 1 TO Charcount3(2).
    MOVE ")" TO Cobolword(3).
    MOVE 1 TO Charcount3(3).
    MOVE "." TO Cobolword(4).
    MOVE 1 TO Charcount3(4).
    MOVE "+" TO Cobolword(5).
    MOVE 1 TO Charcount3(5).
    MOVE "-" TO Cobolword(6).
    MOVE 1 TO Charcount3(6).
    MOVE "*" TO Cobolword(7).
    MOVE 1 TO Charcount3(7).
    MOVE "/" TO Cobolword(8).
    MOVE 1 TO Charcount3(8).
    MOVE ";" TO Cobolword(9).
    MOVE 1 TO Charcount3(9).
    MOVE "(" TO Cobolword(10).
    MOVE 1 TO Charcount3(10).
    MOVE "TEIL" TO Cobolword(11).
    MOVE 4 TO Charcount3(11).
    MOVE "LAENGE" TO Cobolword(12).
    MOVE 6 TO Charcount3(12).
    MOVE "SUMME" TO Cobolword(13).
    MOVE 5 TO Charcount3(13).
    MOVE "GLAETTEN" TO Cobolword(14).
    MOVE 8 TO Charcount3(14).
    MOVE "REST" TO Cobolword(15).
    MOVE 4 TO Charcount3(15).
    MOVE "&" TO Cobolword(16).
    MOVE 1 TO Charcount3(16).
    *>MOVE ">" TO Cobolword(990).
    *>MOVE 1 TO Charcount3(990).
    Trimprog.
    MOVE 1 TO Posnum.
    MOVE 1 TO Num2.
    MOVE 1 TO Lengthcalcnum.
    MOVE 1 TO Prespacedetectnum.
    MOVE 1 TO Trimpos.
    MOVE " " TO Scriptbase.
    MOVE Subtext2(4) TO Extstr.
    MOVE Subtext2(4) TO Substrinput.
    IF Substrinput(1:1) = """" THEN STRING Substrinput(2:Charcount2(4) - 2) "  " INTO Substrinput(1:Charcount2(4))
    ELSE PERFORM Inspectprog
    END-IF.
    PERFORM Trimsubprog 200 TIMES.
    MOVE " " TO Substrinput.
    MOVE Trimresult TO Substrinput.
    MOVE Substrinput TO Cellcontent(Scriptpos).
    Trimsubprog.
    IF Substrinput(Lengthcalcnum:1) EQUAL TO " " THEN MOVE 1 TO Spacedetectnum.
    IF Substrinput(Lengthcalcnum:1) NOT EQUAL TO " " THEN MOVE 0 TO Prespacedetectnum.
    IF Prespacedetectnum NOT EQUAL TO 1 THEN MOVE Substrinput(Lengthcalcnum:1) TO Trimresult(Trimpos:1).
    IF Prespacedetectnum NOT EQUAL TO 1 THEN ADD 1 TO Trimpos.
    MOVE Spacedetectnum TO Prespacedetectnum.
    MOVE 0 TO Spacedetectnum.
    ADD 1 TO Lengthcalcnum.
    Substituteprog.
    MOVE 1 TO Posnum.
    MOVE 1 TO Num2.
    MOVE 1 TO Lengthcalcnum.
    MOVE 1 TO Prespacedetectnum.
    MOVE 1 TO Trimpos.
    MOVE " " TO Scriptbase.
    MOVE Subtext2(4) TO Extstr.
    MOVE Subtext2(4) TO Substrinput.
    IF Substrinput(1:1) = """" THEN STRING Substrinput(2:Charcount2(4) - 2) "  " INTO Substrinput(1:Charcount2(4))
    ELSE PERFORM Inspectprog
    END-IF.
    PERFORM Trimsubprog 200 TIMES.
    MOVE " " TO Substrinput.
    MOVE Trimresult TO Substrinput.
    MOVE Substrinput TO Cellcontent(Scriptpos).
    Substitutesubprog.
    IF Substrinput(Lengthcalcnum:1) EQUAL TO " " THEN MOVE 1 TO Spacedetectnum.
    IF Substrinput(Lengthcalcnum:1) NOT EQUAL TO " " THEN MOVE 0 TO Prespacedetectnum.
    IF Prespacedetectnum NOT EQUAL TO 1 THEN MOVE Substrinput(Lengthcalcnum:1) TO Trimresult(Trimpos:1).
    IF Prespacedetectnum NOT EQUAL TO 1 THEN ADD 1 TO Trimpos.
    MOVE Spacedetectnum TO Prespacedetectnum.
    MOVE 0 TO Spacedetectnum.
    ADD 1 TO Lengthcalcnum.
    Lengthcalc.
    IF Substrinput(Lengthcalcnum:1) NOT EQUAL TO " " THEN MOVE Lengthcalcnum TO Lengthcalcres.
    ADD 1 TO Lengthcalcnum.
    IF Lengthcalcnum = 200 THEN MOVE " " TO Substrinput.
    Inspectprog.
    *>Programm, welches die Zellart ermittelt
    *>MOVE Substrinput TO Extstr.
    INSPECT Extstr REPLACING All "A" BY "|", "B" BY "|", "C" BY "|", "D" BY "|".
    INSPECT Extstr REPLACING All "E" BY "|", "F" BY "|", "G" BY "|", "H" BY "|".
    INSPECT Extstr REPLACING All "I" BY "|", "J" BY "|", "K" BY "|", "L" BY "|".
    INSPECT Extstr REPLACING All "M" BY "|", "N" BY "|", "O" BY "|", "P" BY "|".
    INSPECT Extstr REPLACING All "Q" BY "|", "R" BY "|", "S" BY "|", "T" BY "|".
    INSPECT Extstr REPLACING All "U" BY "|", "V" BY "|", "W" BY "|", "X" BY "|".
    INSPECT Extstr REPLACING All "Y" BY "|", "Z" BY "|", "1" BY ",", "2" BY ",".
    INSPECT Extstr REPLACING All "3" BY ",", "4" BY ",", "5" BY ",", "6" BY ",".
    INSPECT Extstr REPLACING All "7" BY ",", "8" BY ",", "9" BY ",", "0" BY ",".
    DISPLAY "Replaced: " Extstr.
    *>MOVE Extstr TO Cellkind(Num44).
    *>IF Extstr = "|,:|," THEN DISPLAY "Cellrange".
    IF Extstr = "|," THEN MOVE "Celladdress" TO Inspectres.
    IF Extstr = "|,," THEN MOVE "Celladdress" TO Inspectres.
    IF Extstr = "|,:|," THEN MOVE "Cellrange" TO Inspectres.
    *>IF Extstr = "|," THEN PERFORM Rangetableprog.
    IF Extstr = " " THEN MOVE "Empty" TO Inspectres.
    IF Extstr = "|," THEN PERFORM Cellcontentmove.
    IF Extstr = "|,," THEN PERFORM Cellcontentmove.
    *>IF Extstr = "|,:|," THEN PERFORM Rangetableprog.
    IF Extstr = "," THEN PERFORM Valmove.
    IF Extstr = ",," THEN PERFORM Valmove.
    IF Extstr = ",,," THEN PERFORM Valmove.
    IF Extstr = ",,,," THEN PERFORM Valmove.
    Valmove.
    MOVE Valinputtext TO Valinput.
    Cellcontentmove.
    MOVE 2 TO Rowstart.
    IF Extstr = "|,," THEN MOVE 2 TO Rowstartnum ELSE MOVE 1 TO Rowstartnum.
    MOVE 1 TO Colstart.
    MOVE 1 TO Colstartnum.
    MOVE Substrinput(Colstart:Colstartnum) TO Coladresstext.
    IF Coladresstext = "A" THEN MOVE 1 TO Coladressnum.
    IF Coladresstext = "B" THEN MOVE 2 TO Coladressnum.
    IF Coladresstext = "C" THEN MOVE 3 TO Coladressnum.
    IF Coladresstext = "D" THEN MOVE 4 TO Coladressnum.
    MOVE Substrinput(Rowstart:Rowstartnum) TO Rowadressnum.
    MOVE Rowadressnum TO Rowadressnumtext.
    SUBTRACT 1 FROM Rowadressnum.
    IF Rowadressnum > 0 THEN MULTIPLY 4 BY Rowadressnum.
    ADD Coladressnum TO Rowadressnum GIVING Celladressres.
    *>IF Rowadressnum = 10 THEN MULTIPLY 4 BY Rowadressnum GIVING Celladressres.
    *>MOVE Cellcontent(Celladressres) TO Substrinput.
    MOVE Cellcontent(Celladressres) TO Substrinput.
    MOVE Celladressres TO Cellposalt2.
    MOVE 0 TO Coladressnum.
    MOVE 0 TO Rowadressnum.
    MOVE 0 TO Celladressres.
    MOVE 0 TO Rowstart.
    MOVE 0 TO Rowstartnum.
    Cellrangemovealt.
    IF Currentcol GREATER THAN Firstcol AND Currentcol LESS THAN Lastcol THEN
    MOVE Cellcontent(Celladressres) TO Cellrange(Rangecontentpos)
    END-IF.
    ADD 1 TO Celladressres.
    IF Currentcol GREATER THAN Firstcol AND Currentcol LESS THAN Lastcol THEN ADD 1 TO Rangecontentpos.
    IF Currentcol = 4 THEN MOVE 1 TO Currentcol ELSE ADD 1 TO Currentcol.
    Cellpathevalprog.
    *>MOVE Substrinput TO Cellrange(4).
    END PROGRAM YOUR-PROGRAM-NAME.
