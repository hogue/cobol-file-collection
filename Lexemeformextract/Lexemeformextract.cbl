*>****************************************************************
*> Author: Hogü-456
*> Date: October 2023
*> Purpose: A program to extract out of a Turtlefile of a Lexeme the different forms and putting it to a CSV.
*>****************************************************************
IDENTIFICATION DIVISION.
PROGRAM-ID. YOUR-PROGRAM-NAME.
ENVIRONMENT DIVISION.
       INPUT-OUTPUT SECTION.
       FILE-CONTROL.
SELECT File-Name2 ASSIGN TO "D:\Cobolprogramme\Lexemeextract\L408970.ttl"
*>SELECT File-Name2 ASSIGN TO "D:\Wikidata_Lexemextrakt\wikidata-20230818-lexemes-BETA.ttl\wikidata-20230818-lexemes-BETA.ttl.001"
ORGANIZATION IS LINE SEQUENTIAL.
SELECT Outputfile ASSIGN TO "D:\Cobolprogramme\Lexemeextract\Output.csv"
ORGANIZATION IS LINE SEQUENTIAL.
DATA DIVISION.
FILE SECTION.
           FD  File-Name2.
           01  Record-Name3.
           02 Field1                  PIC X(2000).
           FD  Outputfile.
           01  Record-Name4.
           02 Field2                  PIC X(2000).
WORKING-STORAGE SECTION.
    01 Str PICTURE X(2000).
    01 Label22 PICTURE X(2000).
    01 Prelabeloutput PICTURE X(2000).
    01 Labeloutput PICTURE X(2000).
    01 Feature PICTURE X(2000).
    01 Decstr PICTURE X(8).
    01 Lexemeid PICTURE X(2000).
    01 Featurestring PICTURE X(2000).
    01 Num PICTURE 9(15).
    01 Presetnum PICTURE 9(15).
    01 Setnum PICTURE 9(15).
    01 Setnum2 PICTURE 9(15).
    01 Num2 PICTURE 9(15).
    01 Num3 PICTURE 9(4).
    01 Prelength PICTURE 9(4).
    01 Featurelength PICTURE 9(4).
    01 Diffnum PICTURE 9(4).
    01 Relset PICTURE 9(1).
    01 Decnum PICTURE 9(1).
    01 Tallnum2 PICTURE 9(1).
    01 Labelstart PICTURE 9(4).
    01 Labelend PICTURE 9(4).
    01 Labelend2 PICTURE 9(4).
    01 Langend PICTURE 9(4).
    01 Langend2 PICTURE 9(4).
    01 Lexemecount PICTURE 9(4).
    01 Lexemecount2 PICTURE 9(4).
    01 current-time.
    05 ct-hours      pic 99.
    05 ct-minutes    pic 99.
    05 ct-seconds    pic 99.
    05 ct-hundredths pic 99.
    PROCEDURE DIVISION.
    ACCEPT current-time FROM TIME.
    DISPLAY "Starttime: " current-time.
    MOVE 1 TO Setnum.
    OPEN OUTPUT Outputfile.
    MOVE "Form,Length,Feature1,Feature2" TO Labeloutput.
    *>WRITE Record-Name4 FROM Labeloutput.
    PERFORM Lexemeprog.
    WRITE Record-Name4 FROM Labeloutput.
    DISPLAY "Output in Output.csv".
    CLOSE Outputfile.
    ACCEPT current-time FROM TIME.
    DISPLAY "Stoptime: " current-time.
    STOP RUN.
    Lexemeprog.
    OPEN INPUT File-Name2.
    MOVE 1 TO Diffnum.
    PERFORM Subread UNTIL Tallnum2 = 1.
    *>PERFORM Subread2 32 TIMES.
    *>READ File-Name2 INTO Str.
    *>DISPLAY Str.
    *>DISPLAY "Rownumber: " Num.
    CLOSE File-Name2.
    Subread2.
    READ File-Name2.
    Subread.
    MOVE 0 TO Lexemecount.
    MOVE 0 TO Lexemecount2.
    MOVE Setnum TO Setnum2.
    READ File-Name2 INTO Str AT END MOVE 1 TO Tallnum2.
    ADD 1 TO Num.
    STRING Str(1:4) INTO Decstr.
    IF Decstr = "data" THEN MOVE 1 TO Decnum.
    IF Decstr = "wd:L" THEN MOVE 2 TO Decnum.
    IF Decnum = 1 THEN PERFORM Lexemesearch.
    *>IF Decnum = 2 THEN PERFORM Featureextract.
    IF Decnum = 2 PERFORM Featureextract.
    IF Decnum = 3 THEN PERFORM Featuresub.
    INSPECT Str TALLYING Setnum FOR ALL ".".
    *>DISPLAY "Tallyed" Setnum.
    IF Setnum NOT EQUAL TO Setnum2 THEN MOVE 0 TO Decnum.
    *>MOVE 0 TO Setnum2.
    Lexemesearch.
    INSPECT Str TALLYING Lexemecount FOR CHARACTERS AFTER "schema:about wd:L".
    *>DISPLAY "Lexemecount: " Lexemecount.
    IF Lexemecount > 0 THEN INSPECT Str TALLYING Lexemecount2 FOR CHARACTERS AFTER ";".
    IF Lexemecount > 0 THEN STRING Str(2000 - Lexemecount:Lexemecount - Lexemecount2) INTO Lexemeid.
    *>IF Lexemecount > 0 THEN DISPLAY "Lexemeid: " Lexemeid.
    Featureextract.
    INSPECT Str TALLYING Num2 FOR CHARACTERS AFTER "wikibase:grammaticalFeature".
    *>IF Num2 NOT EQUAl TO 0 THEN DISPLAY "Grammatical Feature".
    IF Num2 NOT EQUAl TO 0 THEN MOVE " " TO Prelabeloutput.
    IF Num2 NOT EQUAl TO 0 THEN MOVE Labeloutput TO Prelabeloutput.
    IF Num2 NOT EQUAl TO 0 THEN MOVE " " TO Labeloutput.
    *>IF Num2 NOT EQUAl TO 0 THEN DISPLAY "Label: " Label22.
    IF Num2 NOT EQUAl TO 0 THEN MOVE 0 TO Langend2.
    IF Num2 NOT EQUAl TO 0 THEN PERFORM Langextract.
    IF Num2 NOT EQUAl TO 0 THEN WRITE Record-Name4 FROM Prelabeloutput.
    IF Num2 NOT EQUAl TO 0 THEN MOVE " " TO Label22.
    IF Num2 > 0 THEN STRING Str(2000 - Num2 + 2:2000 - Num2 + 2) INTO Featurestring.
    IF Num2 NOT EQUAl TO 0 THEN MOVE 3 TO Decnum.
    MOVE 0 TO Num2.
    INSPECT Str TALLYING Labelstart FOR CHARACTERS BEFORE "rdfs:label """.
    INSPECT Str TALLYING Labelend FOR CHARACTERS BEFORE """@".
    INSPECT Str TALLYING Langend FOR CHARACTERS AFTER ";".
    IF Labelstart < 2000 THEN STRING Str(Labelstart + 13 :Labelend - Labelstart - 2) INTO Label22.
    MOVE 0 TO Labelstart.
    MOVE 0 TO Labelend.
    MOVE 0 TO Langend.
    Featuresub.
    *>DISPLAY Str.
    IF Featurestring EQUAL TO " " THEN ADD 5 TO Diffnum ELSE ADD 3 TO Diffnum.
    IF Featurestring EQUAL TO " " THEN MOVE Str TO Featurestring.
    INSPECT Featurestring TALLYING Num3 FOR CHARACTERS AFTER ",".
    IF Num3 = 0 THEN INSPECT Featurestring TALLYING Num3 FOR CHARACTERS AFTER ".".
    *>DISPLAY "Num3: " Num3 Featurestring.
    SUBTRACT Num3 Diffnum FROM 2000 GIVING Featurelength.
    STRING Featurestring(Diffnum:2000 - Num3 - Diffnum) INTO Feature.
    STRING Featurestring(Diffnum:Featurelength) INTO Feature.
    *>DISPLAY "Diff: " Diffnum.
    IF Diffnum > 5 THEN SUBTRACT 5 FROM Diffnum ELSE SUBTRACT 3 FROM Diffnum.
    *>DISPLAY "Feature: " Feature.
    MOVE Langend2 TO Prelength.
    ADD Featurelength TO Langend2.
    *>DISPLAY "Length:" Langend2.
    MOVE "," TO Labeloutput(Prelength + 1:1).
    MOVE Feature(1:Featurelength) TO Labeloutput(Prelength + 2:Langend2 - Prelength).
    *>DISPLAY "Output: " Labeloutput.
    ADD 1 TO Langend2.
    MOVE 0 TO Num3.
    MOVE " " TO Featurestring.
    Langextract.
    INSPECT Label22 TALLYING Labelend2 FOR CHARACTERS BEFORE """@".
    INSPECT Label22 TALLYING Langend2 FOR CHARACTERS BEFORE ";".
    *>DISPLAY "End2: " Langend2.
    MOVE Label22(1:Labelend2) TO Labeloutput.
    MOVE "," TO Labeloutput(Labelend2 + 1:1).
    MOVE Label22(Labelend2 + 3:Langend2 - Labelend2 - 3) TO Labeloutput(Labelend2 + 2:Langend2 - Labelend2).
    SUBTRACT 2 FROM Langend2.
    *>DISPLAY "Totallength: " Langend2.
    MOVE 0 TO Labelend2.
    *>MOVE 0 TO Langend2.
END PROGRAM YOUR-PROGRAM-NAME.
