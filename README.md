# Cobol file collection

This repository includes COBOL-Files. COBOL is an old programming language with a syntax similar to English sentences. 

You can find different several COBOL Sourcefiles in this repository. In COBOL it is possible to define Text based User Interfaces. The files are written and tested using GNU Cobol and Open COBOL IDE. This repository is also about how to create text based user interfaces.
