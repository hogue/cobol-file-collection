*>****************************************************************
*> Author:
*> Date:
*> Purpose: Convert Cobol Statements to Blocks
*> Tectonics: cobc
*>****************************************************************
IDENTIFICATION DIVISION.
PROGRAM-ID. YOUR-PROGRAM-NAME.
ENVIRONMENT DIVISION.
       INPUT-OUTPUT SECTION.
       FILE-CONTROL.
            SELECT File-Name ASSIGN TO "D:\Cobolprogramme\Reservedwords.txt"
                 ORGANIZATION IS LINE SEQUENTIAL.
                             SELECT File-Name2 ASSIGN TO "D:\Cobolprogramme\Cobolexampleprog\Cobolprog.txt"
                 ORGANIZATION IS LINE SEQUENTIAL.
                             SELECT Distancefile ASSIGN TO "D:\Cobolprogramme\Distanceparameter.txt"
                 ORGANIZATION IS LINE SEQUENTIAL.
                 SELECT Varfile ASSIGN TO "D:\Cobolprogramme\Cobolexampleprog\Cobolvar.txt"
                 ORGANIZATION IS LINE SEQUENTIAL.
                 SELECT Shortfile ASSIGN TO "D:\Cobolprogramme\Cobolexampleprog\Shortpartfile.txt"
                 ORGANIZATION IS LINE SEQUENTIAL.
                 SELECT Statements ASSIGN TO "D:\Cobolprogramme\Cobolexampleprog\Cobolstatements20230105.txt"
                 ORGANIZATION IS LINE SEQUENTIAL.
                 DATA DIVISION.
                        FILE SECTION.
       FD  File-Name.
       01  Record-Name.
           02 Field1                  PIC X(80).
                  FD  File-Name2.
       01  Record-Name2.
           02 Field2                  PIC X(80).
           FD  Distancefile.
                  01  Record-Name3.
           02 Field3                 PIC 9(4).
                      FD  Varfile.
                  01  Record-Name4.
           02 Field4                PIC X(200).
                                 FD  Shortfile.
                  01  Record-Name5.
           02 Field5               PIC X(200).
                                            FD  Statements.
                  01  Record-Name6.
           02 Field5               PIC X(200).
WORKING-STORAGE SECTION.
01 Str PIC X(200).
01 Str2 PIC X(200).
01 Str3 PIC X(200).
01 Readstring PIC X(200).
01 Searchstring PIC X(200).
01 Prestr PIC X(200).
01 Str4 PIC 9(4).
01 Lastspace PIC 9(4).
01 Lastchar PIC 9(4).
01 Startval PIC 9(4).
01 Endval PIC 9(4).
01 Diffdist PIC 9(4).
01 Precumdiff PIC 9(4).
01 Countnum PIC 9(4).
01 Countnum2 PIC 9(4).
01 Cumdiff PIC 9(4).
01 Wordcountval PIC 9(4).
01 Str5 PIC X(200).
01 Str6 PIC 9(2).
01 Controlarg PIC 9(2).
01 Concatstr PIC X(200).
01 Prestring PIC X(200).
01 Widesign PIC X(200).
01 Quotvar PIC 9(4).
01 Loopnum PIC 9(4).
01 Loopnum2 PIC 9(4).
01 Checkval PIC 9(4).
01 Precheckval PIC 9(4).
01 Checkval2 PIC 9(4).
           01 Presign PIC X(200).
           01 Actsign PIC X(200).
           01 Calcsign PIC X(200).
           01 New-Val                   PIC X(80)
           VALUE 'Hello World'.
           PROCEDURE DIVISION.
           MAIN-PROCEDURE.
           MOVE 1 TO Loopnum.
           MOVE 0 TO Precheckval.
           OPEN OUTPUT Statements.
           *> Enter the number of Lines of Cobolprog.txt
           PERFORM Loopprog 182 TIMES.
           CLOSE Statements.
           STOP RUN.
           Loopprog.
           *>DISPLAY "Function:" ACCEPT Str.
           DISPLAY "Char: ŋ".
           OPEN INPUT File-Name2.
           SUBTRACT 1 FROM Loopnum GIVING Loopnum2.
           PERFORM Filename2read Loopnum2 TIMES.
           MOVE 1 TO Str4.
           READ File-Name2 INTO Str3.
           DISPLAY "Str3: " Str3.
           MOVE 1 TO Countnum.
           MOVE Str3 TO Str.
           PERFORM Spacewideprog 100 TIMES.
           MOVE Str5 TO Str.
           MOVE " " TO Str5.
           MOVE Str TO Str3.
           DISPLAY "New Str: " Str.
           MOVE Str3 TO Readstring.
           CLOSE File-Name2.
           PERFORM Charlen.
           *>MOVE "H" TO Str5.
           *>IF Str5 IS ALPHABETIC-UPPER THEN DISPLAY "Test".
           *>DISPLAY Str.
           PERFORM Wordcount.
           OPEN INPUT Distancefile.
           MOVE "_" TO Prestr.
           MOVE 1 TO Cumdiff.
           OPEN OUTPUT Varfile.
           PERFORM Partextract Wordcountval TIMES.
           CLOSE Distancefile.
           CLOSE Varfile.
           DISPLAY "Cum: " Cumdiff.
           STRING Concatstr(2:Cumdiff) INTO Concatstr.
           SUBTRACT 1 FROM Cumdiff.
           STRING Concatstr(1:Cumdiff) INTO Str5.
           WRITE Record-Name6 FROM Str5.
           DISPLAY "Str5: " Loopnum ", " Str5.
           ADD 1 TO Loopnum.
           *>PERFORM Furtherprog.
           Filename2read.
           READ File-Name2.
           Furtherprog.
           OPEN INPUT Shortfile.
           PERFORM Blocksearch.
           CLOSE Shortfile.
           DISPLAY Str2.
           STRING Str2(Cumdiff + 2:4) INTO Str.
           MOVE 0 TO Str6.
           STRING Str(3:2) INTO Str6.
           DISPLAY "Extractedstr6: " Str6.
           PERFORM Replacesubprog.
           Spacewideprog.
           STRING Str(Countnum:1) INTO Str2.
           IF Str2 = ":"  OR Str2 = "." OR Str2 = ")" OR Str2 = "(" THEN DISPLAY "Searchsign" Str2.
           IF Str2 = ":"  OR Str2 = "." OR Str2 = ")" OR Str2 = "(" THEN STRING SPACE Str2(1:1) SPACE INTO Str3 ELSE MOVE "|||" TO Str3.
           IF Str2 = ":"  OR Str2 = "." OR Str2 = ")" OR Str2 = "(" THEN STRING Str(1:Countnum - 1) Str3(1:3) Str(Countnum + 1:100) INTO Str5.
           IF Str2 = ":"  OR Str2 = "." OR Str2 = ")" OR Str2 = "(" THEN MOVE Str5 TO Str.
           IF Str2 = ":"  OR Str2 = "." OR Str2 = ")" OR Str2 = "(" THEN ADD 3 TO Countnum ELSE ADD 1 TO Countnum.
           IF Str2 = ":"  OR Str2 = "." OR Str2 = ")" OR Str2 = "(" THEN MOVE 1 TO Controlarg.
           IF Countnum = 100 AND Controlarg = 0 THEN MOVE Str TO Str5.
           Replacesubprog.
           INSPECT Str REPLACING All "a" BY "A", "b" BY "B", "c" BY "C", "d" BY "D".
           INSPECT Str REPLACING All "e" BY "E", "f" BY "F", "g" BY "G", "h" BY "H".
           INSPECT Str REPLACING All "i" BY "I", "j" BY "J", "k" BY "K", "l" BY "L".
           INSPECT Str REPLACING All "m" BY "M", "n" BY "N", "o" BY "O", "p" BY "P".
           INSPECT Str REPLACING All "q" BY "Q", "r" BY "R", "s" BY "S", "t" BY "T".
           INSPECT Str REPLACING All "u" BY "U", "v" BY "V", "w" BY "W", "x" BY "X".
           INSPECT Str REPLACING All "y" BY "Y", "z" BY "Z".
           DISPLAY Str.
           Charlen.
           MOVE 1 TO Str4.
           PERFORM Substrprog 100 TIMES.
           SUBTRACT 1 FROM Lastchar.
           DISPLAY "The String is " Lastchar " Characters long".
           Wordcount.
           MOVE 1 TO Str4.
           ADD 1 TO Lastchar.
           OPEN OUTPUT Distancefile.
           MOVE 0 TO Countnum.
           PERFORM Diffwrite Lastchar TIMES.
           CLOSE Distancefile.
           DIVIDE Countnum BY 2 GIVING Countnum2 REMAINDER Checkval.
           MOVE Countnum2 TO Wordcountval.
           ADD Checkval TO Wordcountval.
           DISPLAY "There are " Wordcountval " words in the sentence from " Countnum " as base".
           Rownumevaluate.
           MOVE 1 TO Countnum.
           OPEN INPUT File-Name.
           PERFORM Substrprog3 UNTIL Str = "End".
           CLOSE File-Name.
           SUBTRACT 1 FROM Countnum2.
           *>DISPLAY "Number of Rows " Countnum2.
           Substrprog.
           MOVE Str(Str4:1) TO Str2.
           ADD 1 TO Str4.
           IF Str2 = """" THEN ADD 1 TO Quotvar.
           DIVIDE Quotvar BY 2 GIVING Checkval2 REMAINDER Checkval.
           PERFORM Spaceident.
           PERFORM Nextinit.
           Diffwrite.
           MOVE Str(Str4:1) TO Str2.
           IF Str2 = """" THEN ADD 1 TO Quotvar.
           DIVIDE Quotvar BY 2 GIVING Checkval2 REMAINDER Checkval.
           IF Str2 = """" THEN DISPLAY "Quotvar: " Quotvar ", " Checkval ", " Precheckval.
           IF Str2 = """" THEN DISPLAY "Case in " Str4 " and old Startval is " Startval.
           IF Str2 = SPACE AND Checkval = 0 THEN MOVE "Nocontent" TO Str3 ELSE MOVE "Collection" TO Str3.
           IF Prestr = "Collection" AND Str3 = "Nocontent" AND Checkval = 0 THEN MOVE Str4 TO Endval.
           SUBTRACT 1 FROM Endval.
           *>IF Str4 > 8 AND Str4 < 30 THEN MOVE 1 TO Checkval ELSE MOVE 0 TO Checkval.
           *>IF Prestr = "Collection" AND Str3 = "Nocontent" AND Checkval = 0 THEN DISPLAY "Endval: " Endval.
           IF Prestr = "Nocontent" AND Str3 = "Collection" AND Checkval = 0 THEN MOVE Str4 TO Startval.
           IF Prestr = "Nocontent" AND Str3 = "Collection" AND Checkval = 1 AND Precheckval = 0 THEN MOVE Str4 TO Startval.
           *>IF Prestr = "Nocontent" AND Str3 = "Collection" AND Checkval = 0 THEN DISPLAY "Startval: " Startval.
           IF Prestr = "Collection" AND Str3 = "Nocontent" AND Checkval = 0 THEN WRITE Record-Name3 FROM Endval.
           IF Prestr = "Nocontent" AND Str3 = "Collection" AND Checkval = 0 THEN WRITE Record-Name3 FROM Startval.
           IF Prestr = "Nocontent" AND Str3 = "Collection" AND Checkval = 1 AND Precheckval = 0 THEN WRITE Record-Name3 FROM Startval.
           IF Prestr = "Collection" AND Str3 = "Nocontent" AND Checkval = 0 THEN ADD 1 TO Countnum.
           IF Prestr = "Nocontent" AND Str3 = "Collection" AND Checkval = 0 THEN ADD 1 TO Countnum.
           IF Str2 = """" THEN DISPLAY "Case new Startval " Startval " and Prestr is " Prestr.
           MOVE Checkval TO Precheckval.
           ADD 1 TO Str4.
           PERFORM Nextinit.
           Spaceident.
           IF Str2 = SPACE AND Checkval = 0 THEN MOVE "Nocontent" TO Str3 ELSE MOVE "Collection" TO Str3.
           IF Str2 = SPACE AND Checkval = 0 THEN MOVE Str4 TO Lastspace ELSE MOVE Str4 TO Lastchar.
           Nextinit.
           MOVE Str3 TO Prestr.
           Writeprog22.
           DISPLAY "Extractval".
           Substrprog3.
           READ File-Name INTO Str
           AT END MOVE "End" TO Str.
           IF Str = Searchstring THEN MOVE "Match" TO Str3.
           IF Str = Searchstring THEN MOVE "End" TO Str.
           MOVE Countnum TO Countnum2.
           *> DISPLAY "Line" Countnum ": " Str.
           ADD 1 TO Countnum.
           Partextract.
               *>Liest den Start- und Endwert aus
           READ Distancefile INTO Startval.
           READ Distancefile INTO Endval.
           SUBTRACT Endval FROM Startval GIVING Diffdist.
           ADD 1 TO Diffdist.
           *>DISPLAY Diffdist.
           *>DISPLAY Diffdist.
           *>MOVE 1 TO Diffdist.
           DISPLAY "Read: " Readstring.
           MOVE " " TO Str.
           STRING Readstring(Startval:Diffdist) INTO Str.
           MOVE Str TO Prestring.
           PERFORM Replacesubprog.
           MOVE Str TO Searchstring.
           MOVE " " TO Str2.
           MOVE "Nomatch" TO Str3.
           PERFORM Rownumevaluate.
           DISPLAY "Matchval: " Str3.
           MOVE Cumdiff TO Precumdiff.
           DISPLAY "Search: " Searchstring.
           IF Str3 = "Nomatch" THEN WRITE Record-Name4 FROM Prestring.
           IF Str3 = "Nomatch" THEN MOVE "_" TO Searchstring.
           IF Str3 = "Nomatch" THEN WRITE Record-Name4 FROM Diffdist.
           IF Str3 = "Nomatch" THEN MOVE 1 TO Diffdist.
           STRING Prestr(1:Cumdiff) Searchstring(1:Diffdist) INTO Concatstr.
           DISPLAY "Concat: " Concatstr.
           ADD Diffdist TO Precumdiff GIVING Cumdiff.
           MOVE Concatstr TO Prestr.
           *>DISPLAY Str3..
           MOVE " " TO Str.
           Blocksearch.
           PERFORM Blockread UNTIL Str3 = Str5.
           Blockread.
           READ Shortfile INTO Str2.
           STRING Str2(1:Cumdiff) INTO Str3.
           END PROGRAM YOUR-PROGRAM-NAME.
