                 IDENTIFICATION DIVISION.
                 PROGRAM-ID. YOUR-PROGRAM-NAME.
                 ENVIRONMENT DIVISION.
                 INPUT-OUTPUT SECTION.
                 FILE-CONTROL.
                 SELECT File-Name ASSIGN TO Filepath
                 ORGANIZATION IS LINE SEQUENTIAL.
                 SELECT File-Name2 ASSIGN TO Filepath2
                 ORGANIZATION IS LINE SEQUENTIAL.
                 SELECT Lengthinfo ASSIGN TO "D:\Cobolprogramme\SpreadsheetToCode_Cobolversion\Partspec.txt"
                 ORGANIZATION IS LINE SEQUENTIAL.
                 SELECT File-Name3 ASSIGN TO Filepath3
                 ORGANIZATION IS LINE SEQUENTIAL.
                 DATA DIVISION.
                 FILE SECTION.
                 FD  File-Name.
                 01  Record-Name.
                 02  Field1                  PIC X(80).
                 FD  File-Name2.
                 01  Record-Name2.
                 02  Field1                  PIC X(80).
                 FD  Lengthinfo.
                 01  Lengthstat.
                 02  Field1                  PIC X(200).
                 FD  File-Name3.
                 01  Record-Name3.
                 02  Field1                  PIC X(80).
                 WORKING-STORAGE SECTION.
                 01 Str PIC X(200).
                 01 Str2 PIC X(200).
                 01 Substr PIC X(200).
                 01 Evaluatestring PIC X(200).
                 01 Evalres PIC X(200).
                 01 Writestr PIC X(20).
                 01 Searchstring PIC X(200).
                 01 Checkstring PIC X(200).
                 01 Filepath PIC X(200).
                 01 Filepath2 PIC X(200).
                 01 Filepath3 PIC X(200).
                 01 Prestr PIC X(200).
                 01 Prestr2 PIC X(199).
                 01 Quotcount PIC 9(4).
                 01 Quotdiv PIC 9(4).
                 01 Funcdiff PIC 9(4).
                 01 Quotremainder PIC 9(4).
                 01 Quotrem PIC 9(4).
                 01 Countnum PIC 9(4).
                 01 Lengthnum PIC 9(4).
                 01 Currval PIC 9(4).
                 01 Startnum PIC 9(4).
                 01 Endnum PIC 9(4).
                 01 Prerow PIC 9(4).
                 01 Prewritecount PIC 9(4).
                 01 Writecount PIC 9(4).
                 01 Lastcharnum PIC 9(4).
                 01 Writediff PIC 9(4).
                 01 Diffnum PIC 9(4).
                 01 Compcheck PIC 9(4).
                 01 Compstr PIC X(1).
                 01 Precompstr PIC X(1).
                 01 Precompstrres PIC X(200).
                 01 Compstrres PIC X(200).
                 PROCEDURE DIVISION.
                 MAIN-PROCEDURE.
                 OPEN Output Lengthinfo.
                 MOVE "D:\Cobolprogramme\SpreadsheetToCode_Cobolversion\Partoutput.txt" TO Filepath.
                 *>MOVE "=C673+55" TO Str.
                 MOVE "=WENN(C3>5;1;0)" TO Str.
                 PERFORM Subextract.
                 DISPLAY "Counted: " Writecount.
                 MOVE " " TO Str.
                 PERFORM Writecountcalc.
                 *>MOVE 1 TO Writecount.
                 *>IF Evaluatestring = "Argfunc" THEN MOVE 3 TO Prerow ELSE MOVE 1 TO Prerow.
                 MOVE 1 TO Prerow.
                 DISPLAY "Evalstring: " Evaluatestring.
                 IF Evaluatestring = "Singlefunc" THEN ADD 0 TO Writecount.
                 IF Evaluatestring = "Opfunc" THEN SUBTRACT 1 FROM Writecount.
                 IF Evaluatestring = "Argfunc" THEN ADD 2 TO Writecount.
                 MOVE "D:\Cobolprogramme\SpreadsheetToCode_Cobolversion\Argcategory.txt" TO Filepath2.
                 MOVE "D:\Cobolprogramme\SpreadsheetToCode_Cobolversion\Argspecification.txt" TO Filepath3.
                 MOVE Writecount TO Prewritecount.
                 MOVE " " TO Writestr.
                 MOVE "Start" TO Writestr.
                 DISPLAY Prewritecount.
                 IF Evaluatestring = "Argfunc" THEN MOVE 3 TO Prerow.
                 IF Evaluatestring = "Argfunc" THEN SUBTRACT 2 FROM Prewritecount.
                 PERFORM Funcpartwrite.
                 CLOSE Lengthinfo.
                 STOP RUN.
                 Funcpartwrite.
                 OPEN OUTPUT File-Name2.
                 OPEN OUTPUT File-Name3.
                 DISPLAY "Quotremainder: " Quotremainder.
                 *>PERFORM Funcparts 1 TIMES.
                 PERFORM Funcparts Prewritecount TIMES.
                 MOVE "LastLine" TO Writestr.
                 WRITE Record-Name2 FROM Writestr.
                 WRITE Record-Name3 FROM Writestr.
                 ClOSE File-Name2.
                 CLOSE File-Name3.
                 Argnameeval.
                 READ File-Name2 INTO Str.
                 IF Str NOT = "LastLine" THEN PERFORM Argnameeval ELSE DISPLAY "End".
                 Writecountcalc.
                 IF Evaluatestring = "Argfunc" THEN SUBTRACT 3 FROM Writecount.
                 IF Evaluatestring = "Argfunc" THEN DIVIDE Writecount BY 2 GIVING Writediff.
                 IF Evaluatestring = "Argfunc" THEN DISPLAY Writediff.
                 Funcparts.
                 PERFORM Funcargbase.
                 DISPLAY Prestr2.
                 *>DISPLAY "Evalres: " Evaluatestring.
                 *>MOVE "Funcargrel" TO Evaluatestring.
                 PERFORM Funcargevalprog.
                 *>IF Evaluatestring = "Singlefunc" THEN PERFORM Funcargevalprog.
                 IF Evaluatestring = "Singlefunc" THEN WRITE Record-Name2 FROM Writestr.
                 IF Evaluatestring = "Singlefunc" THEN WRITE Record-Name2 FROM Prerow.
                 IF Evaluatestring = "Singlefunc" THEN PERFORM Nextfuncrun.
                 Nextfuncrun.
                 IF Evaluatestring = "Singlefunc" THEN DISPLAY "Argkindeval".
                 PERFORM Argsearchprep.
                 MOVE "=" TO Precompstr.
                 MOVE 2 TO Countnum.
                 MOVE "=" TO Precompstrres.
                 MOVE 1 TO Lengthnum.
                 MOVE 2 TO Quotcount.
                 PERFORM Argsearchres 199 TIMES.
                 WRITE Record-Name2 FROM Lastcharnum.
                 DISPLAY "Comp: " FUNCTION TRIM(Compstrres).
                 MOVE "Compstrres" TO Writestr.
                 WRITE Record-Name3 FROM Writestr.
                 WRITE Record-Name3 FROM Compstrres.
                 MOVE "Start" TO Writestr.
                 *>DISPLAY Lengthnum.
                 Funcargbase.
                 OPEN INPUT File-Name.
                 DISPLAY "Prerow: " Prerow.
                 *>DISPLAY "Eval: " Evaluatestring.          
                 PERFORM Outputrowread Prerow TIMES.
                 READ File-Name INTO Prestr2.
                 CLOSE File-Name.
                 ADD 1 TO Prerow.
                 Funcargevalprog.
                 STRING "=" Prestr2 INTO Str.
                 DISPLAY Str.
                 PERFORM Subextractinit.
                 *>Changed Progname from Subextract
                 PERFORM Subextractalt.
                 Outputrowread.
                 READ File-Name.
                 Subextractinit.
                 MOVE 0 TO Lengthnum.
                 MOVE 0 TO Quotcount.
                 MOVE " " TO Checkstring.
                 MOVE " " TO Str2.
                 MOVE " " TO Evalres.
                 MOVE " " TO Evaluatestring.
                 Subextractalt.
                 MOVE 2 TO Countnum.
                 MOVE "Singlefunc" TO Evaluatestring.
                 PERFORM Evaluationprogs UNTIL Checkstring = "End".
                 IF Evalres = "Opfunc" THEN MOVE "Opfunc" TO Evaluatestring.
                 DISPLAY "Functioncategory: " Evaluatestring.
                 SUBTRACT 2 FROM Lengthnum.
                 DISPLAY "The String is " Lengthnum " Characters long."
                 DISPLAY "Entered String: " Str.
                 WRITE Lengthstat FROM Evaluatestring.
                 WRITE Lengthstat FROM Lengthnum.
                 WRITE Lengthstat FROM Str.
                 Subextract.
                 MOVE 2 TO Countnum.
                 MOVE "Singlefunc" TO Evaluatestring.
                 PERFORM Evaluationprogs UNTIL Checkstring = "End".
                 IF Evalres = "Opfunc" THEN MOVE "Opfunc" TO Evaluatestring.
                 DISPLAY "Functioncategory: " Evaluatestring.
                 SUBTRACT 2 FROM Lengthnum.
                 WRITE Lengthstat FROM Str.
                 WRITE Lengthstat FROM "Evalbase".
                 WRITE Lengthstat FROM Evaluatestring.
                 WRITE Lengthstat FROM "Arglist".
                 DISPLAY "The String is " Lengthnum " Characters long."
                 DISPLAY "Entered String: " Str.
                 ADD 1 TO Lengthnum.
                 MOVE 1 TO Countnum.
                 MOVE 0 TO Quotcount.
                 ADD 1 TO Lengthnum.
                 MOVE 0 TO Funcdiff.
                 IF Evaluatestring = "Opfunc" THEN MOVE 2 TO Startnum ELSE MOVE 1 TO Startnum.
                 DISPLAY "Startnum: " Startnum.
                 MOVE 1 TO Countnum.
                 OPEN OUTPUT File-Name.
                 IF Evaluatestring = "Argfunc" THEN PERFORM Argfuncextprog Lengthnum TIMES.
                 IF Evaluatestring = "Opfunc" THEN PERFORM Opfuncextprog Lengthnum TIMES.
                 IF Evaluatestring = "Singlefunc" THEN MOVE "=" TO Substr.
                 IF Evaluatestring = "Singlefunc" THEN WRITE Record-Name FROM Substr.
                 IF Evaluatestring = "Singlefunc" THEN STRING Str(2:Lengthnum) INTO Substr.
                 IF Evaluatestring = "Singlefunc" THEN WRITE Record-Name FROM Substr.
                 IF Evaluatestring = "Singlefunc" THEN ADD 1 TO Writecount.
                 CLOSE File-Name.
                 Substrevaluation.
                 STRING Str(Countnum:1) INTO Str2.
                 ADD 1 TO Countnum.
                 Argfuncevaluation.
                 IF Str2 = "(" AND Quotremainder = 0 THEN ADD 1 TO Funcdiff.
                 IF Str2 = ")" AND Quotremainder = 0 THEN SUBTRACT 1 FROM Funcdiff.
                 IF Str2 = "(" AND Quotremainder = 0 THEN MOVE "Argfunc" TO Evaluatestring.
                 IF Str2 = ")" AND Quotremainder = 0 THEN MOVE "Argfunc" TO Evaluatestring.
                 Opfuncevaluation.
                 IF Str2 = "&" AND Quotremainder = 0 AND Funcdiff = 0 THEN MOVE "Opfunc" TO Evaluatestring.
                 IF Str2 = "+" AND Quotremainder = 0 AND Funcdiff = 0 THEN MOVE "Opfunc" TO Evaluatestring.
                 IF Str2 = "-" AND Quotremainder = 0 AND Funcdiff = 0 THEN MOVE "Opfunc" TO Evaluatestring.
                 IF Str2 = "*" AND Quotremainder = 0 AND Funcdiff = 0 THEN MOVE "Opfunc" TO Evaluatestring.
                 IF Str2 = "^" AND Quotremainder = 0 AND Funcdiff = 0 THEN MOVE "Opfunc" TO Evaluatestring.
                 IF Str2 = "/" AND Quotremainder = 0 AND Funcdiff = 0 THEN MOVE "Opfunc" TO Evaluatestring.
                 IF Str2 = "<" AND Quotremainder = 0 AND Funcdiff = 0 THEN MOVE "Opfunc" TO Evaluatestring.
                 IF Str2 = ">" AND Quotremainder = 0 AND Funcdiff = 0 THEN MOVE "Opfunc" TO Evaluatestring.
                 IF Str2 = "=" AND Quotremainder = 0 AND Funcdiff = 0 AND Countnum > 2 THEN MOVE "Opfunc" TO Evaluatestring.
                 *>IF Str2 = "=" AND Quotremainder = 0 AND Funcdiff = 0 THEN MOVE "Opfunc" TO Evaluatestring.
                 IF Evaluatestring = "Opfunc" THEN MOVE "Opfunc" TO Evalres.
                 Stringevaluation.
                 IF Str2 = """" THEN ADD 1 TO Quotcount.
                 DIVIDE Quotcount BY 2 GIVING Quotdiv REMAINDER Quotremainder.
                 Emptyevaluation.
                 IF Str2 = " " AND Quotremainder = 0 THEN MOVE "End" TO Checkstring.
                 IF Str2 = " " AND Quotremainder = 0 THEN MOVE Countnum TO Lengthnum.
                 Evaluationprogs.
                 PERFORM Substrevaluation.
                 PERFORM Stringevaluation.
                 PERFORM Argfuncevaluation.
                 PERFORM Opfuncevaluation.
                 PERFORM Emptyevaluation.
                 Opfuncextract.
                 IF Str2 = "&" AND Quotremainder = 0 AND Funcdiff = 0 THEN PERFORM Opfuncextsub.
                 IF Str2 = "+" AND Quotremainder = 0 AND Funcdiff = 0 THEN PERFORM Opfuncextsub.
                 IF Str2 = "-" AND Quotremainder = 0 AND Funcdiff = 0 THEN PERFORM Opfuncextsub.
                 IF Str2 = "*" AND Quotremainder = 0 AND Funcdiff = 0 THEN PERFORM Opfuncextsub.
                 IF Str2 = "^" AND Quotremainder = 0 AND Funcdiff = 0 THEN PERFORM Opfuncextsub.
                 IF Str2 = "/" AND Quotremainder = 0 AND Funcdiff = 0 THEN PERFORM Opfuncextsub.
                 IF Str2 = "<" AND Quotremainder = 0 AND Funcdiff = 0 THEN PERFORM Opfuncextsub.
                 IF Str2 = ">" AND Quotremainder = 0 AND Funcdiff = 0 THEN PERFORM Opfuncextsub.
                 IF Str2 = "=" AND Quotremainder = 0 AND Funcdiff = 0 AND Countnum > 2 THEN PERFORM Opfuncextsub.
                 IF Countnum = Lengthnum THEN PERFORM Opfuncextsub.
                 Opfuncextsub.
                 IF Countnum = Lengthnum THEN ADD 1 TO Countnum.
                 IF Countnum = Lengthnum + 1 THEN ADD 1 TO Countnum.
                 MOVE Countnum TO Endnum.
                 STRING Str(Startnum - 1:1) INTO Substr.
                 IF Startnum > 2 THEN STRING Str(Startnum - 2:1) INTO Precompstr.
                 IF Precompstr = "=" OR Precompstr = ">" OR Precompstr = "<" THEN ADD 1 TO Compcheck.
                 IF Substr = "=" OR Substr = ">" OR Substr = "<" THEN ADD 1 TO Compcheck.
                 IF Compcheck < 2 THEN WRITE Record-Name FROM Substr.
                 IF Compcheck < 2 THEN ADD 1 TO Writecount.
                 DISPLAY "Comp: " Compcheck.
                 MOVE 0 TO Compcheck.
                 DISPLAY "Comp: " Compcheck.
                 MOVE Substr TO Precompstr.
                 *>DISPLAY "Extracted Substr: " Substr.
                 MOVE " " TO Substr.
                 IF Startnum = Endnum - 1 THEN MOVE 0 TO Diffnum ELSE MOVE 1 TO Diffnum.
                 STRING Str(Startnum:Endnum - Startnum - Diffnum) INTO Substr.
                 DISPLAY "Nextwrite: " Substr.
                 WRITE Record-Name FROM Substr.
                 ADD 1 TO Writecount.
                 DISPLAY "Endnum: " Endnum.
                 *>IF Startnum = Endnum - 1 THEN ADD 1 TO Endnum.
                 MOVE Endnum TO Startnum.
                 MOVE " " TO Substr.
                 Argfuncextract2.
                 IF Str2 = "(" AND Quotremainder = 0 AND Funcdiff = 1 THEN PERFORM Argfuncextsub2.
                 IF Str2 = ";" AND Quotremainder = 0 AND Funcdiff = 1 THEN PERFORM Argfuncextsub2.
                 IF Str2 = ")" AND Quotremainder = 0 AND Funcdiff = 0 THEN PERFORM Argfuncextsub2.
                 Argfuncextsub2.
                 SUBTRACT 1 FROM Countnum.
                 MOVE Countnum TO Endnum.
                 ADD 1 TO Countnum.
                 *>ADD 1 TO Countnum.
                 STRING Str(Startnum:1) INTO Substr.
                 DISPLAY Substr.
                 WRITE Record-Name FROM Substr.
                 ADD 1 TO Writecount.
                 MOVE " " TO Substr.
                 STRING Str(Startnum + 1 :Endnum - Startnum - 1) INTO Substr.
                 MOVE Endnum TO Startnum.
                 WRITE Record-Name FROM Substr.
                 ADD 1 TO Writecount.
                 DISPLAY Substr.
                 MOVE " " TO Substr.
                 Opfuncextprog.
                 PERFORM Substrevaluation.
                 PERFORM Stringevaluation.
                 PERFORM Argfuncevaluation.
                 PERFORM Opfuncextract.
                 Argfuncextprog.
                 PERFORM Substrevaluation.
                 PERFORM Stringevaluation.
                 PERFORM Argfuncevaluation.
                 PERFORM Argfuncextract2.
                 IF Countnum = Lengthnum THEN DISPLAY ")".
                 IF Countnum = Lengthnum THEN MOVE ")" TO Substr.
                 IF Countnum = Lengthnum THEN WRITE Record-Name FROM Substr.
                 IF Countnum = Lengthnum THEN ADD 1 TO Writecount.
                 Argsearchprep.
                 *>MOVE "=A$2" TO Str.
                 INSPECT Str REPLACING All "A" BY "|", "B" BY "|", "C" BY "|", "D" BY "|".
                 INSPECT Str REPLACING All "E" BY "|", "F" BY "|", "G" BY "|", "H" BY "|".
                 INSPECT Str REPLACING All "I" BY "|", "J" BY "|", "K" BY "|", "L" BY "|".
                 INSPECT Str REPLACING All "M" BY "|", "N" BY "|", "O" BY "|", "P" BY "|".
                 INSPECT Str REPLACING All "Q" BY "|", "R" BY "|", "S" BY "|", "T" BY "|".
                 INSPECT Str REPLACING All "U" BY "|", "V" BY "|", "W" BY "|", "X" BY "|".
                 INSPECT Str REPLACING All "Y" BY "|", "Z" BY "|", "1" BY ",", "2" BY ",".
                 INSPECT Str REPLACING All "3" BY ",", "4" BY ",", "5" BY ",", "6" BY ",".
                 INSPECT Str REPLACING All "7" BY ",", "8" BY ",", "9" BY ",", "0" BY ",".
                 DISPLAY Str.
                 Argsearchres.
                 *>DISPLAY "Argsearchstr: " Str.
                 STRING Str(Countnum:1) INTO Compstr.
                 IF Compstr NOT EQUAL TO " " THEN MOVE Countnum TO Lastcharnum.
                 IF Compstr = """" THEN ADD 1 TO Quotcount ELSE ADD 0 TO Quotcount.
                 DIVIDE Quotcount BY 2 GIVING Quotdiv REMAINDER Quotrem.
                 IF Quotrem = 1 AND Compstr = """" THEN PERFORM Argopt1.
                 IF Quotrem = 0 THEN PERFORM Argopt2.
                 Argopt1.
                 STRING Precompstrres(1:Countnum - 1) Compstr INTO Compstrres.
                 MOVE Compstrres TO Precompstrres.
                 ADD 1 TO Lengthnum.
                 MOVE Compstr TO Precompstr.
                 *>IF Compstr NOT EQUAL TO " " THEN MOVE Countnum TO Lastcharnum.
                 ADD 1 TO Countnum.
                 Argopt2.
                 IF Compstr NOT = Precompstr THEN STRING Precompstrres(1:Countnum - 1) Compstr INTO Compstrres.
                 IF Compstr NOT EQUAL TO Precompstr THEN MOVE Compstrres TO Precompstrres.
                 IF Compstr NOT EQUAL TO Precompstr THEN ADD 1 TO Lengthnum.
                 *>IF Compstr NOT EQUAL TO Precompstr THEN DISPLAY "Position: " Countnum.
                 IF Compstr NOT EQUAL TO Precompstr THEN WRITE Record-Name2 FROM Countnum.
                 MOVE Compstr TO Precompstr.
                 *>IF Compstr NOT EQUAL TO " " THEN MOVE Countnum TO Lastcharnum.
                 ADD 1 TO Countnum.
                 END PROGRAM YOUR-PROGRAM-NAME.
